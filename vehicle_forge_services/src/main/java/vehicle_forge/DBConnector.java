package vehicle_forge;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnector {
	
	private final static String logTag = DBConnector.class.getName();

	private static DBConnector connectorInstance = null;
	private Statement stmt = null;
    private final String url = Config.DB_HOST + ":" + Config.DB_PORT + "/" + Config.DB_NAME;
    
	protected DBConnector() {
    	try {
	    	Properties props = new Properties();
	    	props.setProperty("user", Config.DB_USER);
	    	props.setProperty("password", Config.DB_PASS);
	    	Connection conn = DriverManager.getConnection(url, props);
	    	
	    	this.stmt = conn.createStatement();
	    	ServiceLogger.log(logTag, "Connection URL - " + url);
    	} catch (SQLException e) {
    		ServiceLogger.log(logTag, e.getMessage());
    	}
	}
	
	public static ResultSet executeQuery(String query) {
		try {
			if (connectorInstance == null) {
				connectorInstance = new DBConnector();
			} 
			return connectorInstance.stmt.executeQuery(query);
		} catch(SQLException e) {
			ServiceLogger.log(logTag, e.getMessage());
		}
		return null;
	}
}