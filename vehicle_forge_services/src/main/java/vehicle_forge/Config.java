package vehicle_forge;

public class Config {
	
		//Example: jdbc:postgresql://localhost:5432/vehicle_forge
		
		public static final String DB_HOST = "jdbc:postgresql://localhost";
		public static final int DB_PORT = 5432;
		public static final String DB_NAME = "vehicle_forge";
		public static final String DB_USER = "postgres";
		public static final String DB_PASS = "password"; //Please change to your local account password
		
		public static final String LOG_FILE = "log/events.log";
		
	}
