package vehicle_forge;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.JSONObject;

public class SpecificationDao {

	private final String logTag = "Specification Search";
	private ResultSet resultSet; 

	public SpecificationDao() {}

	public Specification getSpecification(int serviceID) {
		////Logger.log(logTag, "In specification search with service id " + serviceID);
		Trial trial = null;
		int numMembers = 0, numProjects = 0, numSuccesses = 0, numFails = 0;
		Usage usage = null;
		int num_inputs = 0, num_outputs = 0;
		String description = "";
		
		try {
			resultSet = DBConnector.executeQuery("SELECT count(owner_id) AS count FROM cem_runnables_running WHERE interface_id = "
		+ serviceID);
			while (resultSet.next()) {
				numMembers = resultSet.getInt("count");
				//Logger.log(logTag, "Number of members - " + numMembers);
			}
			
			resultSet = DBConnector.executeQuery("SELECT count(group_id) AS count FROM service_subscriptions WHERE interface_id = "
					+ serviceID);
			while (resultSet.next()) {
				numProjects = resultSet.getInt("count");
				//Logger.log(logTag, "Number of projects using service - " + numProjects);
			}
			
			usage = new Usage(numMembers, numProjects);
			//Logger.log(logTag, "" + usage);
			
			resultSet = DBConnector.executeQuery("SELECT interface_data, description FROM dome_interfaces WHERE interface_id = "
					+ serviceID);
			while (resultSet.next()) {
				description = resultSet.getString("description");
				//Logger.log(logTag, "Description: " + description);
				
				
				JSONObject json = new JSONObject(resultSet.getString("interface_data")); 
				
				if(json.has("inParams")){
					JSONObject inputParams = json.getJSONObject("inParams");
					num_inputs = inputParams.length();
				}
				
				if(json.has("outParams")){
					JSONObject outputParams = json.getJSONObject("outParams");
					num_outputs = outputParams.length();
				}
				
				//Logger.log(logTag, "Number of inputs for service - " + num_inputs);
				//Logger.log(logTag, "Number of outputs for service - " + num_outputs);
			}
			
			
			resultSet = DBConnector.executeQuery("SELECT count(*) AS Fails FROM runnable_runtimes WHERE interface_id = "
					+ serviceID + " AND date_completed = 0");
			while (resultSet.next()) {
				numFails = resultSet.getInt("Fails");
				//Logger.log(logTag, "Number of failed tests: " + numFails);	
			}
			
			resultSet = DBConnector.executeQuery("SELECT count(*) AS Successes FROM runnable_runtimes WHERE interface_id = "
					+ serviceID + " AND date_completed != 0");
			while (resultSet.next()) {
				numSuccesses = resultSet.getInt("Successes");
				//Logger.log(logTag, "Number of passed tests: " + numSuccesses);	
			}
			
			trial = new Trial(numSuccesses, numFails);
			
			return new Specification(description, num_inputs, num_outputs, usage, trial);
			
		} catch (SQLException e) {
			//Logger.log(logTag, e.getMessage());
		}
		return null;
	}

}