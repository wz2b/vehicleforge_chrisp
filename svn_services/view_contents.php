<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:12 PM 10/28/11
 */

require_once 'config.php';

$Repo=$_REQUEST['repo'];
$File=$_REQUEST['file'];

$Filename=substr($File,strrpos($File,"/")+1);

//TODO: add in error checking
//TODO: Duplicate files may exist if 2 users try to read files with the same filename - Might be an issue - Add time hash to file to fix this
//Export an individual file to another folder for reading
$CMD="svn export file://".$SVNRoot."/".$Repo."/".$File." ".$SVNContents."/".$Filename;
exec($CMD);

//Read the file - passthru prints the contents of the file to the page
$CMD="cat ".$SVNContents."/".$Filename;
passthru($CMD);

//Remove the file since it has already been read
$CMD="rm ".$SVNContents."/".$Filename;
exec($CMD);
?>