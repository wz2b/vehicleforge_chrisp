<?php
require_once APP_PATH.'common/Amazon/sdk-1.5.4/sdk.class.php';
require_once APP_PATH.'common/Amazon/sdk-1.5.4/services/ses.class.php';

class SES   {
    private $AmazonSES, $VerfiedSenders=null;

    public function __construct(){
        $this->AmazonSES=new AmazonSES();
    }

    private function getVerifiedSenders(){
        if (!$this->VerfiedSenders){
            $Result=$this->AmazonSES->list_verified_email_addresses();

            $Result=$Result->body->ListVerifiedEmailAddressesResult->VerifiedEmailAddresses->member;

            foreach ($Result as $i)
                $this->VerfiedSenders[]=$i->to_string();
        }

        return $this->VerfiedSenders;
    }

    public function sendMail($From, $To, $Subject, $Message){
        if (in_array($From, $this->getVerifiedSenders())){
            $To=(array)$To;

            $Result=$this->AmazonSES->send_email($From,array(
                'ToAddresses'=>$To
            ),array(
                'Subject.Data'=>$Subject,
                'Body.Text.Data'=>str_replace('<br />','
', strip_tags($Message)),
                'Body.Html.Data'=>$Message
            ));

            if ($Result->isOK()){
                return true;
            }else{
                var_dump($Result);
            }
        }

        return false;
    }
}
?>