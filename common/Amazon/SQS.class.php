<?php
require_once APP_PATH.'common/Amazon/sdk-1.5.2/services/sqs.class.php';
//require_once APP_PATH.'common/Amazon/sdk-1.5.2/utilities/credentials.class.php';

class SQS extends Error    {
	private $Debug, $AmazonSQS;

	function __construct($Debug=false){
		@$this->AmazonSQS=new AmazonSQS;
		$this->Debug=$Debug;
	}

	function createQueue($QueueName){
		$Result=$this->AmazonSQS->create_queue($QueueName);

		if ($Result->isOK()){
			$QueueURL=$Result->body->CreateQueueResult->QueueUrl;
			$QueueURL='https://queue.amazonaws.com/'.substr($QueueURL,strpos($QueueURL,'com/')+4);
			return $QueueURL;
		}else{
			$this->setError('Cannot create queue');
			return false;
		}
	}

	/**
	 * getMessages - Returns an array of QueueMessage objects
	 *
	 * @param string $QueueURL
	 * @param int $Limit
	 * @param int $Timeout
	 * @return array|bool
	 */
	function getMessages($QueueURL, $Limit=10, $Timeout=5){
		$Result=$this->AmazonSQS->receive_message($QueueURL, array(
			'MaxNumberOfMessages'=>$Limit,
			'VisibilityTimeout'=>$Timeout
		));
		if ($Result->isOK()){
			$Response=$Result->body->ReceiveMessageResult->Message;

			$Messages=array();
			foreach($Response as $i)
				$Messages[]=new QueueMessage($i);

			return $Messages;
		}else{
			$this->setError('Cannot receive messages from queue');
			return false;
		}
	}

	function deleteQueue($QueueURL){
		$Result=$this->AmazonSQS->delete_queue($QueueURL);
		if ($Result->isOK()){
			return true;
		}else{
			$this->setError('Cannot delete queue');
			return false;
		}
	}

	function deleteMessage($QueueURL, $ReceiptHandle){
		$Result=$this->AmazonSQS->delete_message($QueueURL, $ReceiptHandle);
		if($Result->isOK()){
			return true;
		}else{
			$this->setError('Cannot delete message from queue');
			return false;
		}
	}
}

class QueueMessage  {
	private $ID, $ReceiptHandle, $Body;

	function __construct($MessageObject){
		$this->Body=$MessageObject->Body;
		$this->ReceiptHandle=$MessageObject->ReceiptHandle;
		$this->ID=$MessageObject->MessageId;
	}

	/**
	 * @return string
	 */
	function getBody(){
		return $this->Body;
	}

	/**
	 * @return string
	 */
	function getReceiptHandle(){
		return $this->ReceiptHandle;
	}
}
?>