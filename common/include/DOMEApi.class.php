<?php
//require_once $GLOBALS['gfcommon'].'include/curl.php';
//require_once 'Error.class.php';

class DOMEApi extends Error{
    public $CURL;
	private $URL, $Debug, $SQS=null;

    /**
     * @param string $URL - URL to DOME server
     * @param bool $Debug
     */
	function __construct($URL, $Debug=false){
		//$this->URL=$GLOBALS['DOME_URL'];
		$this->URL=$URL;
		$this->Debug=$Debug;

		$this->CURL=new curl;//($URL.'getChildren');
	}

    /**
     * getChildren - Gets all children within a folder or model
     * Returns:
     *      Success: Object
     *      Failure: false
     *
     * @param string|null $Type
     * @param string|null $Name
     * @param array|null $Path
     * @return object|bool
     */
	function getChildren($Type=null, $Name=null, $Path=null){
		if (!is_string($Type) && $Type!=null){
			$this->setError("Type must be a string");
			return false;
		}

		if (!is_string($Name) && $Name!=null){
			$this->setError("Name must be a string");
			return false;
		}

		if (!is_array($Path) && $Path!=null){
			$this->setError("Path must be an array");
			return false;
		}

		$this->CURL->opts(array(
			CURLOPT_URL=>$this->URL.'getChildren'
		));

	        $URL='/';

		if ($Type!=null && $Name!=null && $Path!=null){
			$PostData=array(
				'type'=>$Type,
				'name'=>$Name,
				'path'=>(array)$Path
			);
			
			$PostData=json_encode($PostData);
			$this->CURL->opts(array(
						CURLOPT_URL=>$this->URL.'getChildren?data='.urlencode($PostData)
						));
			$URL='?'.$PostData;
		}

		$Data=json_decode($this->CURL->exec());

		if ($this->Debug){
			echo '<b>Requesting url:</b> '.$this->URL.'getChildren';
			echo '<br /><b>Requesting curl:</b> '.$this->CURL->info();
			$this->CURL->error();
			echo '<br /><b>Response</b>: ';
			print_r($Data);
			echo '<br /><br />';
		}

        if (is_object($Data)){
            if ($Data->status=='success'){
                return $Data->pkg->children;
            }else{
                $this->setError('Error in getChildren: '.$Data->msg);
                return false;
            }
        }else{
            if ($Data['status']=='success'){
                return $Data['pkg']['children'];
            }else{
                $this->setError('Error in getChildren: '.$Data['msg']);
                return false;
            }
        }
	}

    /**
     * getInterfaces - Gets all interfaces within a model
     * Returns:
     *      Success: Object
     *      Failure: false
     *
     * @param int $Version
     * @param int $ModelID
     * @param string $Description
     * @param int $DateMod
     * @param string $Type
     * @param string $Name
     * @param array $Path
     * @return object|bool
     */
	function getInterfaces($Version, $ModelID, $Description, $DateMod, $Type, $Name, $Path){
		$PostData=array(
			'version'=>(int)$Version,
			'modelId'=>$ModelID,
			'description'=>$Description,
			'dateModified'=>(int)$DateMod,
			'type'=>$Type,
			'name'=>$Name,
			'path'=>(array)$Path
		);
		$PostData=json_encode($PostData);

		$this->CURL->opts(array(
					//			CURLOPT_URL=>$this->URL.'getChildren'
					CURLOPT_URL=>$this->URL.'getChildren?data='.urlencode($PostData)
					));
		
		$Data=json_decode($this->CURL->exec());

		if ($this->Debug){
			echo '<b>Requesting:</b> '.$this->URL.'getChildren?'.$this->CURL->format_data(array('data'=>$PostData));
			echo '<br /><b>Response</b>: ';
			var_dump($Data);
			echo '<br /><br />';
		}

        if (is_object($Data)){
            if ($Data->status=="success"){
                return $Data->pkg->children;
            }else{
                $this->setError('Error in getInterfaces: '.$Data->msg);
                return false;
            }
        }else{
            if ($Data['status']=="success"){
                return $Data['pkg']['children'];
            }else{
                $this->setError('Error in getInterfaces: '.$Data['msg']);
                return false;
            }
        }
	}

    /**
     * getModel - Gets a single interface from a model
     * Returns:
     *      Success: stdObject
     *      Failure: false
     *
     * @param int $Version
     * @param string $ModelID
     * @param int $InterfaceID
     * @param string $Name
     * @param array $Path
     * @param int $IType
     * @internal param int $FolderID
     * @return object bool
     */
    function getModel($Version, $ModelID, $InterfaceID, $Name, $Path, $IType){
        $Type='interface';
        switch($IType) {
        	case 0:
        		$mID = 'modelId';
        		break;
        	case 1:
        		$mID = 'projectId';
        		break;
        }
        $PostData=array(
            'type'=>$Type,
            'version'=>(int)$Version,
            $mID=>$ModelID,
            'interfaceId'=>$InterfaceID,
            'name'=>$Name,
            'path'=>(array)$Path
        );
        $PostData=json_encode($PostData);

        $this->CURL->opts(array(
            CURLOPT_URL=>$this->URL.'getModel?data='.urlencode($PostData),
            CURLOPT_POSTFIELDS=>$this->CURL->format_data(array('data'=>$PostData))
        ));

        $Data=json_decode($this->CURL->exec());

        if ($this->Debug){
            echo '<b>Requesting:</b> '.$this->URL.'getModel?'.$this->CURL->format_data(array('data'=>$PostData));
            echo '<br /><b>Response</b>: ';
            print_r($Data);
            echo '<br /><br />';
        }

        if ($Data->status=="success"){
            return $Data->pkg;
        }else{
            $this->setError('Error in getModel: '.$Data->msg);
            return false;
        }
    }

    /**
     * createSQS - Creates an Amazon SQS instance
     *
     * @return bool
     */
	private function createSQS(){
		//require_once $GLOBALS['gfcommon'].'Amazon/sdk-1.5.2/services/sqs.class.php';
        require_once $GLOBALS['gfcommon'].'Amazon/SQS.class.php';
		if ($this->SQS==null)
			$this->SQS=new SQS;

        return true;
	}

    /**
     * runModel - Creates an SQS queue, sends the queue URL to DOME to run the model
     * Returns the queue URL
     *
     * @param int $InterfaceID
     * @param string $Inputs
     * @return string
     */
	public function runModel($InterfaceID, $Inputs){
		//Get the SQS instance
		//$this->createSQS();

		//Load the DOME interface data
		$DOMEInterface=new DOMEInterface($InterfaceID);
		if ($Data=$DOMEInterface->getData()){
            //print_r($Data);
			//Loop through the default inputs and replace them with the user-given inputs
			$InParams=$Data->inParams;
			$Inputs=json_decode($Inputs);

            $NewInParams=array();

            foreach($InParams as $InParam){
                foreach($Inputs as $key=>$val){
                    //var_dump($InParams[$i]);
                    if ($key==$InParam->name){
                        $NewInParams[$key]=$InParam;
                        $InParam->value=$val;
                    }
                }
            }

            //print_r($NewInParams);

            $Data->inParams=$NewInParams;

			//Create a unique queue name
			$Name=str_replace(' ','',$Data->interFace->name).'-'.time();

			$jsonData=json_encode($Data);
// $Data	{"interFace":{"version":3,"interfaceId":"2978c002-d20e-1004-8083-87ccc336aeab","projectId":"FiberSegmentationModel","type":"interface","name":"FiberSegmentationRawInterface","path":[33]},"inParams":{"RadUBReal":{"name":"RadUBReal","type":"Real","unit":"no unit","category":"no unit","value":"12","parameterid":"1553dab8-d20e-1004-897a-08dc2a95ea04"},"HoughThreshold":{"name":"HoughThreshold","type":"Real","unit":"no unit","category":"no unit","value":"0.45","parameterid":"1553daba-d20e-1004-897a-08dc2a95ea04"},"RadLBReal":{"name":"RadLBReal","type":"Real","unit":"no unit","category":"no unit","value":"3.5","parameterid":"1553dab7-d20e-1004-897a-08dc2a95ea04"},"PixelUnit":{"name":"PixelUnit","type":"String","unit":"","value":"microns","parameterid":"1553dab6-d20e-1004-897a-08dc2a95ea04"},"CotMaxThickness":{"name":"CotMaxThickness","type":"Real","unit":"no unit","category":"no unit","value":"6","parameterid":"1553dab9-d20e-1004-897a-08dc2a95ea04"},"LevelSi":{"name":"LevelSi","type":"Real","unit":"no unit","categor	
// $Data	{"interFace":{"version":1,"modelId":"dce5f097-d209-1004-8c2f-9bc3140adc20","interfaceId":"dce5f098-d209-1004-8c2f-9bc3140adc20","type":"interface","name":"Square Interface","path":[30,31]},"inParams":{"inVal":{"name":"inVal","type":"Real","unit":"no unit","category":"no unit","value":"64","parameterid":"bf8235e4-d1ff-1004-8643-d863585dad68"}},"outParams":{"outVal":{"name":"outVal","type":"Real","unit":"no unit","category":"no unit","value":0,"parameterid":"bf8235e7-d1ff-1004-8643-d863585dad68"}},"modelName":"Square Interface","modelDescription":"","server":{"name":"localhost","port":"7791","user":"ceed","pw":"ceed","space":"USER"}}	
//          {"interFace":{"version":1,"modelId":"bc3f80d5-d0b8-1004-8dcd-5b93a02c8e5f","interfaceId":"bc3f80d6-d0b8-1004-8dcd-5b93a02c8e5f","type":"interface","name":"GDS Tool Runner Interface","path":[23,24]},"inParams":{"inRemoteDisplay":{"name":"inRemoteDisplay","type":"String","unit":"","value":"","parameterid":"cbf1b03c-d0b8-1004-8238-e1f80854c244"},"inWorkingDir":{"name":"inWorkingDir","type":"String","unit":"","value":"\/tmp","parameterid":"98e6953a-d069-1004-8607-b0e5a8a781a1"},"inLEADTool":{"name":"inLEADTool","type":"String","unit":"","value":"bash","parameterid":"98e69534-d069-1004-8607-b0e5a8a781a1"},"inLEADArgs":{"name":"inLEADArgs","type":"String","unit":"","value":"ls -al,exit","parameterid":"98e69537-d069-1004-8607-b0e5a8a781a1"},"inStdErr":{"name":"inStdErr","type":"String","unit":"","value":"","parameterid":"98e69543-d069-1004-8607-b0e5a8a781a1"},"inStdOut":{"name":"inStdOut","type":"String","unit":"","value":"","parameterid":"98e69540-d069-1004-8607-b0e5a8a781a1"},"inExitCode":{"name":"inExitCode","typ
            
			//print_r($jsonData);

			//Create the queue
			//$QueueURL=$this->SQS->createQueue($Name);
			$QueueURL = $Name;
			
			$curlArr = array();
			$uploadfolder = APP_PATH."www/upload/";
			foreach ($_FILES as $file) {
				//move_uploaded_file($file['filename']['tmp_name'], $folder.$file['filename']['name']);
				if ($file["error"] == UPLOAD_ERR_OK) {
					$tmp_name = $file["tmp_name"];
					$name = $file["name"];
					move_uploaded_file($tmp_name, $uploadfolder.$name);
				}
				$curlArr[$file["name"]] = "@".$uploadfolder.$name.";type=application/octet-stream";
				//$this->CURL->addFile($file);
			}
			
			$args = array( "data"=>$jsonData,
						   "queue"=>$QueueURL
						 );
			
			$curlArr = (count($curlArr) > 0?array_merge($curlArr, $args):$args);
			//$this->CURL->data($args);
			
			//Send data to DOME server
			// Effin PHP for some reason only this works.
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, trim($this->URL."runModel"));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $curlArr);
			//curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)");
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_VERBOSE, 0);
			$Data = curl_exec($ch);
			/*$this->CURL->opts(array(
				CURLOPT_URL=>$this->URL."runModel",
				CURLOPT_POSTFIELDS=>$curlArr
				));
			//$this->CURL->setURL($this->URL."runModel");
			$Data=$this->CURL->exec();
			*/
			if ($this->Debug) {
				echo '<b>Requesting:</b> '.$this->URL.'runModel?'.$this->CURL->format_data(array("data"=>$Data)).' from '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'<br><br>';
				echo '<b>Response:</b> '.$Data.'<br><br>';
				echo '<b>Error:</b> '.curl_error($ch).'<br><br>';
			}
			
			curl_close($ch);
			return $QueueURL;
		}else{
			$this->setError($DOMEInterface->getErrorMessage());
			return false;
		}
	}

  /**
   * putIntregrationModel - Creates a new integration model
   * Returns the DOME interface of the new model
   * @param Group $group - owner of the integration model
   * @param string $name - name of the integration model
   * @param string $description - description of the integration model
   * @param Array $models - array of all model interfaces used in the integration model
   * @param Array $relations - array of all relationships in the integration model
   * @return string $
   */
	public function putIntegrationModel($group, $name, $description, $models, $relations, $inParams, $outParams) {
		// set url path	
		$this->CURL->opts(array( CURLOPT_URL => ($this->URL.'putIntegrationModel') ));

		// set post data
		$PostData=array(
			'model' => array(
				'modelName' => $name,
				'interFace' => array(
					'type' => 'interface',
					'version' => 1,
					'modelId' => '',
					'interfaceId' => '',
					'name' => $name,
					'path' => array()			
				), 
				'inParams' => $inParams,
				'outParams' => $outParams
			),
			'resources' => $models,
			'connectors' => $relations,
			'space' => 'USERS',
			'folder' => array(
				'type' => 'folder',
				'name' => $group->getUnixName(),
				'path' => array()
			),
			'description' => $description
		);
		
		$this->CURL->opts(array( 
			CURLOPT_POSTFIELDS => $this->CURL->format_data(array('data'=>json_encode($PostData)))
		));

		// execute query

		$ReturnData = json_decode( $this->CURL->execPost() );
		
		if ($this->Debug){
			echo '<b>Requesting:</b> '.$this->URL.'putIntegrationModel';
			echo '<br />with post parameters:'.$this->CURL->format_data(array('data'=>$PostData));
			echo '<br /><b>Response</b>: ';
			var_dump($ReturnData);
			echo '<br /><br />';
		}
		
		return $ReturnData;
	}
	
    /**
     * Checks if the DOME server is alive
     *
     * @return bool
     */
    public function checkOK(){
        $this->CURL->opts(array(
            CURLOPT_URL=>$this->URL."getCEEDStatus?cmd=isalive"
        ));

        return $this->CURL->exec(true)?true:false;
    }

    /**
     * Turns the service off
     *
     * @return bool
     */
    public function shutdown(){
        $this->CURL->opts(array(
            CURLOPT_URL=>$this->URL."getCEEDStatus?cmd=shutdown"
        ));

        return $this->CURL->exec(true)?true:false;
    }

    /**
     * Turns the service on
     *
     * @return bool
     */
    public function startup(){
        $this->CURL->opts(array(
            CURLOPT_URL=>$this->URL."getCEEDStatus?cmd=startup"
        ));

        return $this->CURL->exec(true)?true:false;
    }
}
?>
