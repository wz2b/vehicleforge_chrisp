<?php
class WikiPage  {
    private $ID, $Data=null;

    public function __construct($ID=null, $Result=null){
        $this->Data=$Result;
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data==null){
            $Result=db_query_params("SELECT * FROM wiki_pages WHERE page_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the id of the wiki page
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }
    
    /**
     * Gets the id of the category of this wiki page
     *
     * @return int|null
     */
    public function getCatID(){
        $Result=db_query_params("SELECT cat_id FROM wiki_pages_wiki_categories_join WHERE page_id=$1 LIMIT 1",array($this->getID()));
        $ResultArr = db_fetch_array($Result,null);
        return $ResultArr['cat_id'];
    }
    
    public function getCatName(){
        $Result=db_query_params("SELECT name FROM wiki_categories WHERE cat_id=$1 LIMIT 1",array($this->getCatID()));
        $ResultArr = db_fetch_array($Result,null);
        return $ResultArr['name'];
    }

    /**
     * Gets the group this wiki belongs to
     *
     * @return int
     */
    public function getGroupID(){
        $this->fetchData();
        return (int)$this->Data['group_id'];
    }

    /**
     * Gets the name of the wiki page
     *
     * @return string
     */
    public function getPageName(){
        $this->fetchData();
        return $this->Data['name'];
    }

    /**
     * Gets the content of the page
     *
     * @return string
     */
    public function getPageContents(){
        $this->fetchData();
        return $this->Data['contents'];
    }

    /**
     * Gets when the page was created
     *
     * @return int
     */
    public function getCreationTime(){
        $this->fetchData();
        return (int)$this->Data['creation_time'];
    }

    /**
     * Gets when the page was last changed
     *
     * @return int
     */
    public function getModifiedTime(){
        $this->fetchData();
        return (int)$this->Data['modified_time'];
    }

    /**
     * Get who created the page
     *
     * @return int
     */
    public function getUserID(){
        $this->fetchData();
        return (int)$this->Data['user_id'];
    }

    /**
     * Gets who updated the page last
     *
     * @return int
     */
    public function getModifiedUserID(){
        $this->fetchData();
        return (int)$this->Data['modified_user_id'];
    }

    /**
     * Creates a new wiki page
     * Returns the new page_id if successful
     *
     * @static
     * @param int $GroupID
     * @param int $UserID
     * @param string $PageName
     * @param string $Contents
     * @return bool|int
     */
    static public function create($GroupID, $UserID, $PageName, $CategoryID, $Contents=''){
        if ($Result=db_query_params("INSERT INTO wiki_pages(group_id,user_id,name,contents,creation_time) VALUES($1,$2,$3,$4,$5)",array($GroupID, $UserID, $PageName, $Contents, time()))){
            $newPageID = (int)db_insertid($Result,'wiki_pages','page_id');
            db_query_params("INSERT INTO wiki_pages_wiki_categories_join(cat_id,page_id) VALUES($1,$2)",array($CategoryID, $newPageID));
            return $newPageID;
        }

        return false;
    }

    /**
     * Updates a page
     *
     * @param int $GroupID
     * @param int $ModifiedUser
     * @param string $PageName
     * @param string $Contents
     * @return int
     */
    public function update($GroupID, $ModifiedUser, $PageName, $CategoryID, $Contents){
        $Result1 = db_query_params("UPDATE wiki_pages SET group_id=$1, name=$2, contents=$3, modified_time=$4, modified_user_id=$5 WHERE page_id=$6",array($GroupID, $PageName, $Contents, time(), $ModifiedUser, $this->getID()));
        $Result2 = db_query_params("UPDATE wiki_pages_wiki_categories_join SET cat_id=$1 WHERE page_id=$2",array($CategoryID, $this->getID()));
        return $Result1 && $Result2;
    }

    /**
     * Permanently deletes a page
     *
     * @return int
     */
    public function delete(){
        $Result1 = db_query_params("DELETE FROM wiki_pages WHERE page_id=$1",array($this->getID()));
        $Result2 = db_query_params("DELETE FROM wiki_pages_wiki_categories_join WHERE page_id=$1",array($this->getID()));
        return $Result1 && $Result2;
    }
}
?>