<?php 
require_once 'SolrClient.class.php';

class SolrServiceClient extends SolrClient {
  
  const JAVASCRIPT_INC = '/include/scripts/SolrServiceClient.js';
  
  public function __construct($solrURL, $solrPort, $solrPath) {
    parent::__construct($solrURL, $solrPort, $solrPath, 'services');
  }
  
  public function updateCoreQueryParams(&$queryParams) {
    $queryParams['qf'] = 'interface_name';
  }
  
  public function writeJavaScriptInclude() {
    echo '<script type="text/javascript" src="'.self::JAVASCRIPT_INC.'"></script>'.PHP_EOL;
  }
  
  public function writeModal() {
    echo '<!--BEGIN: Services Modal-->'.PHP_EOL;
    echo '<div class="modal hide fade" id="serviceModal">'.PHP_EOL;
    echo '  <div class="modal-header">'.PHP_EOL;
    echo '      <a class="close" data-dismiss="modal">×</a>'.PHP_EOL;
    echo '      <h3 id="service_name"></h3>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-body">'.PHP_EOL;
    echo '    <div class="row">'.PHP_EOL;
    echo '      <div style="margin:10px;">'.PHP_EOL;
    echo '        <div id="service_desc"></div>'.PHP_EOL;
    echo '          <br/><div class="row-fluid">'.PHP_EOL;
    echo '          <div class="span6">'.PHP_EOL;
    echo '            <p style="border-bottom: 1px solid #666"><strong>Inputs</strong></p>'.PHP_EOL;
    echo '            <ul id="service_inputs"></ul>'.PHP_EOL;
    echo '          </div>'.PHP_EOL;
    echo '          <div class="span6">'.PHP_EOL;
    echo '            <p style="border-bottom: 1px solid #666"><strong>Outputs</strong></p>'.PHP_EOL;
    echo '            <ul id="service_outputs"></ul>'.PHP_EOL;
    echo '          </div>'.PHP_EOL;
    echo '        </div>'.PHP_EOL;
    echo '      </div>'.PHP_EOL;
    echo '    </div>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-footer">'.PHP_EOL;
    if (session_loggedin()) {
      echo '    <a id="subscribe_to_service" href="#" class="btn btn-primary">Subscribe to Service</a>'.PHP_EOL;
      echo '    <a id="service_url" href="#" class="btn">View Service</a>'.PHP_EOL;
    } else {
      echo '    <p>You must <a href="/account/login">login</a> to use this service.</p>'.PHP_EOL;
    }
    echo '  </div>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '<!--END: Services Modal-->'.PHP_EOL;

  }
  
}

?>