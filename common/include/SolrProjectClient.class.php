<?php 
require_once 'SolrClient.class.php';

class SolrProjectClient extends SolrClient {
  
  const JAVASCRIPT_INC = '/include/scripts/SolrProjectClient.js';
  
  public function __construct($solrURL, $solrPort, $solrPath) {
    parent::__construct($solrURL, $solrPort, $solrPath, 'projects');
  }
  
  public function updateCoreQueryParams(&$queryParams) {
    $queryParams['qf'] = 'group_name';
    $queryParams['fq'] = 'is_public_b:true';
  }
  
  public function writeJavaScriptInclude() {
    echo '<script type="text/javascript" src="'.self::JAVASCRIPT_INC.'"></script>'.PHP_EOL;
  }
  
  public function writeModal() {
    echo '<!--BEGIN: Project Modal-->'.PHP_EOL;
    echo '<div class="modal hide fade out" id="projectModal">'.PHP_EOL;
    echo '  <div class="modal-header">'.PHP_EOL;
    echo '    <a class="close" data-dismiss="modal">×</a>'.PHP_EOL;
    echo '    <h2 id="project_name"></h2>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-body">'.PHP_EOL;
    echo '    <div class="row">'.PHP_EOL;
    echo '      <div style="margin:10px;">'.PHP_EOL;
    echo '        <img id="project_image" src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">'.PHP_EOL;
    echo '        <p id="project_desc"></p>'.PHP_EOL;
    echo '      </div>'.PHP_EOL;
    echo '    </div>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '  <div class="modal-footer">'.PHP_EOL;
    echo '    <a id="project_url" href="#" class="btn">View Project</a>'.PHP_EOL;
    echo '  </div>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '<!--END: Project Modal-->'.PHP_EOL;    
  }
  
}

?>