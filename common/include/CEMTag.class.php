<?php
class CEMTag extends Error  {
    private $Data=null, $ID=null;

    function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetch_data(){
        if ($this->ID && $this->Data===null){
            if ($Result=db_query_params('SELECT * FROM cem_tags WHERE cem_tag_id=$1', array($this->getID()))){
                $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
                return true;
            }else{
                throw new Exception('Cannot fetch data');
            }
        }else{
            return false;
        }
    }

    function getID(){
        return $this->ID;
    }

    function getName(){
        $this->fetch_data();
        return $this->Data['tag_name'];
    }

    /**
     * create - Creates a new tag (if necessary) and returns the tag ID
     *
     * @static
     * @param $TagName
     * @return bool|int
     */
    static function create($TagName){
        if ($Result=db_query_params("SELECT cem_tag_id FROM cem_tags WHERE tag_name=$1", array($TagName))){
            if (db_numrows($Result)){
                //Use existing tag
                $Row=db_fetch_array($Result,null,PGSQL_ASSOC);
                return (int)$Row['cem_tag_id'];
            }else{
                //Create new
                if ($Result=db_query_params("INSERT INTO cem_tags(tag_name) VALUES($1)", array($TagName))){
                    return (int)db_insertid($Result,'cem_tags','cem_tag_id');
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }

    /**
     * search - Searches for a tag starting with $Search
     *
     * @static
     * @param $Search
     * @return array|bool
     */
    static function search($Search/*, $GroupID=null*/){
        $Search.='%';
        $SQL="SELECT * FROM cem_tags WHERE tag_name LIKE $1";
        if ($Result=db_query_params($SQL, array($Search))){
            $Return=array();

            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
                $Tag=new CEMTag($Row['cem_tag_id']);
                //if ($Tag->getGroupID()==$GroupID){
                    $Return[]=$Tag;
                //}
            }

            return $Return;
        }else{
            return false;
        }
    }

    /**
     * Returns if a tag currently exists or not
     *
     * @static
     * @param string|int $TagNameOrID
     * @return bool
     */
    static public function exists($TagNameOrID){
        if (is_int($TagNameOrID)){
            $Result=db_query_params("SELECT cem_tag_id FROM cem_tags WHERE cem_tag_id=$1", array($TagNameOrID));
        }elseif (is_string($TagNameOrID)){
            $Result=db_query_params("SELECT cem_tag_id FROM cem_tags WHERE tag_name=$1", array($TagNameOrID));
        }else{
            return false;
        }
        return (db_numrows($Result))?true:false;
    }
}
?>