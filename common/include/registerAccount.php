<?php
require_once 'env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/session.php';
require_once $gfcommon.'include/account.php';
require_once $gfcommon.'include/utils.php';
require_once $gfcommon.'include/escapingUtils.php';

//print_r($_SERVER);

function registerAccount() {
  $unix_name = "SSO".$_SERVER['HTTP_SM_UNIVERSALID'];
  $firstname = $_SERVER['HTTP_SM_GIVENNAME'];
  $lastname = $_SERVER['HTTP_SM_SN'];
  $password1 = "bogusPassword";
  $password2 = "bogusPassword";
  $email = $_SERVER['HTTP_SM_MAIL'];
  $mail_site = 0;  //$_SERVER['HTTP_SM_PHYSICALDELIVERYOFFICENAME'];
  $mail_va = 0;
  $language_id = 1;
  $timezone = "";
  $jabber_address = "";
  $jabber_only = "";
  $theme_id = 1;
  $address = "";
  $address2 = "";
  $phone = $_SERVER['HTTP_SM_TELEPHONENUMBER'];
  $fax = "";
  $title = "";
  $ccode = $_SERVER['HTTP_SM_COUNTRY'];
  $send_mail = "";
  
/*
echo('EMAIL = '.$email."\n");
echo('SSO = '.$unix_name."\n");
echo('First Name = '.$lastname."\n");
echo('Last Name = '.$firstname."\n");
echo('Phone = '.$phone."\n");
echo('Country Code = '.$ccode."\n");
*/

  $current_user = user_get_object_by_name($unix_name);

  if(!$current_user) {
    $activate_immediately=1;
    // if (($activate_immediately == 1) && forge_check_global_perm ('forge_admin')) {
    //if (($activate_immediately == 1)) {
    $send_mail = false;
    $activate_immediately = true;
    //  } else {
    //    $send_mail = true;
    //    $activate_immediately = false;
    //  }

    $new_user = new GFUser();
    $register = $new_user->create($unix_name,$firstname,$lastname,$password1,$password2,
				  $email,$mail_site,$mail_va,$language_id,$timezone,$jabber_address,$jabber_only,$theme_id,'',
				  $address,$address2,$phone,$fax,$title,$ccode,$send_mail);
    if ($register) {
      if ($activate_immediately) {
	$new_user->setStatus('A');
      }
    }
    $current_user=$new_user;
  }
  session_set_new($current_user->getID());
}
?>