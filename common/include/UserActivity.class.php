<?php
//require_once 'SiteActivity.class.php';

class UserActivity extends SiteActivity{
	private $UserID;

	function __construct($UserID){
		$this->UserID=$UserID;
		$this->Me=user_get_object($UserID);
	}

	/**
	 * getData() - Gets all comments on a user's page
	 * @return bool|array
	 */
	function getCommentData(){
		$Return=array();
		if ($Result=db_query_params("SELECT * FROM site_activity WHERE object_id=$1 AND type_id=$2 ORDER BY time_posted DESC",array($this->UserID,1))){
			while($Row=db_fetch_array($Result)){
				$Return[]=$Row;
			}

			return $Return;
		}else{
			$this->setError("Unable to get user activity");
			return false;
		}
	}

	/**
	 * getCommentsContent() - Gets the content of the comments on a user's page
	 * @return void
	 */
	function getCommentContent(){

		$Data=$this->getCommentData();
		echo '<div id="activity">';
		if (sizeof($Data)){
			foreach($Data as $i){
				echo $this->formatData($i);
			}
		}else{
			echo '<strong id="no_comments">Nobody has commented on this profile yet. Be the first!</strong>';
		}
		echo '</div>';
	}


    /**
     * getActivityData() - Gets the data that the user has created
     * @param int $Limit
     * @return array|bool
     */
	function getActivityData($Limit=20){
		if ($Result=db_query_params("SELECT * FROM site_activity WHERE user_id=$1 ORDER BY time_posted DESC LIMIT $2",array($this->UserID, $Limit))){
			$Return=array();
			while($Row=db_fetch_array($Result)){
				$Return[]=$Row;
			}
			return $Return;
		}else{
			$this->setError("Unable to fetch user activity data");
			return false;
		}
	}

    /**
     * getActivityContents() - Gets all activity created byt he user and displays it
     * @param int $Limit
     * @return void
     */
	function getActivityContent($Limit=20){
		$Data=$this->getActivityData($Limit);

		$Return='';
		foreach($Data as $i){
			if ($i['type_id']==1){
				$Object=user_get_object($i['object_id']);
			}else{
				$Object=group_get_object($i['object_id']);
			}

			switch($i['type_id']){
				case 1:
					$Return.='<img src="/images/icons/comment_user.png" /> Commented on ';
					if ($this->UserID==$i['object_id']){
						$Return.=' their own';
					}else{
						$Return.='<a href="'.$Object->getURL().'">'.$Object->getRealName()."'s</a>";
					}

					$Return.=' profile';
					break;

				case 2:
					$Return.='<img src="/images/icons/comment_project.png" /> Commented on <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
					break;

                case 4:
                    $Return.='Posted news';
                    break;

				case 7:
					$Return.='Created a new bug in <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
					break;

                case 8:
                    $Return.='Created a new task in <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
                    break;

                case 9:
                    $Return.='Updated a task in <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
                    break;

                case 10:
                    $Return.='Created a new project: <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
                    break;

                case 11:
                    $Return.='Updated a bug';
                    break;

                case 12:
                    $Return.='Closed a bug';
                    break;

                case 13:
                    $Return.='Created a new component <a href="'.$Object->getURL().'">'.$Object->getPublicName().'</a>';
                    break;

				default:
					$Return.='Unknown or unfinished type_id='.$i['type_id'];
			}

			$Return.='<br />';
		}

		echo $Return;
	}
}
?>
