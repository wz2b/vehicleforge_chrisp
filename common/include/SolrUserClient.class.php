<?php 
require_once 'SolrClient.class.php';

class SolrUserClient extends SolrClient {
  
  const JAVASCRIPT_INC = '/include/scripts/SolrUserClient.js';
  
  public function __construct($solrURL, $solrPort, $solrPath) {
    parent::__construct($solrURL, $solrPort, $solrPath, 'users');
  }
  
  public function updateCoreQueryParams(&$queryParams) {
    $queryParams['qf'] = 'realname';
  }
  
  public function writeJavaScriptInclude() {
    echo '<script type="text/javascript" src="'.self::JAVASCRIPT_INC.'"></script>'.PHP_EOL;
  }
  
  public function writeModal() {
    echo '<div class="modal hide fade" id="userModal">'.PHP_EOL;
    echo '<div class="modal-header">'.PHP_EOL;
    echo '<button class="close" data-dismiss="modal">'.PHP_EOL;
    echo '</button>'.PHP_EOL;
    echo '<h3></h3>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '<div class="modal-body">'.PHP_EOL;
    echo '<div class="image pull-left"></div>'.PHP_EOL;
    echo '<div class="about pull-right">'.PHP_EOL;
    echo '<h6>About</h6>'.PHP_EOL;
    echo '<p class="about_me"></p>'.PHP_EOL;
    echo '<h6>Skills</h6>'.PHP_EOL;
    echo '<p class="skills"></p>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '<div class="modal-footer">'.PHP_EOL;
    echo '<a href="#" class="pull-right btn" id="profile_link">Visit Profile</a>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
    echo '</div>'.PHP_EOL;
  }
  
}

?>