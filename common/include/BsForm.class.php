<?php
/*
 * @author Jason Kaczmarsky
 */
class FormUtils {
    protected function parseAttribs($Attribs){
        $Code='';

        foreach($Attribs as $key=>$val){
            switch ($key){
                case 'checked':
                    if ($val)
                        $Code.=' checked="checked"';
                    break;

                case 'autocomplete':
                    if (!$val)
                        $Code.=' autocomplete="off"';
                    break;

                case 'required':
                    if ($val)
                        $Code.=' required="required"';
                    break;

                case 'multiple':
                    if ($val)
                        $Code.=' multiple="multiple"';
                    break;

                default:
                    $Code.=' '.$key.'="'.$val.'"';
            }
        }

        return $Code;
    }

    protected function getPend1($Attribs){
        if (isset($Attribs['prepend']))
            return '<div class="input-prepend"><span class="add-on">'.$Attribs['prepend'].'</span>';

        if (isset($Attribs['append']))
            return '<div class="input-append">';
    }

    protected function getPend2($Attribs){
        if (isset($Attribs['append']))
            return '<span class="add-on">'.$Attribs['append'].'</span></div>';

        if (isset($Attribs['prepend']))
            return '</div>';
    }

    protected function getHelpText($HelpText){
        if ($HelpText)
            return '<p class="help-block">'.$HelpText.'</p>';
    }
}

class BsForm extends FormUtils {
    private $Code='', $UseHead=false, $Objs=array(), $Inline=false;

    public function init($Action='#', $Method='POST', $Attribs=array()){
        $this->Code.='<form action="'.$Action.'" method="'.strtoupper($Method).'"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.='>';

        if (isset($Attribs['class'])){
            if(strpos($Attribs['class'],'form-inline'))
                $this->Inline=true;
        }

        return $this;
    }

    public function head($Title){
        $this->UseHead=true;

        $this->Code.='<fieldset><legend>'.$Title.'</legend>';

        return $this;
    }
    
    public function render_fieldset(){
    
    	$this->Code.='</fieldset>';
    
    	return $this;
    }
    
    
    public function right(){
    	$this->Code.='<form-right>';
    
    	$Inputs=func_get_args();
    	for($i=0, $Size=count($Inputs); $i<$Size; $i++){
    		$this->Code.=$Inputs[$i]->render();
    		$this->Objs[]=$Inputs[$i];
    	}
    
    	$this->Code.='</form-right>';
    
    	return $this;
    }
    

    /**
     * @param string $Label
     * @param array|object $Attribs
     * @return BsForm
     */
    public function group($Label='',$Attribs){
        if (!$this->Inline){
            $A=array();
            //var_dump($Attribs);
            $Start=1;
            if (is_array($Attribs)){
                $Start=2;
                $A=$Attribs;

                if (isset($Attribs['class']))
                    $A['class']='control-group '.$Attribs['class'];
            }else{
                $A['class']='control-group';
            }

            $this->Code.='<div'.parent::parseAttribs($A).'>';
            $Inputs=func_get_args();

            if (is_object($Inputs[0])){
                echo '<b style="color:red">First parameter is an object. Did you want this to be an inline form?</b>';
                exit;
            }

            $Size=count($Inputs)-2;

            if ($Label)
                $this->Code.='<label class="control-label">'.$Label.'</label>';
            $this->Code.='<div class="controls';

            if ($Size>2)
                $this->Code.=' grouping';

            $this->Code.='">';

            for($i=$Start; $i<$Size+2; $i++){
                $this->Code.=$Inputs[$i]->render();
                $this->Objs[]=$Inputs[$i];
            }

            $this->Code.='</div></div>';
        }else{
            $Inputs=func_get_args();
            $Size=count($Inputs);

            for($i=0; $i<$Size; $i++){
                $this->Code.=$Inputs[$i]->render();
                $this->Objs[]=$Inputs[$i];
            }
        }

        return $this;
    }

    public function hidden_group(){
        $this->Code.='<div class="hidden">';
        $Inputs=func_get_args();
        $Size=count($Inputs);
        for($i=0; $i<$Size; $i++){
            $this->Code.=$Inputs[$i]->render();
            $this->Objs[]=$Inputs[$i];
        }

        $this->Code.='</div>';

        return $this;
    }

    public function actions(){
        $this->Code.='<div class="form-actions">';

        $Inputs=func_get_args();
        for($i=0, $Size=count($Inputs); $i<$Size; $i++){
            $this->Code.=$Inputs[$i]->render();
            $this->Objs[]=$Inputs[$i];
        }

        $this->Code.='</div>';

        return $this;
    }

    public function render($Cleanup=true){
        if ($this->UseHead)
            $this->Code.='</fieldset>';
        $this->Code.='</form>';

        $C=$this->Code;

        if ($Cleanup){
            foreach($this->Objs as $i){
                unset($i);
                $this->Code='';
                $this->Objs=array();
                $this->UseHead=false;
            }
        }

        return $C;
    }
}

class Text extends FormUtils  {
    private $Code='';

    public function __construct($Attribs=array()){
        $this->Code.=parent::getPend1($Attribs);
        $this->Code.='<input type="text"'.parent::parseAttribs($Attribs).' />';
        $this->Code.=parent::getPend2($Attribs);
    }

    function render(){
        return $this->Code;
    }
}

class Hidden extends FormUtils   {
    private $Code='';

    public function __construct($Attribs=array()){
        $this->Code.='<input type="hidden"'.parent::parseAttribs($Attribs).' />';
    }

    function render(){
        return $this->Code;
    }
}

class Password extends FormUtils    {
    private $Code='';

    public function __construct($Attribs=array()){
        $this->Code.=parent::getPend1($Attribs);
        $this->Code.='<input type="password"'.parent::parseAttribs($Attribs).' />';
        $this->Code.=parent::getPend2($Attribs);
    }

    function render(){
        return $this->Code;
    }
}

class Checkbox extends FormUtils    {
    private $Code='';

    public function __construct($Label='', $Attribs=array(), $Inline=false){
        $this->Code.='<label class="checkbox';
        if ($Inline)
            $this->Code.=' inline';
        $this->Code.='"><input type="checkbox"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.=' />'.$Label.'</label>';

        return $this;
    }

    function render(){
        return $this->Code;
    }
}

class Radio extends FormUtils   {
    private $Code;

    public function __construct($Label='', $Attribs=array(), $Inline=false){
        $this->Code.='<label class="radio';
        if ($Inline)
            $this->Code.=' inline';
        $this->Code.='"><input type="radio"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.=' />'.$Label.'</label>';
    }

    function render(){
        return $this->Code;
    }
}

class Dropdown extends FormUtils    {
    private $Code='';

    public function __construct($Options=array(), $Selected=null, $Attribs=array()){
        $this->Code.='<select ';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.='>';

        $Multiple=is_array($Selected);

        foreach($Options as $key=>$val){
            $this->Code.='<option value="'.$key.'"';
            if (!$Multiple){
                if ($Selected==$key)
                    $this->Code.=' selected';
            }else{
                if (in_array($key,$Selected))
                    $this->Code.=' selected';
            }
            $this->Code.='>'.$val.'</option>';
        }

        $this->Code.='</select>';
    }

    function render(){
        return $this->Code;
    }
}

class Help  {
    private $Code='';

    public function __construct($Text){
        $this->Code.='<p class="help-block">'.$Text.'</p>';
    }

    function render(){
        return $this->Code;
    }
}

class Textarea extends FormUtils    {
    private $Code='';

    public function __construct($Content='', $Attribs=array()){
        $this->Code.='<textarea';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.='>'.$Content.'</textarea>';
    }

    public function render(){
        return $this->Code;
    }
}

class File extends FormUtils    {
    private $Code='';

    public function __construct($Attribs=array()){
        $this->Code.='<input type="file"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.=' />';
    }

    function render(){
        return $this->Code;
    }
}

class Button extends FormUtils  {
    private $Code='';

    public function __construct($Label='', $Attribs=array()){
        $this->build($Label, $Attribs);
    }

    protected function build($Label='Button', $Attribs=array()){
        if (!isset($Attribs['class']))
            $Attribs['class']='btn';
        else
            $Attribs['class']='btn '.$Attribs['class'];

        if (!isset($Attribs['type']))
            $Attribs['type']='button';

        $this->Code.='<button type="'.$Attribs['type'].'"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.='>'.$Label.'</button>';
    }

    function render(){
        return $this->Code;
    }
}

class Submit extends Button {
    public function __construct($Label='Submit',$Attribs=array('class'=>'btn-primary')){
        $Attribs['type']="submit";
/*        if (!isset($Attribs['class']))
            $Attribs['class']='btn-primary';
        else
            $Attribs['class'].=' btn-primary';*/
        parent::build($Label, $Attribs);
    }
}

class Reset extends Button  {
    public function __construct($Label='Reset',$Attribs=array()){
        $Attribs['type']="reset";
        parent::build($Label, $Attribs);
    }
}

class Custom extends FormUtils  {
    private $Code='';

    public function __construct($Content, $Attribs=array()){
        if (isset($Attribs['class']))
            $Attribs['class'].=' input-custom';
        else
            $Attribs['class']='input-custom';

        $this->Code.='<div'.parent::parseAttribs($Attribs).'>'.$Content.'</div>';
    }

    public function render(){
        return $this->Code;
    }
}

class ButtonGroup extends FormUtils   {
    private $Code='';

    public function __construct(){
        $this->Code.='<div class="btn-group">';

        $Inputs=func_get_args();
        for($i=0, $Size=count($Inputs); $i<$Size; $i++){
            $this->Code.=$Inputs[$i]->render();
        }

        $this->Code.='</div>';
    }

    public function render(){
        echo $this->Code;
    }
}

class BGButton extends FormUtils  {
    private $Code='';

    public function __construct($Icon='cog', $Attribs=array()){
        $this->Code.='<a class="btn" href="javascript:void(0)"';
        $this->Code.=parent::parseAttribs($Attribs);
        $this->Code.='><i class="icon-'.$Icon.'"></i></a>';
    }

    public function render(){
        return $this->Code;
    }
}

class ButtonDropdown extends FormUtils  {
    private $Code='';

    public function __construct($Label='', $Dropdown=array()){
        $this->Code.='<div class="btn-group"><a class="btn" href="javascript:void(0)">'.$Label.'</a><a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)"><span class="caret"></span></a><ul class="dropdown-menu">';
        foreach($Dropdown as $i){
            $this->Code.='<li><a href="javascript:void(0)" id="'.$i[0].'">';
            if ($i[1])
                $this->Code.='<i class="icon-'.$i[1].'"></i>';
            $this->Code.=$i[2];
            $this->Code.='</a>';
        }
        $this->Code.='</ul></div>';
    }

    public function render(){
        return $this->Code;
    }
}
?>