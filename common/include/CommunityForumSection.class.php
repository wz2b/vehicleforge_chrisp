<?php
class CommunityForumSection    {

    static public function getSections(){
        $Result=db_query_params("SELECT * FROM community_forum_sections");
        return util_result_columns_to_array($Result);
    }

    static public function getForums($SectionID=-1){
        $Return = array();
		
		if($SectionID > 0){
        	$Result=db_query_params("SELECT forum_id FROM community_forum_sections_join WHERE section_id=$1", array($SectionID));
		} else{
			$Result=db_query_params("SELECT forum_id FROM community_forum_sections_join");
		}
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Return[]=new Forum($Row['forum_id']);

        return $Return;
    }

    static public function getSectionByForum($ForumID){
        $Result=db_query_params("SELECT community_forum_sections.* FROM community_forum_sections_join, community_forum_sections WHERE community_forum_sections_join.forum_id=$1 AND community_forum_sections_join.section_id=community_forum_sections.section_id",array($ForumID));

        return db_fetch_array($Result,null,PGSQL_ASSOC);
    }

    static public function getSection($SectionID){
        $Result=db_query_params("SELECT * FROM community_forum_sections WHERE section_id=$1",array($SectionID));

        return db_fetch_array($Result,null,PGSQL_ASSOC);

    }
}
?>