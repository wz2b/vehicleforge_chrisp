<?php
class QueryBuilder extends Error    {
	private $ParamCount=0, $Params=array(), $QueryString='';

	function addParam($ParamName,$ParamVal){
		$this->ParamCount++;
		$this->Params[]=$ParamVal;

		$this->QueryString.=$ParamName.'=$'.$this->ParamCount;

		return $this;
	}

	function addParamArray($ParamName, $Array, $Append=''){
		$this->addSql('(');

		for($i=0,$Count=sizeof($Array);$i<$Count;$i++){
			if ($i==$Count-1){
				$this->addParam($ParamName,$Array[$i]);
			}else{
				$this->addParam($ParamName,$Array[$i])->addSql($Append);
			}
		}

		$this->addSql(')');

		return $this;
	}

	function addSql($Statement){
		$this->QueryString.=$Statement;

		return $this;
	}

	public function clear(){
		$this->ParamCount=0;
		$this->Params=array();
		$this->QueryString='';
	}

	function getQuery(){
		return $this->QueryString;
	}

	function getParams(){
		return $this->Params;
	}

	function getQueryVals(){
		return pg_query_params_return_sql($this->QueryString,$this->Params);
	}
}
?>
