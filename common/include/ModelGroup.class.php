<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:17 PM 11/23/11
 */

require_once $gfwww.'include/model.php';

class ModelGroup extends Group{
	private $GroupID;

	function ModelGroup($GroupID){
		$this->GroupID=$GroupID;
		$Res=db_query_params("SELECT * FROM groups WHERE group_id=$1",array($GroupID));
		$Res=db_fetch_array($Res);
		if ($Res['group_type']!=2){
			$this->setError(_("Group is not a model group type"));
			return false;
		}else{
			//Get data from DB, populate it in $data_array
			$this->fetchData($GroupID);
		}

		return true;
	}

	/**
	 * getModel() - Gets the ID of a linked model
	 *
	 * @return int
	 */
	function getModel(){
		$Res=db_query_params("SELECT model_id FROM groups WHERE group_id=$1",array($this->GroupID));
		$Res=db_fetch_array($Res);

		return $Res['model_id'];
	}
}
?>