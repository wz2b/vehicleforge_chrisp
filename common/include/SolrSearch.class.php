<?php
class SolrSearch {
    static private $Proxy, $Curl, $Cores=array(
        'components',
        'projects',
        'users',
        'services'
    );

    /**
     * @static
     * @param string $Core
     * @param string $SearchTerm
     * @param string $SearchField
     * @param int $Start
     * @param int $Rows
     * @return array|bool
     */
    static public function search($Core, $SearchTerm, $SearchField, $Start=0, $Rows=10){
        if (!self::$Curl){
            self::$Curl=new curl;
	    //            self::$Proxy=$_SERVER['SERVER_NAME'].'/solrSearch/proxy.php?version=2.2&indent=on&defType=edismax&wt=json';
            self::$Proxy=$_SERVER['SERVER_NAME'].'/solrSearch/proxy.php?defType=edismax&dataType=json';
        }

        if (in_array($Core, self::$Cores)){
            $URL=self::$Proxy.'&q='.$SearchTerm.'&core='.$Core.'&qf='.$SearchField.'&start='.$Start.'&rows='.$Rows;

            self::$Curl->opts(array(
				    CURLOPT_URL             => $URL,
				    CURLOPT_AUTOREFERER     => true
				    ));
	    var_dump($URL);

            $Data=self::$Curl->exec();
	    var_dump($Data);
            if ($Data){
                $Data=json_decode($Data);
		var_dump($Data);
                if (is_object($Data)){
                    return $Data->response->docs;
                }else{
                    return $Data['response']['docs'];
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
?>