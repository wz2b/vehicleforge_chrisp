<?php
//require_once $gfcommon.'include/SystemCategory.class.php';

class CEM extends Error  {
    private $ID=null, $Data=null;

    function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetch_data(){
        if ($this->ID!==null && $this->Data===null){
            if ($Result=db_query_params("SELECT * FROM cem_objects WHERE cem_id=$1",array($this->getID()))){
                $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
            }else{
                throw new Exception('Unable to fetch data');
            }
        }
    }

    /**
     * create - Creates a new cem_object in the database and returns the cem_id of the new cem_object
     * Returns:
     *      Success: (int) cem_id
     *      Failure: false
     *
     * @param int $GroupID
     * @param string $Name
     * @param int|null $ParentCemID
     * @return bool|int
     */
	static function create($GroupID, $Name, $ParentCemID=0){
        if (!is_int($GroupID) || !is_string($Name) || !is_int($ParentCemID)){
            //$this->setError('Wrong parameter types');
            return false;
        }

		db_begin();

        if ($Result=db_query_params("INSERT INTO cem_objects(group_id, name, parent_cem_id) VALUES($1,$2,$3)",array($GroupID, $Name, $ParentCemID))){
            $CemID=db_insertid($Result,'cem_objects','cem_id');
            if ($CemID){
                /*if ($ParentCemID){
                    //Create a row in the children table
                    db_query_params("INSERT INTO cem_children(cem_id,child_id) VALUES($1,$2)", array($ParentCemID, $CemID));
                }*/

                db_commit();
/*
                if (!$UserID)
                    $UserID=user_getid();

                SiteActivity::create($UserID,'project_component_add',$GroupID,$CemID);*/

                return (int)$CemID;
            }else{
                //$this->setError('Cannot fetch cem id');
                return false;
            }
        }else{
            db_rollback();
            //$this->setError('Cannot create new component');
            return false;
        }
	}

    /**
     * getID - Returns the cem_id
     *
     * @return int|null
     */
	function getID(){
		return $this->ID;
	}

    /**
     * getName - Returns the name on the component
     *
     * @return string
     */
	function getName(){
        $this->fetch_data();

		return $this->Data['name'];
	}

    /**
     * Replaces invalid characters in the component name
     *
     * @return string
     */
    public function getUnixName(){
        return str_replace(' ','-', $this->getName());
    }

    /**
     * getGroupID - Returns the group_id that the component is linked to
     *
     * @return int
     */
	function getGroupID(){
        $this->fetch_data();

		return (int)$this->Data['group_id'];
	}

	/**
	 * @return array|bool
	 *
	 * @deprecated
	 */
	function getRunnables(){
		if ($Result=db_query_params("SELECT * FROM cem_runnables WHERE cem_id=$1", array($this->getID()))){
			$Runnables = array();
			while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
				$Runnables[]= new CEMRunnable($Row['runnable_id']);
			}
			return $Runnables;
		}else{
			$this->setError('Unable to get runnable components');
			return false;
		}
	}

	/**
	 * getInterfaces - Gets all DOME interfaces associated with this component
	 * Returns:
	 *      Success: array of DOMEInterface objects
	 *      Failure: false
	 *
	 * @return array|bool
	 */
	function getInterfaces(){
		if ($Result=db_query_params("SELECT * FROM dome_interfaces WHERE cem_id=$1", array($this->getID()))){
			$Return=array();
			while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
				$Return[]=new DOMEInterface($Row['interface_id']);

			//return util_result_columns_to_array($Result);
			return $Return;
		}else{
			$this->setError('Unable to get runnable components');
			return false;
		}
	}

	function isValid(){
		return !is_null($this->Data);
	}

	function getFiles(){
		$files = array();
		$num = rand(0,3);
		for ($i = 0; $i < $num; $i++) {
			$files[] = array ("name"=>"subfolder".$i, "type"=>"folder", "updated"=>"yesterday", "message"=>"added this file", "activity"=>"...");
		}

		$num = rand(2,6);
		for ($i = 0; $i < $num; $i++) {
			$files[] = array ("name"=>"file".$i, "type"=>"file", "updated"=>"yesterday", "message"=>"added this file", "activity"=>"...");
		}
		return $files;
	}

    /**
     * getSubComponents - Gets all subcomponents of a component
     * Returns an array of CEM objects
     *
     * @return array
     */
    function getSubComponents(){
        /*$Result=db_query_params("SELECT * FROM cem_children WHERE cem_id=$1", array($this->getID()));
        $Subs=array();
        while ($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Subs[]=new CEM($Row['child_id']);

        return $Subs;*/
        $Result=db_query_params("SELECT * FROM cem_objects WHERE parent_cem_id=$1", array($this->getID()));
        $Subs=array();
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $Subs[]=new CEM($Row['cem_id']);

        return $Subs;
    }

	/**
	 * getParents - Gets all parent components of a component
	 * Returns an array of CEM objects
	 *
	 * @return array
	 */
	function getParents(){
		$Result=db_query_params("SELECT * FROM cem_children WHERE child_id=$1",array($this->getID()));
		$Parents=array();
		while ($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
			$Parents[]=new CEM($Row['child_id']);

		return $Parents;
	}

	/**
	 * getSystemCategories - Gets all system categories attached to this CEM object
	 *
	 * @return array
	 */
	function getSystemCategories(){

		$Result=db_query_params("SELECT * FROM cem_service_categories WHERE cem_id=$1", array($this->getID()));

		// Create a SystemCategory for each entry in the db
		$Systems = array();
		while ($Row=db_fetch_array($Result,null,PGSQL_ASSOC)) {
			$Systems[] = new SystemCategory($Row['cat_id']);
		}

		return $Systems;

	}

    /**
     * attachSystemCategory - Adds a system category from the component
     *
     * @param $CategoryID
     */
	function attachSystemCategory($CategoryID){
		db_query_params("INSERT INTO cem_service_categories(cem_id,cat_id) VALUES($1,$2)",array($this->getID(), $CategoryID));
	}

    /**
     * detachSystemCategory - Removes a system category from the component
     *
     * @param $CategoryID
     */
	function detachSystemCategory($CategoryID){
		db_query_params("DELETE FROM cem_service_categories WHERE cem_id=$1 AND cat_id=$2",array($this->getID(), $CategoryID));
	}

    /**
     * getTags - Returns an array of CEMTag objects
     *
     * @return array|bool
     */
    function getTags(){
        if ($Result=db_query_params("SELECT * FROM cem_tags_join WHERE cem_id=$1", array($this->getID()))){
            $Tags=array();
            while ($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Tags[] = new CEMTag($Row['cem_tag_id']);

            return $Tags;
        }else{
            return false;
        }
    }

    /**
     * attach - Attaches a tag to a CEM object
     *
     * @param $TagID
     * @return bool
     */
    function attachTag($TagID){
        if (CEMTag::exists($TagID)){
            if (db_query_params("INSERT INTO cem_tags_join(cem_id,cem_tag_id,group_id) VALUES($1, $2, $3)", array($this->getID(), $TagID, $this->getGroupID()))){
                return true;
            }else{
                $this->setError('Unable to attach tag'.db_error());
                return false;
            }
        }else{
            $this->setError('Tag ('.$TagID.') does not exist');
            return false;
        }
    }

    /**
     * detach - Removes a tag from a CEM object
     *
     * @param $TagID
     * @return bool
     */
    function detachTag($TagID){
        if (db_query_params("DELETE FROM cem_tags_join WHERE cem_id=$1 AND cem_tag_id=$2 AND group_id=$3", array($this->getID(), $TagID, $this->getGroupID()))){
            return true;
        }else{
            $this->setError('Unable to detach tag');
            return false;
        }
    }
    
    function destroy() {
    	$id = $this->getID();

        //SiteActivity::create(user_getid(),'project_component_delete',$this->getGroupID(),$id);
        SiteActivity::deleteByRefID($id);

    	// destroy sub-components (recursively)
    	foreach ($this->getSubComponents() as $c) {
    		$c->destroy();
    	}
    	
    	// Remove service category mappings
			$Result=db_query_params("DELETE FROM cem_service_categories WHERE cem_id=$1", array($id));

			// Remove tag mappings
			$Result=db_query_params("DELETE FROM cem_tags_join WHERE cem_id=$1", array($id));
			
			// Remove dome_interface mappings
			$Result=db_query_params("DELETE FROM dome_interfaces WHERE cem_id=$1", array($id));
			
			// Remove cem_children mappings to parent
			// 200018807:3/7/2013: Commented out because table cem_children is not accessible
			// $Result=db_query_params("DELETE FROM cem_children WHERE child_id=$1", array($id));

            //Remove Git ACL settings
            db_query_params("DELETE FROM cem_object_git_acl WHERE cem_id=$1",array($this->getID()));
			
			// Remvoe cem entry in cem_objects
			$Result=db_query_params("DELETE FROM cem_objects WHERE cem_id=$1", array($id));


			return true;
    }

    /**
     * getAll - Gets all components from the database
     * Returns:
     *      Success: Array of CEM objects
     *
     * TODO: Limit how many get queried
     * @return array
     */
    static function getAll(){
        $Result=db_query_params("SELECT cem_id FROM cem_objects");
        $CEMs=array();
        while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
            $CEMs[]=new CEM($Row['cem_id']);

        return $CEMs;
    }

    /**
     * copy - Copies the component to a new group
     *
     * @param $ToGroupID
     * @return bool|int
     */
    function copy($ToGroupID){
        $this->getName();

        return CEM::create($ToGroupID,$this->getName());
    }

    public function makeLink(){
        return '<a href="/components/?group_id='.$this->getGroupID().'&cid='.$this->getID().'">'.$this->getName().'</a>';
    }

    public function update($GroupID, $Name, $ParentID){
        return db_query_params('UPDATE cem_objects SET group_id=$1, name=$2, parent_cem_id=$3 WHERE cem_id=$4', array($GroupID, $Name, $ParentID, $this->getID()));
    }

    public function getParent(){
        $this->fetch_data();

        return $this->Data['parent_cem_id'];
    }

    /**
     * getRepoName - Returns the repo name for the component
     *
     * @return string
     */
    public function getRepoName(){
        $this->fetch_data();

        $Group=group_get_object($this->getGroupID());
        return $Group->getUnixName().'.'.$this->getUnixName().'-'.$this->getID().'.git';
    }

    /**
     * getRepoLastUpdate - Returns the time the repo was last updated
     *
     * If the repo state needs to be checked, used getRepoStatus()
     *
     * @return int
     */
    private function getRepoLastUpdate(){
        $this->fetch_data();

        return (int)$this->Data['last_repo_update'];
    }

    /**
     * getRepoStatus - Gets the current usable state of the repo
     * State values:
     *  1: No repo exists
     *  2: Repo will be created within 5 minutes
     *  3: Permissions changes will happen within 5 minutes
     *  4: Repo exists, nothing pending
     *
     * @return int
     */
    public function getRepoStatus(){
        $LastUpdate=$this->getRepoLastUpdate();
        $Pending=$LastUpdate+(5*60)>time();

        if (!$this->getRepoExists()){
            if ($Pending)
                return 2; //Pending creation
            else
                return 1; //No repo
        }else{
            if ($Pending)
                return 3; //Changes pending
            else
                return 4; //OK
        }
    }

    /**
     * setRepoLastUpdate - Set the time the repo was updated last
     *
     * @param int $Time
     * @return bool
     */
    public function setRepoLastUpdate($Time){
        $this->Data['last_repo_update']=$Time;

        return db_query_params("UPDATE cem_objects SET last_repo_update=$1 WHERE cem_id=$2",array($Time,$this->getID()));
    }

    /**
     * setRepoCreationTime - Set when the repo was first created
     *
     * @param int $Time
     * @return int
     */
    public function setRepoCreationTime($Time){
        $this->Data['repo_creation_time']=$Time;

        return db_query_params("UPDATE cem_objects SET repo_creation_time=$1 WHERE cem_id=$2",array($Time, $this->getID()));
    }

    /**
     * getRepoCreationTime - Get when the repo was created
     * Used to check repo status
     *
     * @return int
     */
    public function getRepoCreationTime(){
        $this->fetch_data();

        return (int)$this->Data['repo_creation_time'];
    }

    /**
     * getRepoExists - Checks if the repo is created
     * Used to check if repo doesn't exist or is being created in getRepoStatus
     *
     * @return bool
     */
    public function getRepoExists(){
        if (($this->getRepoCreationTime()+(5*60))>time() || $this->getRepoCreationTime()==0){
            return false;
        }else{
            return true;
        }
    }

    /**
     * getExportControl - Gets if the component is export controlled
     *
     * @return bool
     */
    public function getExportControl(){
        $this->fetch_data();

        return util_parse_bool($this->Data['export_control']);
    }

    /**
     * setExportControl - Sets the export control status
     *
     * @param bool $Value
     * @return bool
     */
    public function setExportControl($Value){
        $this->fetch_data();

        if ($this->getExportControl()==$Value){
            //Flag is already set to $Value
            return true;
        }else{
            if ($this->getExportControl()==true && $Value==false){
                //Cant go from off to on
                return false;
            }

            if ($this->getExportControl()==false && $Value==true){
                $Group=group_get_object($this->getGroupID());
                if ($Group->setExportControl(true))
                    return db_query_params("UPDATE cem_objects SET export_control=$1 WHERE cem_id=$2",array(util_convert_bool($Value),$this->getID()));
                else
                    return false;
            }
        }

        return false;
    }

    /**
     * Sets the component description
     *
     * @param string $Description
     * @return int
     */
    public function setDescription($Description){
        return db_query_params("UPDATE cem_objects SET description=$1 WHERE cem_id=$2",array($Description, $this->getID()));
    }

    public function getDescription(){
        $this->fetch_data();

        return $this->Data['description'];
    }
}
?>