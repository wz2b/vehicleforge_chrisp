<?php
class ForumTopic    {
    private $ID=null, $Data=null;

    public function __construct($ID=null, $Result=null){
        if ($Result)
            $this->Data=$Result;
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM forum_topics WHERE topic_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    /**
     * Gets the topic ID
     *
     * @return int|null
     */
    public function getID(){
        return $this->ID;
    }

    /**
     * Gets the topic name
     *
     * @return string
     */
    public function getName(){
        $this->fetchData();
        return $this->Data['title'];
    }

    /**
     * Gets the forum ID
     *
     * @return int
     */
    public function getForumID(){
        $this->fetchData();
        return (int)$this->Data['forum_id'];
    }

    /**
     * Gets the user ID who posted this topic
     *
     * @return int
     */
    public function getUserID(){
        $this->fetchData();
        return (int)$this->Data['user_id'];
    }

    /**
     * Gets the user object who posted this topic
     *
     * @return GFUser
     */
    public function getUser(){
        $this->fetchData();
        return user_get_object($this->getUserID());
    }

    /**
     * Gets the URL to this topic
     *
     * @param bool $Community
     * @return string
     */
    public function getURL($Community=false){
        $this->fetchData();

        if ($Community)
            return '/community/boards.php?forum='.$this->getForumID().'&topic='.$this->getID();
        else{
            return '/forum/forum.php?group_id='.$this->getGroupID().'&forum='.$this->getForumID().'&topic='.$this->getID();
        }
    }

    /**
     * Gets the group this forum belongs to
     *
     * @return int
     */
    public function getGroupID(){
        $this->fetchData();
        $Forum=new Forum($this->getForumID());
        return $Forum->getGroupID();
    }

    /**
     * Generates the HTML link to get to this topic
     *
     * @param bool $Community
     * @return string
     */
    public function makeLink($Community=false){
        return '<a href="'.$this->getURL($Community).'">'.$this->getName().'</a>';
    }

    /**
     * Gets the timestamp of the last activity in this topic
     *
     * @return int
     */
    public function getLastActivity(){
        $this->fetchData();
        return (int)$this->Data['last_activity'];
    }

    /**
     * Generates the HTML for the messages within this topic in a flat view
     *
     * @return bool|string
     */
    public function getMessagesFlat(){
        if ($Result=db_query_params("SELECT message_id FROM forum_messages WHERE topic_id=$1 ORDER BY time_posted ASC",array($this->getID()))){
            $HTML='';

            $Num=0;
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
                $Num++;
                $Message=new ForumMessage($Row['message_id']);
                $HTML.=$Message->getHTML($Num, $this->getForumID(), $this->getID());
            }

            return $HTML;
        }else{
            return false;
        }
    }

    /**
     * Generates the HTML for the messages within this topic in a threaded view
     *
     * @param bool $Community
     * @return bool|string
     */
    public function getMessagesThreaded($Community=false){
        $this->fetchData();
        if ($Result=db_query_params("SELECT message_id FROM forum_messages WHERE topic_id=$1 AND reply_to=0 ORDER BY time_posted ASC",array($this->getID()))){
            $HTML='';

            $Num=0;
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC)){
                $Num++;
                $Message=new ForumMessage($Row['message_id']);
                $HTML.=$Message->getHTML($Num, $this->getForumID(), $this->getID(), false, $Community);

                $Result2=db_query_params("SELECT message_id FROM forum_messages WHERE reply_to=$1",array($Message->getID()));
                while($Row2=db_fetch_array($Result2,null,PGSQL_ASSOC)){
                    $Num++;
                    $Message2=new ForumMessage($Row2['message_id']);
                    $HTML.=$Message2->getHTML($Num, $this->getForumID(), $this->getID(), true, $Community);
                }
            }

            return $HTML;
        }else{
            return false;
        }
    }

    /**
     * Gets the number of messages in this topic
     *
     * @return bool|int
     */
    public function getMessageCount(){
        if ($Result=db_query_params("SELECT count(message_id) FROM forum_messages WHERE topic_id=$1",array($this->getID()))){
            return (int)db_result($Result,0,'count')-1;
        }else{
            return false;
        }
    }

    /**
     * Creates a new topic
     *
     * @static
     * @param int $ForumID
     * @param string $Title
     * @param int $UserID
     * @return bool|int
     */
    static public function create($ForumID, $Title, $UserID){
        $Time=time();
        if ($Result=db_query_params("INSERT INTO forum_topics(title, user_id, time_posted, last_activity, forum_id) VALUES($1,$2, $3, $3, $4)",array($Title, $UserID, $Time, $ForumID))){
            return (int)db_insertid($Result,'forum_topics', 'topic_id');
        }else{
            return false;
        }
    }

    /**
     * Gets the ForumMessage that was last posted in this topic
     *
     * @return bool|ForumMessage
     */
    public function getLastMessage(){
        $this->fetchData();
        if ($Result=db_query_params("SELECT * FROM forum_messages WHERE topic_id=$1 ORDER BY time_posted DESC LIMIT 1",array($this->getID()))){
            $Row=db_fetch_array($Result,null,PGSQL_ASSOC);
            return new ForumMessage(db_result($Result,0,'message_id'), $Row);
        }

        return false;
    }
}
?>