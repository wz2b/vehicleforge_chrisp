<?php
class Document extends Error {
	private $DocID, $Debug, $Data;

	function __construct($DocID=null,$Debug=false){
		if ($DocID){
			$this->DocID=$DocID;

			$Result=db_query_params("SELECT * FROM doc2_files WHERE file_id=$1",array($DocID));
			$this->Data=db_fetch_array($Result);
		}

		$this->Debug=$Debug;
	}

	function getName(){
		return $this->Data['filename'];
	}

	function getOwnerID(){
		return $this->Data['owner_id'];
	}

	function getOwnerName(){
		$User=user_get_object($this->Data['owner_id']);
		return $User->getRealName();
	}

	function getDateModified(){
		return $this->Data['modified_date'];
	}

	function getDescription(){
		return $this->Data['description'];
	}

    function getDocGroupID(){
        return $this->Data['doc_group_id'];
    }

    function getGroupID(){
        return $this->Data['group_id'];
    }

	function create($OwnerID,$FileInputName,$Description,$DocGroupID=null,$Size,$GroupID){
		$Group=group_get_object($GroupID);

        $FileData=new FileData;
        if ($FileData->load($FileInputName)){

            if ($Group->getGalleryGroupID()==$DocGroupID){
                //If adding to gallery folder
                $AllowedTypes=array('jpg','jpeg','gif','bmp','png');
            }else{
                //Allow all file types
                $AllowedTypes='*';
            }

            $DirectoryPath=GF_IMAGE_STORE.'project_files/'.$Group->getUnixName().'/';

            if ($DocGroupID){
                $DocGroup=new DocumentGroup($DocGroupID);

                $DirectoryPath.=$DocGroup->getName().'/';
            }

            $FileUploader=new FileUploader();
            if ($FileUploader->upload($FileData, $DirectoryPath.$FileData->get('name'), $AllowedTypes)){
                if (db_query_params("INSERT INTO doc2_files(owner_id,filename,description,modified_date,doc_group_id,size,group_id) VALUES($1,$2,$3,$4,$5,$6,$7)",array($OwnerID,$FileData->get('name'),$Description,time(),$DocGroupID,$_FILES[$FileInputName]['size'],$GroupID))){
                    return true;
                }else{
                    $this->setError("Unable to create Doc in database");
                }
            }else{
                $this->setError($FileUploader->getErrorMessage());
            }
        }

		return false;
	}

    public function getPublicPath(){
        $Path='/image_store/project_files/';
        $Path.=$this->getPath();
        return $Path;
    }

    private function getPrivatePath(){
        $Path=GF_IMAGE_STORE.'project_files/';
        $Path.=$this->getPath();
        return $Path;
    }

    private function getPath(){
        $Group=group_get_object($this->getGroupID());
        $Path=$Group->getUnixName().'/';

        if ($this->getDocGroupID()!=0){
            $Result=db_query_params("SELECT group_name FROM doc2_groups WHERE doc_group_id=$1",array($this->getDocGroupID()));
            $DocGroup=db_fetch_array($Result);
            $Path.=$DocGroup['group_name'].'/';
        }

        $Path.=$this->getName();

        return $Path;
    }

	/**
	 * delete() - Deletes a document from the file system and database
	 * @return bool
	 */
	function delete(){
		$Result=db_query_params("SELECT * FROM doc2_files WHERE file_id=$1",array($this->DocID));
		$Doc=db_fetch_array($Result,null,PGSQL_ASSOC);
		$Group=group_get_object($Doc['group_id']);

		if ($Group->getImage()!=$this->DocID){
			//TODO: Check if file is in use before deleting!
			if ($this->getOwnerID()==user_getid()){
	            if (unlink($this->getPrivatePath())){
	                if (db_query_params("DELETE FROM doc2_files WHERE file_id=$1",array($this->DocID))){
	                    return true;
	                }else{
	                    $this->setError("Unable to delete file from database");
	                }
	            }else{
	                $this->setError("Cannot delete from file system");
	            }
			}else{
				$this->setError("You dont have permission to delete this document");
			}
		}else{
			$this->setError("File is in use");
		}

		return false;
	}
}
?>
