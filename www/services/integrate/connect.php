 <p>In this pane, you can add services from your project to your integrated service and build connections between these services.  When you are done, move on to step 2, where you will build a runtime interface for your integrated service.</p>
  
<div id="service-list" class="row">
	<h4>1a. Drag and drop services onto the integration panel</h4>
	<?php
		foreach( $services as $s) {
			$interface = $s['interface'];
			$data = $interface->getData();
			$panes = $data->inParams;
			$outputs = $data->outParams; ?>
			
			<div class="service" diid="<?= $interface->getID() ?>" draggable="true"><?= $interface->getName() ?></div>
		<?php	} ?>
</div> 

<br/>
<h4>1b. Connect inputs and outputs </h4>
<ul id="integration-panel" class="well">
</ul>

<div id="service-form" class="service-pane hide">				
	<h1 class="resource-id"></h1>
	<i class="pull-right close">&times;</i>	
	<h2 class="service-name" style="margin-bottom: 10px; border-bottom: 1px solid #000; font-weight: 300;"></h2>
	<div class="row-fluid">
		<div class="span6">
			<h3>Inputs</h3>
			<ul class="inputs"></ul>
		</div>
		<div class="span6">
			<h3>Outputs</h3>
			<ul class="outputs"></ul>
		</div>
	</div>
</div>

<script>
	$(document).ready( function() {
		
		/* BEGIN dnd service listeners */
		$('.service').draggable({
			scope: 'services',
			revert: true,
			helper: 'clone',			
			start: function(e,ui) {
				$(this).addClass('moving');
			},				
			stop:	function(e,ui) {
		  	$(this).removeClass('moving');
			}
		});
		/* END dnd service listeners */
		
		/* BEGIN integration panel listeners */
		$('#integration-panel').droppable({
			scope: 'services',
			activeClass: 'dragtarget',
			hoverClass: 'dragover',
			tolerance: 'pointer',
			drop: function(e,ui) {
				IM.addResource(ui.helper.attr('diid'));
			}
		});
		
		$('#integration-panel').sortable({
			tolerance: 'pointer',
			scrollSensitivity: 60,
			scrollSpeed: 10,
			containment: 'parent'
		});		
		/* END integration panel listeners */
		
		/* BEGIN DOME event listeners */
		$('body').bind('DOME_AddResource', addResourceToConnect);		
		$('body').bind('DOME_RemoveResource', removeResourceFromConnect);
		$('body').bind('DOME_AddRelation', addRelationToConnect);
		$('body').bind('DOME_RemoveRelation', removeRelationFromConnect);
		/* END DOME event listeners */
						
		/* BEGIN Parameter Filtering listeners */
		$('#integration-panel').bind('DOME_ClickParam', filterConnectParameters);
		/* END parameter filtering listeners */
		
		/* BEGIN Connection Builder listeners */
		$('body').bind('DOME_ClickParam', updateConnectPane);
		
		$('#relation-pane .close').bind('click', function() {
			var $target = $(this).closest('li');
			var pID = $target.attr('parameter-id')
			if (pID)
				$('.parameter[parameter-id="' + pID + '"]').removeClass('active').trigger('DOME_ClickParam');
		});
		
		$('#save-relation').hide().click(function() {
			var toID = $('#input-parameter').attr('parameter-id');
			var toParam = $('.parameter[parameter-id="' + toID + '"]').data('parameter');
			var fromID = $('#output-parameter').attr('parameter-id');
			var fromParam = $('.parameter[parameter-id="' + fromID + '"]').data('parameter');			
			IM.addRelation(fromParam, toParam);
		});		
		/* END Relation Builder listeners */
	});	
	
	
	/* addResource - adds a new DOME service resource to the integration panel
	 * @param event
	 * @param resourceID
	 * @param interface ID
	 * @returns null
	 */
	function addResourceToConnect(event, resource) {
		// parse resource variables
		var resourceID = resource.getID();
		var name = resource.getName();
		
		// build service pane
		var $pane = $('#service-form').clone().removeAttr('id').data('resource', resource);
		
		// set resourceID parameters
		$pane.attr('resource-id', resourceID);
		$pane.children('.resource-id').html(resourceID);
		$pane.children('.service-name').html(name);
		
		// build inputs
		$.each(resource.getInputs(), function(index, parameter) {
			addParameterToConnect($pane.find('.inputs'), resource, parameter, 'input');
		});
		
		// build outputs
		$.each(resource.getOutputs(), function(index, parameter) {
			addParameterToConnect($pane.find('.outputs'), resource, parameter, 'output');
		});
		
		// set up removeResource connectors
		$pane.children('.close').on('click', function(e) {
			if ( confirm('Are you sure you want to remove this service from the integration model?') ) {			
				$pane.find('.parameter.active').removeClass('active').trigger('DOME_ClickParam');
				IM.removeResource(resourceID);
			}
		});
		
		// set up listeners for parameter clicks
		$pane.find('.parameter').bind('click', function(event) {		
			if ($(this).attr('disabled') != 'disabled')
				$(this).toggleClass('active').trigger('DOME_ClickParam');
		});
		
		// append to integration panel
		$pane.removeClass('hide').appendTo('#integration-panel');
		
		// filter parameters
		filterConnectParameters();
	}
	
	function addParameterToConnect($list, resource, parameter, type) {
			var $param = $('<li class="parameter">' + parameter.getName() + '<span class="unit pull-right">' + parameter.getUnit() + '</span><ul class="relation-list"></ul></li>');
			$param.addClass(type).attr('parameter-id', parameter.getID()).attr('resource-id', resource.getID()).data('parameter', parameter);
			$list.append($param);
	}
	
	/* removeResource - removes a DOME service resource from the integration panel
	 * @param event
	 * @param resourceID
	 * @returns null
	 */
	function removeResourceFromConnect(event, resource) {
		$('.service-pane[resource-id="' + resource.getID() + '"]').fadeOut(500).remove();
	}
	
	/* addRelation - creates a graphical representation of a relation
	 * @param event
	 * @param relationID
	 * @param relationData
	 * @returns null
	 */
	function addRelationToConnect(event, relation) {
		var $from = $('#integration-panel .parameter[parameter-id="' + relation.from.getID() + '"]');
		var $to = $('#integration-panel .parameter[parameter-id="' + relation.to.getID() + '"]');
		
		buildRelationLabel($from, relation.getID(), relation.to);
		buildRelationLabel($to, relation.getID(), relation.from);
	}
	
	// helper method to generate labels
	function buildRelationLabel($param, relationID, linkedParameter) {
		// set text
		var text = '<li class="relation" relation-id="' + relationID + '"> ' + ' >> ' + linkedParameter.getDisplayName() + '<a class="pull-right close">&times;</a>' + '<li>';
		$param.children('ul.relation-list').append(text);
		$param.find('.close').click( function(event) {
			event.stopPropagation();
			if ( confirm('Are you sure you want to remove this relation from the integration model?') )
				IM.removeRelation(relationID);
		});
		
		// fire event
		$param.toggleClass('active').trigger('DOME_ClickParam');
	}

	
	/* removeRelation - removes a relationship from the UI
	 * @param event
	 * @param relationID
	 * @returns null
	 */
	function removeRelationFromConnect(event, relation) {
		$('ul.relation-list .relation[relation-id="' + relation.getID() + '"]').fadeOut(500).remove();
	}
	
	/* filterConnectParameters - limits selection of parameters based on currently selected parameters based on dimensionality
	 * @returns null
	 */
	function filterConnectParameters() {
		var $activeParams = $('#integration-panel .parameter.active');			
		var $targetsToFilter = $();
		
		$activeParams.each( function(index) {
			var element = $(this);
			var parameter = element.data('parameter');
					
			// filter elements in this resource
			$targetsToFilter = $targetsToFilter.add(element.closest('.service-pane').find('.parameter'));
			
			// filter elements with different dimensional constraints
/*
			var filter = 'dome-type!="' + element.attr('dome-type') + '"';	
			$targetsToFilter = $targetsToFilter.add('#integration-panel .parameter[' + filter + ']');			
*/

			if (element.hasClass('input')) {				
				// make sure that input is only mappable to one output				
				if (element.find('.relation').length > 0)
						$targetsToFilter = $targetsToFilter.add( $('#integration-panel .parameter') );			
				// filter all inputs
				$targetsToFilter = $targetsToFilter.add( '#integration-panel .parameter.input' );				
				
			} else {			
				// filter all inputs with existing relations
				$targetsToFilter = $targetsToFilter.add($('#integration-panel .parameter.input .relation').closest('.parameter'));
				// filter all outputs
				$targetsToFilter = $targetsToFilter.add( '#integration-panel .parameter.output' );
			}
		});
		
		// remove active parameters from filtering list
		$targetsToFilter = $targetsToFilter.not($activeParams);
		
		// apply filter	
		$('#integration-panel .parameter').not($targetsToFilter).removeAttr('disabled').fadeIn(300);
		$targetsToFilter.attr('disabled', 'disabled').slideUp(300);
	}
	
	function updateConnectPane(event) {
		var $param = $(event.target);
		var parameter = $param.data('parameter');
		
		var $target = $param.hasClass('input') ? $('#input-parameter') : $('#output-parameter');
		
		if ($param.hasClass('active')) {
			$target.children('input').attr('value', parameter.getDisplayName());
			$target.attr('parameter-id', parameter.getID());			
			
		} else {
			$target.children('input').attr('value', '');
			$target.removeAttr('parameter-id');
		}
		
		// update save relation button state
		if ($('#input-parameter').attr('parameter-id') && $('#output-parameter').attr('parameter-id')) {					
			$('#save-relation').fadeIn(500);		
		}	else {
			$('#save-relation').fadeOut(500);
		}
	}		
</script>

<style>
	[draggable] {
	  -moz-user-select: none;
	  -khtml-user-select: none;
	  -webkit-user-select: none;
	  user-select: none;
	}
	
	#service-list {
		padding-left: 20px;
	}
	
	.service {
	  float: left;
	  padding: 5px 10px;
	  margin: 5px;
	  
	  border: 1px solid #CCC;
	  background-color: #F5F5F5;
	  
	  -webkit-border-radius: 10px;
	  -ms-border-radius: 10px;
	  -moz-border-radius: 10px;
	  border-radius: 10px;
	  
	  text-align: center;
	  cursor: move;
	}
	
	.service.moving {
		opacity: 0.4;
		-webkit-transform: scale(0.8);
		-moz-transform: scale(0.8);
		-ms-transform: scale(0.8);
		-o-transform: scale(0.8);
	}

	.service-pane {
		margin: 10px;
		margin-left: 20px;
		padding: 10px;
		padding-left: 50px;		
		border: 1px solid #000;
		border-radius: 5px;	
		background-color: #FFF;
	}
	
	.service-pane .resource-id {
		float: left;
		margin-left: -35px; 
	}
	
	#relation-pane {
		position: fixed;
		width: 180px;
	}
	
	#input-parameter > input,
	#output-parameter > input {
		width: 80%;
		color: #06C;
		background-color: #FFF;
	}
	
	ul.inputs, 
	ul.outputs {
		list-style: none;
		margin: 0;
	}
	
	.parameter {
		margin: 5px;
		padding: 5px;
		font-size: 14px;		
		border-radius: 5px;
		border: 1px solid #FFF;
		cursor: pointer;
	}
	
	.parameter.active {
		background-color: #08C;
		color: #FFF;
	}	
	
	.parameter:hover {
		border: 1px solid #000;
	}
	
	.parameter[disabled=disabled] {
		color: #999;
		cursor: default;
	}
	
	.parameter[disabled=disabled]:hover {
		border: 1px solid #FFF;
	}
	
	.parameter .relation-list {
		list-style: none;
		margin: 0 10px;
	}
	
	.relation {
		border-radius: 5px;
		padding: 2px 5px;
		background-color: #08C;
		color: #FFF;
		font-size: 0.8em;
		margin-top:5px;
	}
	
	.relation .close {
		font-size: 15px;
		line-height:15px;
		color: #FFF;
		opacity: 1;
		font-weight:100;
	}
	
	#integration-panel {
		min-height: 400px;
		padding: 10px;
		margin: 0;
		
	  border-radius: 10px;		
	  -webkit-border-radius: 10px;
	  -ms-border-radius: 10px;
	  -moz-border-radius: 10px;
	}
	
	#integration-panel.dragtarget {
		border: 1px dashed #08C;
		background: #F5F5F5;	
	}
	
	#integration-panel.dragover {
		border: 1px solid #08C;
		background: #F5F5F5;
	}
</style>

