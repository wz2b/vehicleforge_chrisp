<h4>Select Inputs to Integration Service</h4>

<table class="table table-striped" id="build-inputs">
	<thead>
		<tr>
			<th>&#x2713;</th>
			<th>ID</th>
			<th>Service</th>
			<th>Parameter</th>
			<th>Name</th>
			<th>Default Value</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<h4>Select Outputs to Integration Service</h4>
<table class="table" id="build-outputs">
	<thead>
		<tr>
			<th>&#x2713;</th>
			<th>ID</th>
			<th>Service</th>
			<th>Parameter</th>
			<th>Name</th>
			<th>Default Value</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<script>
	$(document).ready( function() {		
		$('body').bind('DOME_AddResource', addResourceToBuild);
		$('body').bind('DOME_RemoveResource', removeResourceFromBuild);

		$('body').bind('DOME_AddRelation', function(event, relation) {
			removeParameterFromBuild(relation.to);
		});	
		$('body').bind('DOME_RemoveRelation', function(event, relation) {
			addParameterToBuild(relation.to, 'input');
		});
		
	});
	
	function addResourceToBuild(event, resource) {
		// inputs
		var inputs = resource.getInputs();
		for (var i in inputs) {
			addParameterToBuild(inputs[i], 'input');
		}
		
		// outputs
		var outputs = resource.getOutputs();
		for (var j in outputs) {
			addParameterToBuild(outputs[j], 'output');
		}
	}
	
	function removeResourceFromBuild(event, resource) {
		$('#build .build-parameter[resource-id="' + resource.getID() + '"]').remove();		
	}
	
	function addParameterToBuild(parameter, type) {		
		var $param = $('<tr class="build-parameter"></tr>');
		$param.attr('resource-id', parameter.getResourceID()).attr('parameter-id', parameter.getID()).data('parameter');
		
		// build checkbox
		var $check = $('<td><input type="checkbox"></td>').click(checkBuildParameter);
		$param.append($check);

		var resource = Resource.find(parameter.getResourceID());		
		$param.append('<td class="resource-id">' + resource.getID() + '</td>');
		$param.append('<td class="resource-name">' + resource.getName() + '</td>');
		
		// build resource parameter
		$param.append('<td class="resource-parameter">' + parameter.getName() + '</td>');
		
		// build integration parameter text box
		$param.append('<td class="integration-parameter"><input disabled type="text" value="' + parameter.getName() + '"></td>');
		
		// build default value input
		var $value = $('<td class="default-value"></td>');		
		switch (parameter.getType().toLowerCase()) {
			case "file":
				$value.append('<input style="width:80px" type="file">');
				break;
			default:				
				$value.append('<input style="width:80px" type="text">');
				break;
		}		
		$value.children('input').attr('value', parameter.getDefaultValue());
		$param.append($value);
		
		// build unit
		$param.append('<td class="unit">' + parameter.getUnit() + '</td>');
		
		// append element to appropriate table
		if (type == "input")
			$param.appendTo('#build-inputs tbody');
		else
			$param.appendTo('#build-outputs tbody');
	}
	
	function removeParameterFromBuild(parameter) {
		$('.build-parameter[parameter-id="' + parameter.getID() + '"]').remove()
	}
	
	function checkBuildParameter(event) {
		var cb = $(event.target);
		if (cb.is(':checked')) {
			cb.closest('.build-parameter').find('.integration-parameter input').removeAttr('disabled');
		} else {
			cb.closest('.build-parameter').find('.integration-parameter input').attr('disabled', 'disabled');			
		}		
	}
	
</script>

<style>
	.table td {
		vertical-align: middle;		
	}
	
	.table input {
		margin: 0;		
	}
</style>