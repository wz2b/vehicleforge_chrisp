<?php
/*
 *  Show a specific DOME Interface/Service
 *	Jeff Mekler (2/29/2012 Leap Day!)
 *
 *  local variables:
 *      $DOMEInterface - DOMEInterface object
 *      $group - Group object
 */

function TabHeader($DOMEInterface, $subtitle, $Side='') {
	return '<h2 style="border-bottom: 1px solid #AAA;" class="service_name">'.$DOMEInterface->getName().'<span style="font-weight:300;"> : '.$subtitle.'</span>'.$Side.'</h2>';
}

$RunID=getIntFromRequest('runid',false);

$Layout->col(3,true,true);
?>

<div class="well">
	<ul id="service_nav" class="nav nav-list nav-stacked">
	  <li<?=$RunID?'':' class="active"'?>><a href="#info" data-toggle="pill"><i class="icon-home"></i> Description</a></li>
	  <li><a href="#run" data-toggle="pill"><i class="icon-refresh"></i> Run Service</a></li>
	  <li><a href="#history" data-toggle="pill"><i class="icon-time"></i> Run History</a></li>
	  <li><a href="#settings" data-toggle="pill"><i class="icon-cog"></i> Settings</a></li>
	  <li><a href="/services/?group_id=<?=$group->getID()?>"><i class="icon-th-list"></i> All Services</a></li>
	</ul>
</div>

<?php
$Layout->endcol();
$Layout->col(9);
$ModelData = $DOMEInterface->getData();
?>
<div class="tab-content">
  <div class="tab-pane<?=$RunID?'':' active'?>" id="info">
		<?= TabHeader($DOMEInterface, 'description') ?>
  	    <?php include 'show/index.php'; ?>
  </div>
  
  <div class="tab-pane" id="run">
		<?= TabHeader($DOMEInterface, 'run service') ?>
  	    <?php include 'show/run.php'; ?>
  </div>
  
  <div class="tab-pane" id="history">
		<?= TabHeader($DOMEInterface, 'history of runs') ?>
  	    <?php include 'show/history.php'; ?>
  </div>
  
  <div class="tab-pane" id="settings">
		<?= TabHeader($DOMEInterface, 'settings', '<button id="service_delete" class="btn btn-danger pull-right">Delete Service</button>') ?>
  	    <?php include 'show/settings.php'; ?>
  </div>
    <div class="tab-pane<?=$RunID?' active':''?>" id="view_run">
        <?= TabHeader($DOMEInterface, 'run results (ID: '.$RunID.')') ?>
        <?php include 'show/view.php';?>
    </div>
</div>

<?php


$Layout->endcol();
?>
 
<script>
$(document).ready( function() {
	//$('#service_nav li:first a').tab('show');
});

$('a[data-toggle="pill"]').on('shown', function (e) {
  $(e.target).find('i').addClass('icon-white');
  $(e.relatedTarget).find('i').removeClass('icon-white');
})

</script>