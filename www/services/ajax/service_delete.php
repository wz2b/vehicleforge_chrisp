<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$DIID=getIntFromRequest('diid');
$Return=array('error'=>false);

$DOMEInterface=new DOMEInterface($DIID);
$Return['error']=!$DOMEInterface->delete();

echo json_encode($Return);
?>