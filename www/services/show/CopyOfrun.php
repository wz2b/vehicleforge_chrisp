<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
/*
 * run.php
* Partial to display the run UI for a dome interface*
* local variables:
*		$DOMEInterface - DOMEInterface object
*		$ModelData - DOME Model
*/
function strToID($String){
	return str_replace(" ","_",$String);
}


function formatArray($Arr){
	// echo "<script type="text/javascript">window.alert("Sorry, we only allow uploading GIF images")</script>";
if (is_array($Arr)){
		//return $Arr;
		return json_encode($Arr);
	}else{
		return $Arr;
	}
}

function formatXPut($Value, $XPut, $Disabled=false, $Type="input"){
	switch($XPut->type){
		case 'Array':
		case 'Matrix':
		case 'Vector':
			echo '<input type="text" class="'.$Type.'" id="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.formatArray($Value).'" autocomplete="off"';
			break;

		case 'Enumeration':
			$val = (string)$Value->obj;
			echo '<input type="text" class="'.$Type.'" id="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$val.'" autocomplete="off"';
			break;
			 
		case 'File':
			if ($Disabled) {
				echo '<input type="text" class="'.$Type.'" id="'.$XPut->name.'" name="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off"';
			}else {
				echo '<input type="file" class="'.$Type.'" id="'.$XPut->name.'" name="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off" enabled="true"';
			}
			break;
			 
		default:
			echo '<input type="text" class="'.$Type.'" id="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off"';
	}
	
	if ($Disabled)
		echo ' disabled';

	echo ' />';
	if ($Type == "output"){
	echo '  <input type ="radio" name="radioX" value="'.$XPut->name.'">   ';
	echo '  <input type ="checkbox" name="checkboxY" value="'.$XPut->name.'">  ';
	}
}


//print_r($ModelData);
?>
<script type="text/javascript" src="/js/Visualization.js"></script>
<script type="text/javascript" src="/js/bootstrap/modal.js"></script>
<script type="text/javascript" src="/js/colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="/js/colorbox/jquery.simplemodal-1.4.3.js"></script>
<script type="text/javascript" src="/js/jquery.json-2.4.min.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<link rel="stylesheet" href="/themes/css/visualization.css" type="text/css" />
<body>
	<div>
		Status: <span id="status">Not Running</span>
	</div>
	<form id="inputsForm" action="ajax/model_run.php" class="form-horizontal" >
	<div id="inputs" class="form-horizontal">
		<legend>Inputs</legend>
		<?php foreach ($ModelData->inParams as $i) { ?>
		<div class="control-group">
			<label class="control-label"><?=$i->name?> </label>
			<div class="controls">
				<?php // determine input value
				$value = '';
				if (isset($_GET['complete'])){
					foreach($Messages as $m){
						if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
							$value = $m['new_val'][0];
						}
					}
				} else {
					$value = $i->value;
				}
				
				formatXPut($value, $i, false, "input");
				?>

				<span class="help-inline"><?= (($i->unit=='no unit')? $i->type: $i->unit).' ('.$i->type.')' ?>
				</span>
			</div>
		</div>
		<?php } ?>
		<input type="hidden" name="data" value='{"PutStuff": "Here"}'>
		<input type="hidden" name="server_id" value=<?=$DOMEInterface->getServerID()?>>
		<input type="hidden" name="interface_id" value=<?=$InterfaceID?>>
	</div>

	<div id="outputs" class="form-horizontal">
		<legend>Outputs</legend>
		<?php foreach ($ModelData->outParams as $i) { ?>
		<div class="control-group">
			<label class="control-label"><?=$i->name?> </label>
			<div class="controls">
				<?php // determine input value
				$value = '';
				if (isset($_GET['complete'])){
					foreach($Messages as $m){
						if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
							$value = $m['new_val'][0];
						}
					}
				} else {
					if (isset($i->value)) {
						$value = $i->value;
					}
				}
				
				formatXPut($value, $i, true, "output");
				?>

				<span class="help-inline"><?= (($i->unit=='no unit')? $i->type: $i->unit).' ('.$i->type.')' ?>
				</span>
			</div>
		</div>
		<?php } ?>
	</div>
	<div id="actions" class="form-actions">
		<input type="submit" value="Run" class="btn btn-primary"/>
		<input type="reset" value="Clear Form" class="btn"/>
		<input type="button" value="Visualize" id="visualize" class="btn"/>
		<div style="display: inline; margin-left: 15px;">
			Your Run Time: <span id="my_runtime">0s</span> (average
			<?=Runtimes::getAvg($DOMEInterface->getID())?>
			s)
		</div>
	</div>
	</form>
	
	
	<div id="outputMsg"></div>
<script src="/js/d3.v3.js"></script>
<div id="dvtVisual" class="modal hide fade in form-horizontal">
	<div id="visual" class="modal-body" style="modal-backdrop">
				<!-- <div style="margin:10px;">
			 <iframe src="http://nsk1visbuilder3.research.ge.com:8887/dvaaspoc2.html?fileName=wind_data_1.csv&xSelect=C2&ySelect=C3"></iframe>
			    
			</div> -->
    </div>
   <div class="modal-footer">      
      <a class="btn" id="modal_close">Close</a>
        <button id="scatterPlot" class="btn">Scatter Plot</button>
		<button id="lineChart" class="btn">Line Chart</button>
		<button id="testJSON" class="btn">Test JSON</button>
    </div>
</div>
	
	
	<script>
var runTime=0,
		startTime=0,
		myRunTime=0,
		myTimer=null;


		
function updateTimer(){
	myRunTime+=100;
	$("#my_runtime").text((myRunTime/1000)+"s");
}

function startTimer(){
	myRunTime=0;
	myTimer=setInterval(updateTimer, 100);
}

$(document).ready(function(){
	  $("#dvtVisual").hide();
	  //$('#outputs > .control-group').find('#radioX').hide();
	  //$('#outputs > .control-group').find('#checkboxY').hide();
		$(":radio[name=radioX]").hide();
		$(":checkbox[name=checkboxY]").hide();
		var options = { 
	        target:        '#outputMsg',   // target element(s) to be updated with server response 
	        beforeSubmit:  preRequest,  // pre-submit callback 
	        success:       processRunModel,  // post-submit callback 
	 
	        // other available options: 
	        //url:       url         // override for form's 'action' attribute 
	        type:      'post',        // 'get' or 'post', override for form's 'method' attribute 
	        dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
	        //clearForm: true        // clear all form fields after successful submit 
	        //resetForm: true        // reset the form after successful submit 
	 
	        // $.ajax options can be used here too, for example: 
	        timeout:   2000 
	    }; 
	 
	    // bind form using 'ajaxForm' 
	    $('#inputsForm').ajaxForm(options);
		// bind to the form's submit event 
	    //$('#inputsForm').submit(function() { 
	    //    // inside event callbacks 'this' is the DOM element so we first 
	    //    // wrap it in a jQuery object and then invoke ajaxSubmit 
	    //    $(this).ajaxSubmit(options); 
	 
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	    //    return false; 
	    //}); 
});


$('#visualize').click(function() {
	$('#status').html("Running Visualizer...");
	$('#dvtVisual').slideDown("slow");
	$(":radio[name=radioX]").slideDown("slow");
	$(":checkbox[name=checkboxY]").slideDown("slow");
	var Data={};
	var $Outputs=$('.output');
	$Outputs.each(function(i, tag){
		$(this).prop('disabled', false);
		// $('#outputs > .control-group').find('#checkboxY').prop('checked',true);
		Data[$(this).data('name')]=$(this).val();
	});
	
});

$('#modal_close').click(function() {
	$('#dvtVisual').slideUp("slow");
	$(":radio[name=radioX]").slideUp("slow");
	$(":checkbox[name=checkboxY]").slideUp("slow");
});

$('#testJSON').click(function() {
	 var jsonData = createJSONData();
	 alert(jsonData);
	$('#status').html("Finished!");	
});

$('#lineChart').click(function() {
	 d3.select("svg").remove();
	 runLineVisualization();
	$('#status').html("Finished!");	
});


$('#scatterPlot').click(function() {
	d3.select("svg").remove();
	runScatterVisualization();
	$('#status').html("Finished!");	
});
// clear input/output fields
$('#clear').click(function() {
	$('#inputs > .control-group').find('.input').val('');
	$('#outputs > .control-group').find('.output').val('');
});

//pre-submit callback
function preRequest($formData, jqForm, options) {
    // formData is an array; here we use $.param to convert it to a string to display it
    // but the form plugin does this for you automatically when it submits the data
    //var queryString = $.param(formData);
    var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(i, tag){
		$(this).prop('disabled',true);
		try {
			Data[$(this).data('name')]=eval($(this).val());
		}
		catch (err) {
			Data[$(this).data('name')]=$(this).val();
		}
	});

	var queryString = $.toJSON(Data);
	//formData.push({ name: 'data', value: queryString });
	//formData.push({ name: 'server_id', value: <?=$DOMEInterface->getServerID()?> });
	//formData.push({ name: 'interface_id', value: <?=$InterfaceID?> });
	$.each($formData, function(index, value) {
		if (value.name == "data") { 
			value.value = queryString; 
		}
	});
    //$formData[0].value = queryString;
    //formData[1].value = <?=$DOMEInterface->getServerID()?>;
    //formData[2].value = <?=$InterfaceID?>;

    // jqForm is a jQuery object encapsulating the form element.  To access the
    // DOM element for the form do this:
    // var formElement = jqForm[0];

    // here we could return false to prevent the form from being submitted;
    // returning anything other than false will allow the form submit to continue
    return true;
}

// post-submit callback
function showResponse(responseText, statusText, xhr, $form)  {
    // for normal html responses, the first argument to the success callback
    // is the XMLHttpRequest object's responseText property

    // if the ajaxSubmit method was passed an Options Object with the dataType
    // property set to 'xml' then the first argument to the success callback
    // is the XMLHttpRequest object's responseXML property

    // if the ajaxSubmit method was passed an Options Object with the dataType
    // property set to 'json' then the first argument to the success callback
    // is the json data object returned by the server

    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText +
        '\n\nThe output div should have already been updated with the responseText.');
}

/*
	$('#submit').click(function(){
    $("#status").html("Initializing...");

	//var formData = new FormData();     
    var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(i, tag){
		$(this).prop('disabled',true);
		//if ($(this).attr("type") == 'file') {
		//	$.each($(tag)[0].files, function(i, file) {
		//		//alert("Found file: " + tag.value);
	        //    formData.append(tag.value, file);
	        //});
		//}
		try {
			Data[$(this).data('name')]=eval($(this).val());
		}
		catch (err) {
			Data[$(this).data('name')]=$(this).val();
		}
	});

	Data=$.toJSON(Data);
	//formData.append('data', Data);
	//formData.append('server_id', <?=$DOMEInterface->getServerID()?>);
	//formData.append('interface_id', <?=$InterfaceID?>);

	var $me=$(this);

	// clear output fields
	$('#outputs').children('.control-group').val('').addClass('error');
	$('#inputs').children('.control-group').addClass('error');
	
	$me.prop('disabled',true);

	formData = {'data':Data, 'server_id':<?=$DOMEInterface->getServerID()?>, 'interface_id':<?=$InterfaceID?>};


	$.post('ajax/model_run.php', formData, processRunModel);
	//$.ajax({
	//    url: 'ajax/model_run.php',
	//    data: formData,
	//    cache: false,
	//    contentType: false,
	//    processData: false,
	//    type: 'POST',
	//    success: processRunModel
 	//});
	*/

	var pollTimer=null,
		queueURL="",
		totalMessages=0,
		received=0,
		runningID=0;
	var $me=$('#actions');
	$Inputs=$('.input');
	
	//function processRunModel(msg){    responseText, statusText, xhr, $form
	function processRunModel(responseObj, statusText, xhr, $form){    
		msg=responseObj;
		if (msg.error==false){
			$me.prop('disabled',true);
			startTime=new Date().getTime();
			startTimer();
			$("#status").html("Running...");
			
			queueURL=msg.queue_url;
			runningID=msg.running_id;
			poll();
		}else{
			alert(msg.msg);
		}
	}
	
	function poll(){
		if (pollTimer==null) {
			pollTimer=setInterval(function(){poll();},3000);
		}
		
		$.ajax({
			type: 'POST',
			url: 'ajax/poll_queue.php',
			dataType: 'json',
			data: {
				queue_url: queueURL,
				running_id:runningID,
				timeStamp: new Date().getTime()
			},
			error: function(msg) {
				alert("Error in polling queue: " + msg.msg);
				clearInterval(pollTimer);
				clearTimeout(myTimer);
			},
			success: function(msg){
				if (msg.error==true){
					alert(msg.msg);
					clearInterval(pollTimer);
					clearTimeout(myTimer);
				}
				else {
	                if (msg.messages && (msg.messages[0] != "null")){
	                    //Loop through all messages
	                    var i, l, key, m, field;
	                    for(i=0, l=msg.messages.length; i<l; i++){
	
	                        key=msg.messages[i];
	                        debugMsg("Have keys[" + msg.messages.length + "]: " + key);
	                        received++;
	
	                        m=$.parseJSON(key);
	                        //m=msg;
	
	                        //Check if message received is the message that specifies how many messages there should be
	                        if (m.event=="class mit.cadlab.dome3.api.ParameterStatusChangeEvent" && 
	    	                        ((m.new_val=="SUCCESS") || (m.new_val=="FAILURE"))){
	                            totalMessages=m.param;
	                        }
	
	                        //If a value changed message is received, change it on the page
	                        if (m.event=="class mit.cadlab.dome3.api.ParameterValueChangeEvent"){
	                                field = $('input[data-name="'+m.param+'"]');
	                                oStr = Object.prototype.toString.call(m.new_val[0]);
	                                switch (Object.prototype.toString.call(m.new_val[0])) {
	                                	case "[object Object]":
		                                	/*
		                                		This Code was added to flatten any array structures to the
		                                		output.  It helps with plotting data and it also makes it easier
		                                		to see without all the brackets.
		                      
		                                	*/
	                                		//var unflattenedData = m.new_val[0].data;
	                                		//var flattenedData = unflattenedData.reduce(function(a, b) {
	                                		//    return a.concat(b);
	                                		//});
	                                		//field.val($.toJSON(flattenedData));
	                                		field.val($.toJSON(m.new_val[0].data));
		                                	break;
		                                default:	
	                                		field.val($.toJSON(m.new_val[0]));
                                		break;
	                                }
	                                field.closest('.control-group').removeClass('error').addClass('success');
	                        }
	
	                        //If messages received equals needed messages, stop polling
	                        debugMsg("totalMessages: " + totalMessages + "    received: " + received);
	                        if (totalMessages==received){
	                            //Get run time length
	                            runTime=new Date().getTime() - startTime;
	
	                            //Stop timers
	                            clearTimeout(myTimer);
	                            clearInterval(pollTimer);
	
	                            //Delete the queue
	                            $.ajax({
	                                type: 'POST',
	                                url: 'ajax/delete_queue.php',
	                                data: {
	                                    queue_url: queueURL
	                                },
	                                dataType: 'json',
	                                success: function(msg){
	                                    if (msg.error==true){
	                                        alert(msg.msg);
	                                    }
	                                }
	                            });
	
	                            //Send run results to a script to log them
	                            $.post('ajax/run_complete.php',{runningID:runningID, runtime:runTime, interface_id:<?=$InterfaceID?>});
	                            $("#status").html("Finished");
	
	                            //Reset form
	                            $('#outputs').children('.control-group').removeClass('success');
	                            $('#inputs').children('.control-group').removeClass('success');
	                            $me.prop('disabled',false);
	                            $Inputs.each(function(){
	                                $(this).prop('disabled',false);
	                            });
	                            if (m.new_val=="FAILURE") {
		                            alert("Error: " + m.id.idString);
	                            }
	                        }
	                    } // for each message received
	                    if (totalMessages!=received){
	                    	debugMsg("Messages received but need more, calling poll() again");
                            //poll();
                        }
	                } // if messages
                    else {
                    	debugMsg("No messages received, calling poll() again");
                        //poll();
                    }
				}
			}
		});
	}
//});

function debugMsg(msg) {
	if (typeof console != "undefined") {
		console.log(msg);
	}
}
</script>
