<?php
$Table=new BsTable(array('class'=>'table table-striped'));
$Table->head(array('Run Date', 'Run By', 'Runtime', 'Results'));

$CompletedRuns=$DOMEInterface->getCompletedRuns();

foreach($CompletedRuns as $Run){
    $User=user_get_object($Run->getOwnerID());
    $Runtime=new Runtimes($Run->getID());

    $Table->col(date('n/j/Y',$Runtime->getCompletedDate()))
        ->col('<a href="'.$User->getURL().'">'.$User->getRealName().'</a>')
        ->col(round($Runtime->getRuntime()/1000, 1).'s')
        ->col('<a class="view-results" href="javascript:void(0)" data-runid="'.$Run->getID().'"><i class="icon-download"></i></a>');
}

echo $Table->render();
?>
<script>
$(function(){
    $(".view-results").click(function(){
        var run_id=$(this).data('runid');

        $.ajax({
            type:'post',
            url:'ajax/results_download.php',
            dataType:'json',
            data: {run_id:run_id},
            success:function(msg){
                if (!msg.error){
                    document.location=msg.file;
                }
            }
        });
    });
});
</script>
