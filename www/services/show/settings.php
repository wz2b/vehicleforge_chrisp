<div id="settings_notification" class="alert hidden"></div>
<script>
$(function(){
    var $notification=$("#settings_notification");

    $("#settings_save").click(function(){
        $notification.slideUp();
        var name=$("#alias").val(),
            desc=$("#desc").val();

        $.ajax({
            dataType: 'json',
            url: 'ajax/settings_save.php',
            data: {
                diid    : <?=getIntFromRequest('diid')?>,
                alias   : name,
                desc    : desc
            }
        }).done(function(msg){
            $notification.removeClass('alert-info alert-error alert-success');
            if (msg.error){
                $notification.addClass('alert-error')
                    .html('<strong>Error</strong>')
                    .slideDown();
            }else{
                $notification.addClass('alert-success')
                    .html('<strong>Success!</strong> The settings have been saved')
                    .slideDown()
                    .delay(5000)
                    .slideUp();

                $(".service_name").text(name);
            }
        });
    });

    $("#service_delete").click(function(){
        if (confirm("Are you sure you want to do this? It cannot be undone!")){
            $.ajax({
                dataType: 'json',
                url: 'ajax/service_delete.php',
                data: {
                    diid: <?=getIntFromRequest('diid')?>
                },
                success: function(msg){
                    if (!msg.error){
                        window.location='/services/?group_id=<?=$group->getID()?>'
                    }
                }
            });
            $.post('ajax/service_delete.php',{diid:<?=$DOMEInterface->getID()?>});
        }
    });
});
</script>
<?php
$Form=new BsForm;
echo $Form->init('javascript:void(0)','post',array('class'=>'form-horizontal'))
    ->group('Service Alias',
        new Text(array(
            'value'=>$DOMEInterface->getAlias(),
            'id'=>'alias',
            'autocomplete'=>false
        )),
        new Help('An alternate name for this service')
    )
    ->group('Description',
        new Textarea($DOMEInterface->getDescription(), array(
            'id'            => 'desc',
            'autocomplete'  => false
        ))
    )
    ->actions(
        new Submit('Save',array(
            'id'=>'settings_save',
            'class'=>'btn btn-primary'
        )),
        new Reset()
    )
    ->render();
?>