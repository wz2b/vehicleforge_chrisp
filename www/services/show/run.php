<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
/*
 * run.php
* Partial to display the run UI for a dome interface*
* local variables:
*		$DOMEInterface - DOMEInterface object
*		$ModelData - DOME Model
*/

require_once $gfcommon.'include/textConverter.php';

function strToID($String){
	return str_replace(" ","_",$String);
}

function formatArray($Arr){
	// echo "<script type="text/javascript">window.alert("Sorry, we only allow uploading GIF images")</script>";
if (is_array($Arr)){
		//return $Arr;
		return json_encode($Arr);
	}else{
		return $Arr;
	}
}


function formatXPut($Value, $XPut, $Disabled=false, $Type="input"){
	switch($XPut->type){
		case 'Array':
		case 'Matrix':
		case 'Vector':
			echo '<input type="text" class="'.$Type.'" id="'.$Type."_".$XPut->parameterid.'" data-name="'.$XPut->name.'" value="'.formatArray($Value).'" autocomplete="off"';
			break;

		case 'Enumeration':
			$val = (string)$Value->obj;
			echo '<input type="text" class="'.$Type.'" id="'.$Type."_".$XPut->parameterid.'" data-name="'.$XPut->name.'" value="'.$val.'" autocomplete="off"';
			break;
			 
		case 'File':
			if ($Disabled) {
				echo '<input type="text" class="'.$Type.'" id="'.$Type."_".$XPut->parameterid.'" name="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off"';
			}else {
				echo '<input type="file" class="'.$Type.'" id="'.$Type."_".$XPut->parameterid.'" name="'.$XPut->name.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off" enabled="true"';
			}
			break;
			 
		default:
			echo '<input type="text" class="'.$Type.'" id="'.$Type."_".$XPut->parameterid.'" data-name="'.$XPut->name.'" value="'.$Value.'" autocomplete="off"';
	}
	
	if ($Disabled)
		echo ' disabled';

	echo ' />';
	if (($XPut->type == 'File') && ($Disabled)) {
		echo ' <a  id="'.$XPut->name.'_open" href="javascript:void(0);" target="_blank">Open</a>';
	}
}


//print_r($ModelData);
?>


<script  src="/js/d3.v3.js"></script>


<!--
needed for IE 8
<script  src="/js/r2d3.v2.js"></script>

needed for IE 9
<script  src="/js/d3.v3.js"></script>
-->

<script  src="/js/Visualization.js"></script>
<script  src="/js/bootstrap.js"></script>
<script  src="/js/jquery.json-2.4.min.js"></script>
<script  src="/js/json2.js"></script>
<script  src="/js/chosen.jquery.js"></script>
<script  src="/js/jquery.form.js"></script>
<script  src="/js/bootstrap-select.min.js"></script>
<script  src="/js/zip.js/WebContent/zip.js"></script>

<link rel="stylesheet" href="/themes/css/visualization.css" type="text/css" />
<link rel="stylesheet" href="/themes/css/bootstrap/bootstrap-select.min.css" type="text/css" />


<body>
	<div>
		Status: <span id="status">Not Running</span>
	</div>
	<form id="inputsForm" action="ajax/model_run.php" class="form-horizontal" >
	<div id="inputs" class="form-horizontal">
		<legend>Inputs</legend>
		<?php foreach ($ModelData->inParams as $i) { ?>
		<!--HG: set id to the name of the input parameter-->
		<div class="control-group" id=<?=$i->parameterid?>>
			<label class="control-label" id=<?='label_'.$i->parameterid?>><?=$i->name?></label>
			<div class="controls">
				<?php // determine input value
				$value = '';
				if (isset($_GET['complete'])){
					foreach($Messages as $m){
						if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
							$value = $m['new_val'][0];
						}
					}
				} else {
					$value = $i->value;
				}
				
				formatXPut($value, $i, false, "input");
				?>

				<span class="help-inline"><?= (($i->unit=='no unit')? $i->type: $i->unit).' ('.$i->type.')' ?>
				</span>
			</div>
		</div>
		<?php } ?>
		<input type="hidden" name="modelData" value='{"PutStuff": "Here"}'>
		<input type="hidden" name="server_id" value=<?=$DOMEInterface->getServerID()?>>
		<input type="hidden" name="interface_id" value=<?=$InterfaceID?>>
	</div>

	<div id="outputs" class="form-horizontal">
		<legend>Outputs</legend>
		<?php foreach ($ModelData->outParams as $i) { ?>
		<div class="control-group">
			<label class="control-label" title=<?=$i->type?> id=<?=$i->name?>><?=$i->name?></label>
			<div class="controls">
				<?php // determine input value
				$value = '';
				if (isset($_GET['complete'])){
					foreach($Messages as $m){
						if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
							$value = $m['new_val'][0];
						}
					}
				} else {
					if (isset($i->value)) {
						$value = $i->value;
					}
				}
				
				formatXPut($value, $i, true, "output");
				?>

				<span class="help-inline"><?= (($i->unit=='no unit')? $i->type: $i->unit).' ('.$i->type.')' ?>
				</span>
			</div>
		</div>
		<?php } ?>
	</div>
	<div id="actions" class="form-actions">
		<div class="btn-group">
                  <button type="submit" value="Run" class="btn btn-primary">Run</button>
                  <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                  <!-- dropdown menu links -->
                    <li>
                      <a href="javascript:downloadBulkInterfacePackage()">Download Bulk Form</a>
                    </li>
                    <li>
                      <input class="input" id="bulkRunner" name="bulkRunner" data-name="bulkRunner" autocomplete="off" enabled="true" type="file" onchange="return uploadBulkProcessingPackage();">Upload Bulk Package</input>
                    </li>
                  </ul>
                </div>

		<input type="reset" value="Clear Form" class="btn"/>
		<!-- HG new button to save order of input widgets -->
		<input type="button" value="Save Form" id="save_input_order" class="btn"/>
		<input type="button" value="Visualize" id="visualize" class="btn btn-success"/>
		<br>
                <div style="display: inline;">
			Your Run Time: <span id="my_runtime">0s</span> (average
			<?=Runtimes::getAvg($DOMEInterface->getID())?>
			s)
		</div>
	</div>
	</form>



<!--<h4>CSV Input Data</h4>
<div class="csvHolder">
    <div id="csv"></div>
</div>
<div id="downloadLink"></div>
-->

	<div id="outputMsg"></div>
	<div id="dvtVisual" class="modal hide fade in form-horizontal click" >
		<div class="modal-header">
			<h3>Data Visualization</h3>
		</div>
		<div id="visual" class="modal-body">
		</div>
		<div class="modal-footer">
		<div style="height:50px;width:100px;float:right;">
				<h5 style="width:100px;text-align:left">Y Values</h5>
				<select id="selectY" multiple class="selectpicker" data-width="100px">
				</select>
			</div>
		<div style="height:50px;width:100px;float:right;" >
				<h5 style="width:100px;text-align:left">X Values</h5>
				<select id="selectX" multiple class="selectpicker" data-width="100px">
				</select>
			</div>

			<div style="height:50px;width:100px;float:right;">
				<h5 style="width:100px;text-align:left">Axis Type</h5>
				<select id="axisType" style="width: 100px">
					<option value="Linear">Linear</option>
					<option value="Log-Log">Log-Log</option>
					<option value="Log-Linear">Log-Linear</option>
					<option value="Linear-Log">Linear-Log</option>
				</select>
			</div>
			<div style="height:50px;width:100px;float:right;">
				<h5 style="width:100px;text-align:left">Graph Type</h5>
				<select id="plotType" style="width: 100px">
					<option value="line">Line Graph</option>
					<option value="scatter">Scatter Plot</option>
				</select>
			</div>

			
		</div>
		<div class="modal-footer">
			<button id="plotGraph" class="btn btn-primary">Plot</button>
			<a id="saveGraph" class="btn btn-success">Save</a>
			<button id="addGraph" class="btn">Add Graph</button>
			<button id="clearSettings" class="btn btn-inverse">Clear</button>
			<button id="modalClose" class="btn">Close</button>
		</div>
	</div>


	<script>
var runTime=0,
		startTime=0,
		myRunTime=0,
		myTimer=null;
var uploadFilesRequired = false;

// BootStrap settings
$('#selectX').selectpicker();
$('#selectY').selectpicker();


/*  TODO: need to cut off the front end of the file name;  fakepath is added by IE and Chrome
$("#bulkRunner").change(function() {
    $("#shadow-filename").val(this.files && this.files.length ?
			      this.files[0].name : this.value.replace(/^C:\\fakepath\\/i, ''));
  })
*/

function uploadBulkProcessingPackage(){
  
  
  return alert('Bulk processing package uploaded and processing');
}


function makeCSV() {
  var csv = "";
  var sep = "";
  var formInputs = $("#inputs").sortable("toArray");

  var deleteValue = "";
  for (var i = 0; i < formInputs.length; i++) {
    if (formInputs[i] == deleteValue) {
      formInputs.splice(i, 1);
      i--;
    }
  }


  for(i=0; i < formInputs.length; i++) {
    var inputDiv = $("#label_"+formInputs[i]).sortable("toArray");
    var inputName = inputDiv[0].innerHTML;

    var inputProperties = $("#input_"+formInputs[i]).sortable("toArray");
    // sometime HTMLInputElement, others HTMLDocumentElement                                                                                                                          

    var type = inputProperties[0].type;
    if(type == "file")
      uploadFilesRequired = true;

    csv+= sep + inputName;
    sep= ",";
  }
  csv += "\n";

  return csv;
}



function downloadBulkInterfacePackage() {
  var csv = makeCSV();
  $("#csv").text(csv);
  

  window.URL = window.URL || window.webkiURL;
  var csvBlob = new Blob([csv]);
  var csvURL = window.URL.createObjectURL(csvBlob);
  var fileName = "bulkData.csv"

    //  zip


  var link = document.createElement("a");
  link.setAttribute("href",csvURL);
  link.setAttribute("download",fileName||"Download.csv");
  var event = document.createEvent('MouseEvents');
  event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
  link.dispatchEvent(event);

}


function updateTimer(){
	myRunTime+=100;
	$("#my_runtime").text((myRunTime/1000)+"s");
}

function startTimer(){
	myRunTime=0;
	myTimer=setInterval(updateTimer, 100);
}

function getArrayFromSelectorID(id) {
	var selectedValues = [];    
	$(id +" :selected").each(function(){
	        selectedValues.push($(this).val()); 
	    });    
	return selectedValues;
	
}

function populateSelectors() {
	
	// Get the Label names 
	var numLabels = [];
	$("#outputs label").each( function(index, v) {
		if ($(this).attr("title")=="Matrix" || $(this).attr("title")=="Vector" || $(this).attr("title")=="Array") {
			numLabels.push($(this).html());
		}
	});
	
	// Get the X and Y Selectors and set them to the values in numLabels array
	$.each(numLabels, function(key, value) {   
	     $('#selectY')
	         .append($("<option></option>")
	         .attr("value",value)
	         .text(value)); 
	     $('#selectX')
	     	.append($("<option></option>")
         	.attr("value",value)
         	.text(value)); 
	});
	$('#selectX').selectpicker('refresh');
	$('#selectY').selectpicker('refresh');
}

$(document).ready(function(){
	  $("#dvtVisual").hide();
	  populateSelectors();
	  
		var options = { 
		        target:        '#outputMsg',   // target element(s) to be updated with server response 
		        beforeSubmit:  preRequest,  // pre-submit callback 
		        success:       processRunModel,  // post-submit callback 
		        error: 		   function(msg){
		        	  				alert($.toJSON(msg));
		        	 		   },
		 
		        // other available options: 
		        //url:       url         // override for form's 'action' attribute 
		        type:      'post',        // 'get' or 'post', override for form's 'method' attribute 
		        dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
		        //clearForm: true        // clear all form fields after successful submit 
		        //resetForm: true        // reset the form after successful submit 
		 
		        // $.ajax options can be used here too, for example: 
		        timeout:   <?php echo returnByteValue(ini_get('upload_max_filesize'))/1000; ?>
		    }; 
	 
	    // bind form using 'ajaxForm' 
	    $('#inputsForm').ajaxForm(options);
		// bind to the form's submit event 
	    //$('#inputsForm').submit(function() { 
	    //    // inside event callbacks 'this' is the DOM element so we first 
	    //    // wrap it in a jQuery object and then invoke ajaxSubmit 
	    //    $(this).ajaxSubmit(options); 
	 
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	    //    return false; 
	    //}); 
});

//HG: when save inputs order button is clicked 
//save the order to the input widgets in DB
//parameter ID is used to compare inputs
function getNewInputsOrder(id) {
	// get new order of the inputs
	var idsReOrdered = $("#inputs").sortable("toArray");
	var json_str = JSON.stringify( idsReOrdered );
	return json_str;
}

//HG: ajax call
$('#save_input_order').click(function() {
	// send request to the server
	$.ajax({
		type: 'POST',
		url: 'ajax/save_form.php',
		dataType: 'json',
		data: {
			diid: <?=getIntFromRequest('diid')?>,
			inputs_array: getNewInputsOrder()
		},
		error: function(msg) {
			alert("Return[msg]: " + msg.msg);
			alert("Error: " + msg.error);
		},
		success: function(msg){
			alert("The form has been saved!");
		}
	});	
	$('#status').html("Finished Saving Form!");
});

$('#visualize').click(function() {
	// Here is where we want to load the information from
	// the database
	
	/* 1. Make an AJAX Call out to the database for the Model ID #
	   2. If no entry in the database, set nothing.
	   3. If there is an entry, then set the graph object
	*/

	// This function removes the graph and prepares for regraphing
	clearCanvas();
	var jsonResponse, savedSettings;
	var request = $.ajax({
		type: 'GET',
		cache: false,
		url: 'ajax/get_graph_settings.php',
		dataType: 'json',
		data: {
			model_id: <?=$DOMEInterface->getID()?>
		},
		error: function(msg) {
			alert("Return[msg]: " + msg.msg);
			alert("Error: " + msg.error);
		},
		success: function(msg){
			jsonResponse = msg.msg;
		}});
		request.done(function (msg) {
			//alert(jsonResponse);
			$('#status').html("Running Visualizer...");
			$('#dvtVisual').slideDown("slow");
			savedSettings = $.parseJSON(jsonResponse);

			if (savedSettings !== null) {
				modelViz.settings = savedSettings;
				
	 			if (modelViz.settings.axisType !== "") {
	 				modelViz.plotGraphs();			
	 			}
	 			/* if (modelViz.graph.plotType !== "") {
				if (modelViz.graph.plotType  === "line") {
					modelViz.lineGraph();
				} else  {
					if (modelViz.graph.plotType === "scatter") {
						modelViz.scatterGraph();
					}
				}
			} */
				
				
			}
		});
});

$('.click').mouseup(function() {
	$('#dvtVisual').show();
	$('#dvtVisual').draggable();
	});

$('#clearSettings').click(function() {
	modelViz.clear();
	$('#visual').html("");
	
});
	
$('#modalClose').click(function() {
	modelViz.clear();
	$('#dvtVisual').slideUp("slow");
});

$('#addGraph').click(function() {
	 // Remove Previous Graph
	// $("#visual").html("");
 	 clearCanvas();
	 modelViz.clear();
	 // Create new Graph Object
	var newGraph = modelViz.createGraph();
	newGraph.plotType = $("#plotType").val();
	newGraph.axisType = $("#axisType").val();
	newGraph.xValueNames = getArrayFromSelectorID("#selectX");
	newGraph.yValueNames = getArrayFromSelectorID("#selectY");
	
	newGraph.setGraphData();

	// Updates the x and y ranges for the Plot Canvas
	modelViz.updatePlotRanges(newGraph.xRange, newGraph.yRange);

	modelViz.addGraph(newGraph);

	modelViz.plotGraphs();
	//if (modelViz.settings.axisType !== "") {
	 				
	/*  if (modelViz.graph.plotType !== "") {
					if (modelViz.graph.plotType  === "line") {
						modelViz.lineGraph();
					} else  {
						if (modelViz.graph.plotType === "scatter") {
							modelViz.scatterGraph();
						}
					}
				}  */

	$('#status').html("Finished!");	
	 
});

$('#plotGraph').click(function() {
	/* 
	// Store the Graph Properties to the modelViz Object
	// This will be used to plot the data
	modelViz.graph.plotType = $("#plotType").val();
	modelViz.graph.axisType = $("#axisType").val();
	modelViz.graph.xValueNames = getArrayFromSelectorID("#selectX");
	modelViz.graph.yValueNames = getArrayFromSelectorID("#selectY");
	
	if (modelViz.graph.plotType === "line") {
		modelViz.lineGraph();
	} else  {
		if (modelViz.graph.plotType === "scatter") {
			modelViz.scatterGraph();
		}
	}
*/
	// Remove Previous Graph
	 clearCanvas();
	 modelViz.clear();

	 // Create new Graph Object
	var newGraph = modelViz.createGraph();
	newGraph.plotType = $("#plotType").val();
	newGraph.axisType = $("#axisType").val();
	newGraph.xValueNames = getArrayFromSelectorID("#selectX");
	newGraph.yValueNames = getArrayFromSelectorID("#selectY");
	newGraph.setGraphData();

	// Updates the x and y ranges for the Plot Canvas
	modelViz.updatePlotRanges(newGraph.xRange, newGraph.yRange);

	modelViz.addGraph(newGraph);
	// Important for establishing the general axis type for a canvas
	modelViz.settings.axisType = $("#axisType").val();

	modelViz.plotGraphs();
	 

	$('#status').html("Finished!");	
});


$('#saveGraph').click(function() {

	// This is where the settings are saved for the graph
	// run.  This method goes to the database and saves
	// the layout of the graph.
	// TODO:  Test to see if there is a data layout to save!
	$.ajax({
			type: 'POST',
			url: 'ajax/save_graph_settings.php',
			dataType: 'text',
			data: {
				model_id: <?=$DOMEInterface->getID()?>,
				graph_settings: modelViz.createJSONData()
			},
			error: function(msg) {
				alert("Return[msg]: " + msg.msg);
				alert("Error: " + msg.error);
			},
			success: function(msg){
				alert("Save Success!");
			}
	});	
	$('#status').html("Finished Saving the Graph!");	
});
// clear input/output fields
$('#clear').click(function() {
	$('#inputs > .control-group').find('.input').val('');
	$('#outputs > .control-group').find('.output').val('');
});
function clearCanvas () {
	$("#visual").html("");
}
//pre-submit callback
function preRequest($formData, jqForm, options) {
    // formData is an array; here we use $.param to convert it to a string to display it
    // but the form plugin does this for you automatically when it submits the data
    //var queryString = $.param(formData);
    var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(i, tag){
		$(this).prop('disabled',true);
		var bulkRunner = false;
		if($(this).data('name').search("bulkRunner") > -1) {
		  Data[$(this).data('name')]=eval($(this).val());
		} else {
		  // the next few lines are VERY dependent on the layout of the form used to run a service.
		  // ToDo: need better way to check input type.
		  var parent = $(this).prop('parentElement');
		  var child_1 = parent.children[1];
		  var input_type = child_1.innerHTML;
		  if(input_type.search("(String)") >= 0) {
		    Data[$(this).data('name')]=$(this).val();
		  } else {
		    try {
		      Data[$(this).data('name')]=eval($(this).val());
		    }
		    catch (err) {
		      Data[$(this).data('name')]=$(this).val();
		    }
		  }
		}
	  });

	var queryString = $.toJSON(Data);
	//formData.push({ name: 'data', value: queryString });
	//formData.push({ name: 'server_id', value: <?=$DOMEInterface->getServerID()?> });
	//formData.push({ name: 'interface_id', value: <?=$InterfaceID?> });
	$.each($formData, function(index, value) {
		if (value.name == "modelData") { 
			value.value = queryString; 
		}
	});
	var dataEl = $('#inputs > modelData');
	dataEl.value = queryString;
    //$formData[0].value = queryString;
    //formData[1].value = <?=$DOMEInterface->getServerID()?>;
    //formData[2].value = <?=$InterfaceID?>;

    // jqForm is a jQuery object encapsulating the form element.  To access the
    // DOM element for the form do this:
    // var formElement = jqForm[0];

    // here we could return false to prevent the form from being submitted;
    // returning anything other than false will allow the form submit to continue
    return true;
}

// post-submit callback
function showResponse(responseText, statusText, xhr, $form)  {
    // for normal html responses, the first argument to the success callback
    // is the XMLHttpRequest object's responseText property

    // if the ajaxSubmit method was passed an Options Object with the dataType
    // property set to 'xml' then the first argument to the success callback
    // is the XMLHttpRequest object's responseXML property

    // if the ajaxSubmit method was passed an Options Object with the dataType
    // property set to 'json' then the first argument to the success callback
    // is the json data object returned by the server

    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText +
        '\n\nThe output div should have already been updated with the responseText.');
}

/*
$('#submit').click(function(){
    $("#status").html("Initializing...");

	//var formData = new FormData();     
    var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(i, tag){
		$(this).prop('disabled',true);
		//if ($(this).attr("type") == 'file') {
		//	$.each($(tag)[0].files, function(i, file) {
		//		//alert("Found file: " + tag.value);
	        //    formData.append(tag.value, file);
	        //});
		//}
		try {
			Data[$(this).data('name')]=eval($(this).val());
		}
		catch (err) {
			Data[$(this).data('name')]=$(this).val();
		}
	});

	Data=$.toJSON(Data);
	//formData.append('data', Data);
	//formData.append('server_id', <?=$DOMEInterface->getServerID()?>);
	//formData.append('interface_id', <?=$InterfaceID?>);

	var $me=$(this);

	// clear output fields
	$('#outputs').children('.control-group').val('').addClass('error');
	$('#inputs').children('.control-group').addClass('error');
	
	$me.prop('disabled',true);

	formData = {data:Data, server_id:<?=$DOMEInterface->getServerID()?>, interface_id:<?=$InterfaceID?>};
	
	$.post('ajax/model_run.php', formData, processRunModel);
	//$.ajax({
	//    url: 'ajax/model_run.php',
	//    data: formData,
	//    cache: false,
	//    contentType: false,
	//    processData: false,
	//    type: 'POST',
	//    success: processRunModel
 	//});
 */
 
 	var pollTimer=null,
			queueURL="",
			totalMessages=0,
			received=0,
			runningID=0;
 	var $me=$('#actions');
	$Inputs=$('.input');
	//function processRunModel(msg){
	function processRunModel(responseObj, statusText, xhr, $form){ 
		msg=responseObj; //$.parseJSON(msg);
		if (msg.error==false){
			startTime=new Date().getTime();
			startTimer();
			$("#status").html("Running...");
			
			queueURL=msg.queue_url;
			runningID=msg.running_id;
			totalMessages = 0;
			received = 0;
			poll();
		}else{
			alert(msg.msg);
		}
	}
	
	function poll(){
		if (pollTimer==null) {
			pollTimer=setInterval(function(){poll();},3000);
		}
		
		$.ajax({
			type: 'POST',
			url: 'ajax/poll_queue.php',
			dataType: 'json',
			data: {
				queue_url: queueURL,
				running_id:runningID,
				timeStamp: new Date().getTime()
			},
			error: function(msg) {
				alert("Error in polling queue: " + msg.msg);
				clearInterval(pollTimer);
				clearTimeout(myTimer);
			},
			success: function(msg){
				if (msg.error==true){
					alert(msg.msg);
					clearInterval(pollTimer);
					clearTimeout(myTimer);
				}
				else {
	                if (msg.messages && (msg.messages[0] != "null")){
	                    //Loop through all messages
	                    var i, l, key, m, field;
	                    for(i=0, l=msg.messages.length; i<l; i++){
	
	                        key=msg.messages[i];
	                        debugMsg("Have keys[" + msg.messages.length + "]: " + key);
	                        received++;
	
	                        m=$.parseJSON(key);
	                        //m=msg;
	
	                        //Check if message received is the message that specifies how many messages there should be
	                        if (m.event=="class mit.cadlab.dome3.api.ParameterStatusChangeEvent" && 
	    	                        ((m.new_val=="SUCCESS") || (m.new_val=="FAILURE"))){
	                            totalMessages=m.param;
	                        }
	
	                        //If a value changed message is received, change it on the page
	                        if (m.event=="class mit.cadlab.dome3.api.ParameterValueChangeEvent"){
	                                field = $('input[data-name="'+m.param+'"]');
	                                oStr = Object.prototype.toString.call(m.new_val[0]);
	                                switch (Object.prototype.toString.call(m.new_val[0])) {
	                                	case "[object Object]":
		                                	/*
		                                		This Code was added to flatten any array structures to the
		                                		output.  It helps with plotting data and it also makes it easier
		                                		to see without all the brackets.
		                      
		                                	*/
	                                		//var unflattenedData = m.new_val[0].data;
	                                		//var flattenedData = unflattenedData.reduce(function(a, b) {
	                                		//    return a.concat(b);
	                                		//});
	                                		//field.val($.toJSON(flattenedData));
	                                		field.val($.toJSON(m.new_val[0].data));
		                                	break;
	                                	case "[object Array]":
	                                		field.val($.toJSON(m.new_val[0]));
		                                	break;
		                                default:	
			                                if (field[0].type != 'file') {
		                                		field.val($.toJSON(m.new_val[0]));
			                                	//field_open = $('input[data-name="'+m.param+'_open"]');
			                                	var field_open = document.getElementById(m.param+'_open')
			                                	if (field_open != null) {
			                                		field.val($.toJSON(m.new_val));
				                                	field_open.href = m.new_val;
			                                	}
			                                }
                                			break;
	                                }
	                                field.closest('.control-group').removeClass('error').addClass('success');
	                        }
	
	                        //If messages received equals needed messages, stop polling
	                        debugMsg("totalMessages: " + totalMessages + "    received: " + received);
	                        if (totalMessages==received){
	                            //Get run time length
	                            runTime=new Date().getTime() - startTime;
	
	                            //Stop timers
	                            clearTimeout(myTimer);
	                            clearInterval(pollTimer);
	
	                            //Delete the queue
	                            $.ajax({
	                                type: 'POST',
	                                url: 'ajax/delete_queue.php',
	                                data: {
	                                    queue_url: queueURL
	                                },
	                                dataType: 'json',
	                                success: function(msg){
		                             //if (typeof msg.error  != 'undefined') {  
	                                    //if (msg){
	                                    //    alert($.toJSON(msg));
	                                   // }
		                             //}
	                                }
	                            });
	
	                            //Send run results to a script to log them
	                            $.post('ajax/run_complete.php',{runningID:runningID, runtime:runTime, interface_id:<?=$InterfaceID?>});
	                            $("#status").html("Finished");
	
	                            //Reset form
	                            $('#outputs').children('.control-group').removeClass('success');
	                            $('#inputs').children('.control-group').removeClass('success');
	                            $me.prop('disabled',false);
	                            $Inputs.each(function(){
	                                $(this).prop('disabled',false);
	                            });
	                            if (m.new_val=="FAILURE") {
		                            alert("Error: " + m.id.idString);
	                            }
	                        }
	                    } // for each message received
	                    if (totalMessages!=received){
	                    	debugMsg("Messages received but need more, calling poll() again");
                            //poll();
                        }
	                } // if messages
                    else {
                    	debugMsg("No messages received, calling poll() again");
                        //poll();
                    }
				}
			}
		});
	}
//});

function debugMsg(msg) {
	if (typeof console != "undefined") {
		console.log(msg);
	}
}
</script>
<script>
// HG script to make input widjets draggable
$(function() {
    $( "#inputs" ).sortable();
    $(".control-label").disableSelection();
    $(".help-inline").disableSelection();
    $(".inputs_legend").disableSelection();
    $(".inputs_legend").sortable('disable');
    // this line prevents "Inputs" label from beeing draggable,
    // but it makes all input text fields read-only in Chrome
    //$( "#inputs" ).sortable({ cancel: '.inputs_legend' });
    // this line makes all input entries read-only in Firefox
    //$( "#inputs" ).disableSelection();
  });
</script>
