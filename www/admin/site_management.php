<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'include/html.php';
require_once $gfwww.'include/role_utils.php';

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));
$warning_msg = htmlspecialchars(getStringFromRequest('warning_msg'));



$HTML->addStylesheet('admin.css');
//site_admin_header(array('title'=>'Quick Pass'));
site_admin_header(array('title'=>_('Site Management')), 4);

$Layout->col(12,true);
echo 'This is site management page!';
$Layout->endcol();
?>



<?php 
// Set the footer
$Layout->endcol();
include $gfwww.'help/help.php';
site_admin_footer(array());
?>