<?php
/**
 *  quickpass.php
 * 	author: Junrong Yan
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;


require_once ('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once $gfwww . 'admin/admin_utils.php';
require_once $gfwww . 'project/admin/project_admin_utils.php';
use_stylesheet ( '/admin/admin.css' );

session_require_global_perm ( 'forge_admin' );

//Purpose: approve project
//Given:   group_id
//Return:  if this project has been approved, then return true. Otherwise, return false.
function update_user_status(&$user, $new_status) {
	global $feedback;
	global $error_msg;
	if (! $user || ! is_object ( $user )) {
		exit_error(_('Could Not Get User'),'admin');
		return false;
	} else if ($user->isError ()) {
		exit_error ( $user->getErrorMessage (), 'admin' );
		return false;
	}

	db_begin();
	if (!$user->setStatus($new_status)) {
		$error_msg .= $user->getErrorMessage();
		db_rollback();
		$error_msg .= sprintf ( _ ( 'Error when change status of User: %1$s' ), $user->getUnixName () ) . '<br />';
		return false;
	}else{
		$feedback .= sprintf ( _ ( 'Successfully change the status of User: %1$s' ), $user->getUnixName ())._('to status ').$new_status;
	}
	db_commit();
	//$feedback .= _('Updated ').$user->getUnixName()._('status to ').$new_status;
	return true;
}

if (getStringFromRequest ( 'change_status', false )) {
	$user_id = getIntFromRequest ('user_id');
	//echo $user_id;
	$user = user_get_object ($user_id );
	$new_status = getStringFromRequest ($user_id);
	//echo $new_status;
	update_user_status($user,$new_status);
	
}	
	

//Purpose: once click reject button, show the hidden textarea
//         & change button color to red.
$JS='$(function(){
    $("body").on("change","select",function(e){
		var btn_id=e.target.name;
			
        var me=document.getElementById(btn_id);
		var s=me.value;	
		if(me.className!= "btn btn btn-success"){		     
		     me.className="btn btn btn-success";
		     return false;
     }
})
});';
add_js ( $JS );

//Purpose: once change the task option of drop-list, update the page and
//         reload the content of the request.
$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})        
   
});';
add_js($JS_dropdown_list);


//Purpose: once change the sort option of drop-list, updage the page and
//         reload the content of the request..
$JS_sort_by = '$(function(){
    $("#sort_by").change(function(){
		var $new_rank_page=$(this).val();
		location.href=$new_rank_page;
})

});';
add_js($JS_sort_by);
			

site_admin_header ( array ('title' => _ ( 'Quick Pass Reject Users' )), 0 );
echo '<div class="page">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="index">Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users" selected>Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form action="quickpass_reject_users.php" method="post">
							<div class="input-append">
                				<input id="search_query" size="16" name="search_key" type="text" style="width: 108px;">
								<input type="hidden" name="groupsearch" value="1" />
								<button class="btn btn-primary" type="submit">Search</button>
            				</div>
						</form>
					</div>
				</div> 		
';

//Purpose: get the value of "search_key"
$search = trim(getStringFromRequest('search_key'));
//echo $search;
//Purpose: get the value of "order_by"
$order_value=getStringFromRequest('order_by');
//Purpose: change the selected attr of option in sorting drop-list.
$optionstr1=$order_value == 1? "selected" : "";
$optionstr2=$order_value == 2? "selected" : "";
$optionstr3=$order_value == 3? "selected" : "";
$optionstr4=$order_value == 4? "selected" : "";


if (!$search) {
	if($order_value){
		//Purpose: sorting by time and user_name
		switch($order_value){
			case 1:
				$res_grp = db_query_params ( "SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' ORDER BY add_date DESC", array (), $LIMIT );
				break;
			case 2:
				$res_grp = db_query_params ( "SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' ORDER BY add_date ASC", array (), $LIMIT );
				break;
			case 3:
				$res_grp = db_query_params ( "SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' ORDER BY realname", array (), $LIMIT );
				break;
			case 4:
				$res_grp = db_query_params ( "SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' ORDER BY realname DESC", array (), $LIMIT );
				break;
		}
	}else{

		$res_grp = db_query_params ( "SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' ORDER BY add_date DESC", array (), $LIMIT );
	}
//$res_grp = db_query_params ( "SELECT * FROM groups g, group_history gh WHERE g.group_id=gh.group_id AND g.status='D' AND gh.field_name='rejected'", array (), $LIMIT );

$rows = db_numrows( $res_grp );
if ($rows) {
echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
	if ($rows > $LIMIT) {
		print "<p>" . _ ( 'Rejected users:' ) . "$LIMIT+ ($LIMIT shown)</p>";
	} else {
		print "<p>" . _ ( 'Rejected users:' ) . "$rows</p>";
	}
	
	echo '</div>
			<div class="body-dropdownlist">
				<select id="sort_by" style="width: 100%;">
	               <option name="1" value="quickpass_reject_users?order_by=1" '.$optionstr1.'>Time:Newst</option>
	               <option name="2" value="quickpass_reject_users?order_by=2" '.$optionstr2.'>Time:Oldest</option>
		
					<option name="3" value="quickpass_reject_users?order_by=3" '.$optionstr3.'>Name:A-Z</option>
					<option name="4" value="quickpass_reject_users?order_by=4" '.$optionstr4.'>Name:Z-A</option>
				</select>
		
			</div>
	</div>';

//Purpose: output the detail information of projects
		reject_users_bsform($res_grp);

		echo '
				</div>
				</div>';
} else {
	//echo _( 'No Pending Projects to Approve' );
	echo '
<div class="widget-body">
   <div class="body_header">
            <div class="body-text">
                <p>No rejected projects ...</p>
            </div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
				
				</div>
				</div>';
    }
    
}else{
	$status = 'D';
	//$is_public = getIntFromRequest('is_public', -1);
	$crit_desc = '' ;
	//$qpa = db_construct_qpa () ;
	
	//Purpose: get the value of "search_key"
	$search_crit=getStringFromRequest('search_key');
	//SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected'
	
	$qpa1_num="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (user_id=$1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY add_date DESC";
	$qpa1_str="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY add_date DESC";
	
	$qpa2_num="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (user_id=$1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY add_date ASC";
	$qpa2_str="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY add_date ASC";
	
	$qpa3_num="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (user_id=$1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY realname";
	$qpa3_str="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY realname";
	
	$qpa4_num="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (user_id=$1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY realname DESC";
	$qpa4_str="SELECT * FROM users u, user_history uh WHERE u.user_id=uh.user_id AND u.status='D' AND uh.field_name='rejected' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY realname DESC";
	
	if($order_value){
		//Purpose: sorting the searching results by time and group_name
		switch($order_value){
			case 1:
				//sorting based on the time from newest to oldest
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa1_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa1_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 2:
				//sorting based on the time from oldest to newest
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa2_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa2_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 3:
				//sorting based on the name from a to z
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa3_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa3_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 4:
				//sorting based on the time from z to a
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa4_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa4_str, array(strtolower("%$search_crit%")));
				}
				break;
		}
		
	}else{
		//default sorting based on the time from newest to oldest
		if(is_numeric($search)) {
			
			$res_grp=db_query_params($qpa1_num, array($search, strtolower("%$search%")));
		} else {
			
			$res_grp=db_query_params($qpa1_str, array(strtolower("%$search%")));
		}
	}
	
	if ($status) {
		//$qpa = db_construct_qpa ($qpa, ' AND status=$1', array ($status)) ;
		$crit_desc .= " status=$status";
	}
	
	if ($crit_desc) {
		$crit_desc = "(".trim($crit_desc).")";
	}
	
	
	$rows=db_numrows($res_grp);
	if($rows){
		echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		print '<p><strong>'.sprintf(ngettext('Found users matching <em>%s</em>: %d match', 'Found users matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
		echo '</div>
			<div class="body-dropdownlist">
			<select id="sort_by" style="width: 100%;">
			<option name="1" value="quickpass_reject_users?order_by=1&search=123&search_key='.$search_crit.'" '.$optionstr1.'>Time:Newst</option>
			<option name="2" value="quickpass_reject_users?order_by=2&search=123&search_key='.$search_crit.'" '.$optionstr2.'>Time:Oldest</option>
	
			<option name="3" value="quickpass_reject_users?order_by=3&search=123&search_key='.$search_crit.'" '.$optionstr3.'>Name:A-Z</option>
			<option name="4" value="quickpass_reject_users?order_by=4&search=123&search_key='.$search_crit.'" '.$optionstr4.'>Name:Z-A</option>
			</select>
	
			</div>
	</div>';
		//Purpose: output the detail information of projects
		reject_users_bsform($res_grp);
		echo '
				</div>
				</div>';
		
	}else{
		echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		print '<p><strong>'.sprintf(ngettext('Found users matching <em>%s</em>: %d match', 'Found users matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
		echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
					
					</div>
					</div>';
	}
		
}

site_admin_footer(array());
?>
                      