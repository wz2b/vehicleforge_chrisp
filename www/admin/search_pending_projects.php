<?php
/**
 * Site Admin generic user/group search page
 *
 * This is the single page for searching/selection of users/groups for
 * Site Admin. Currently, it supports querying by (sub)string match in
 * string user/group properties (names, fullnames, email) and status.
 * If new search criteria will be required, they should be added here,
 * not any other (new) page.
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$LIMIT = 50;
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
use_stylesheet ( '/admin/admin.css' );

function activate_group($group_id) {
	global $feedback;
	global $error_msg;

	$group = group_get_object ( $group_id );

	if (! $group || ! is_object ( $group )) {
		$error_msg .= _ ( 'Error creating group object' );
		return false;
	} else if ($group->isError ()) {
		$error_msg .= $group->getErrorMessage ();
		return false;
	}

	if ($group->approve ( session_get_user () )) {
		$feedback .= sprintf ( _ ( 'Approving Project: %1$s' ), $group->getUnixName () );
	} else {
		$error_msg .= sprintf ( _ ( 'Error when approving Project: %1$s' ), $group->getUnixName () ) . '<br />';
		$error_msg .= $group->getErrorMessage ();
		return false;
	}

	return true;
}

if (getStringFromRequest ( 'approve', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	activate_group ( $group_id );
} else if (getStringFromRequest ( 'reject', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	$response_id = getIntFromRequest ( 'response_id' );
	$add_to_can = getStringFromRequest ( 'add_to_can' );
	$response_text = getStringFromRequest ( 'response_text' );
	$response_title = getStringFromRequest ( 'response_title' );

	$group = group_get_object ( $group_id );
	if (! $group || ! is_object ( $group )) {
		exit_no_group ();
	} elseif ($group->isError ()) {
		exit_error ( $group->getErrorMessage (), 'admin' );
	}

	$response_size= strlen($response_text);
	if ($response_size==0) {
		//print "<p>" . _ ( 'Please input your reason to reject...' ) . "</p>";
		$error_msg .= sprintf ( _ ( 'Please input your reason to reject' ), $group->getPublicName() );
	}else{

		if (! $group->setStatus ( session_get_user (), 'D' )) {
			exit_error ( _ ( 'Error during group rejection: ' ) . $this->getErrorMessage (), 'admin' );
		}


		$group->addHistory ( 'rejected', 'x' );
		$group->sendRejectionEmail ( 0, $response_text );

	}

	/*
	 * // Determine whether to send a canned or custom rejection letter and send it if( $response_id == 100 ) { $group->sendRejectionEmail(0, $response_text); if( $add_to_can ) { add_canned_response($response_title, $response_text); } } else { $group->sendRejectionEmail($response_id); }
	*/
} elseif (getStringFromRequest ( 'approve-all', false )) {
	if ($Groups = getStringFromRequest ( 'list_of_groups', false )) {
		$List = explode ( ',', $Groups );
		array_walk ( $List, 'activate_group' );
	}
}



/*
$JS='$(function(){

    $("#rejection_button").click(function(){
        var $me=$(this);

        if (!$me.hasClass("btn-danger")){
            $("#row_rejection_text").slideDown();
            $(this).addClass("btn-danger");
            return false;
        }
    });
});';
*/

$JS='$(function(){
    $("body").on("click","button",function(e){
		var btn_id=e.target.id;
        var me=document.getElementById(btn_id);
		var txt_id=me.value;
		var txtarea=document.getElementById(txt_id);

		if(me.className!= "btn btn btn-danger"){
		     txtarea.className="control-group";
		     me.className="btn btn btn-danger";
		     return false;
     }



})
});';
add_js ( $JS );

$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})
  
});';
add_js($JS_dropdown_list);




site_admin_header(array('title'=>_('Admin Search Results')));
echo '<div class="page">
		<div class="row">
		 <div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="quickpass" selected>Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form name="gpsrch" action="search_pending_projects.php" method="post" >
		                    <input type="text" name="search"  placeholder="search..." />
		         
		                    <input type="hidden" name="groupsearch" value="1" />
							
						</form>
					</div>
				</div> 
';

$search = trim(getStringFromRequest('search'));
if (!$search) {
	$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P'", array (), $LIMIT );
	$rows = db_numrows( $res_grp );
	
	if ($rows) {
		echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		if ($rows > $LIMIT) {
			print "<p>" . _ ( 'Pending projects:' ) . "$LIMIT+ ($LIMIT shown)</p>";
		} else {
			print "<p>" . _ ( 'Pending projects:' ) . "$rows</p>";
		}
	
		echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
	</div>';
	
	
		$BsForm = new BsForm;
	    $i=1;
		while ( $row_grp = db_fetch_array ( $res_grp ))  {
			$u = user_get_object($row_grp['user_id'] );
			echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
			
			->head ( $row_grp ['group_name'] )
			->group ( 'Project Admin',
					new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$u->getUnixName()
					))
			)
			->group ( 'Project Members',
					new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' )
					) )
			)
	
			->group ( 'Purpose',
					new Custom ( $row_grp ['register_purpose'] )
			)
			->group ( 'Register Time:',
					new Custom ( date('Y-m-d',$row_grp['register_time']))
			)
			->group ( 'Project Details',
					new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' )
					) )
			)
			->group ( 'Project URL',
					new Custom ( $row_grp ['unix_group_name'] ),
					new Hidden ( array (
							'name' => 'group_id',
							'value' => $row_grp ['group_id']
					) )
			)
			->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text'.$i ),
					new Textarea ( '', array (
							'name' => 'response_text'
					) )
			)
			->render_fieldset()
			->right (
					new Submit('Approve',array(
							'class'=>'btn btn-success',
							'name'=>'approve',
							'value'=>1
					)),
					new Submit('Reject',array(
							'class'=>'btn',
							//'id'=>'rejection_button',
							'id'=> $row_grp ['group_id'],
							'name'=>'reject',
							'value'=>'row_rejection_text'.$i,
					))
			)
			->render ();
			echo "<hr />";
			$i=$i+1;
		}
	
		// list of group_id's of pending projects
		$arr = util_result_column_to_array ( $res_grp, 0 );
		$group_list = implode ( $arr, ',' );
	
		echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="' . $group_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
                </div>
        		</div>
        		</div>
        		</div>';
	} else {
		//echo _( 'No Pending Projects to Approve' );
		echo '
<div class="widget-body">
   <div class="body_header">
            <div class="body-text">
                <p>No pending projects to approve</p>
            </div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
				</div>
				</div>
				</div>
				</div>';
	}
	
	
	              
     
}else {
//begin if($groupsearch)
if (getStringFromRequest('groupsearch')) {
	$status = 'P';
	$is_public = getIntFromRequest('is_public', -1);
	$crit_desc = '' ;
	$qpa = db_construct_qpa () ;
	
	if(is_numeric($search)) {
		$qpa = db_construct_qpa ($qpa, 'SELECT DISTINCT * FROM groups
WHERE (group_id=$1 OR lower (unix_group_name) LIKE $2 OR lower (group_name) LIKE $2)',
					    array ($search,
						   strtolower ("%$search%"))) ;
	} else {
		$qpa = db_construct_qpa ($qpa, 'SELECT DISTINCT * FROM groups WHERE (lower (unix_group_name) LIKE $1 OR lower (group_name) LIKE $1)',
					    array (strtolower ("%$search%")));
	}

	if ($status) {
		$qpa = db_construct_qpa ($qpa, ' AND status=$1', array ($status)) ;
		$crit_desc .= " status=$status";
	}

	if ($crit_desc) {
		$crit_desc = "(".trim($crit_desc).")";
	}

	$result = db_query_qpa ($qpa) ;
	$rows=db_numrows($result);
	if($rows){
echo '	
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		print '<p><strong>'.sprintf(ngettext('Group search with criteria <em>%s</em>: %d match', 'Group search with criteria <em>%s</em>: %d matches', $rows), $crit_desc, $rows).'</strong></p>';
echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>';
		
		$BsForm = new BsForm;
		while ($row_grp = db_fetch_array ($result)){
			$u = user_get_object($row_grp['user_id'] );
			echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
			->head ( $row_grp ['group_name'] )
			->group ( 'Project Admin',
					new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$u->getUnixName()
					))
			)
			->group ( 'Project Members',
					new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' )
					) )
			)
			
			->group ( 'Purpose',
					new Custom ( $row_grp ['register_purpose'] )
			)
			->group ( 'Register Time:',
					new Custom ( date('Y-m-d',$row_grp['register_time']))
			)
			->group ( 'Project Details',
					new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' )
					) )
			)
			->group ( 'Project URL',
					new Custom ( $row_grp ['unix_group_name'] ),
					new Hidden ( array (
							'name' => 'group_id',
							'value' => $row_grp ['group_id']
					) )
			)
			->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text' ),
					new Textarea ( '', array (
							'name' => 'response_text'
					) )
			)
			->render_fieldset()
			->right (
					new Submit('Approve',array(
							'class'=>'btn btn-success',
							'name'=>'approve',
							'value'=>1
					)),
					new Submit('Reject',array(
							'class'=>'btn',
							'id'=>'rejection_button',
							'name'=>'reject',
							'value'=> 1
					))
			)
			->render ();
			echo "<hr />";
		}
		$arr = util_result_column_to_array ( $result, 0 );
		$group_list = implode ( $arr, ',' );
		
		echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="' . $group_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
        		</div>
        		</div>
        		</div>
        		</div>';
		} else {
			//echo _( 'No Pending Projects to Approve' );
			echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
			print '<p><strong>'.sprintf(ngettext('Group search with criteria <em>%s</em>: %d match', 'Group search with criteria <em>%s</em>: %d matches', $rows), $crit_desc, $rows).'</strong></p>';
			echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
					</div>
					</div>
					</div>
					</div>';
		}
		

}//end if($groupsearch)

}//end if(!$search)

site_admin_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
