<?php
require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/ServiceCategories.class.php';
require_once $gfwww.'admin/admin_utils.php';

if (isset($_POST['cat_name'])){
	db_query_params("INSERT INTO service_categories(name,parent_id) VALUES($1,$2)", array(getStringFromRequest('cat_name'), getIntFromRequest('cat_parent')));
}

site_admin_header(array('title'=>_('Service Categories')));
$Layout->col(12,true);
?>
<button id="add_cat" class="btn">Add Category</button>
<div id="panel_add_cat">
	<form action="service_categories.php" method="post">
		Name: <input type="text" id="cat_name" name="cat_name" /><br />
		Parent: <select id="cat_parent" name="cat_parent"><option value="0">None</option>
		<?php
		$Categories=ServiceCategories::getAll();
		foreach($Categories as $Cat)
		/*$Result=db_query_params("SELECT * FROM service_categories");
		while($Cat=db_fetch_array($Result,null,PGSQL_ASSOC))*/
			echo '<option value="'.$Cat['cat_id'].'">'.$Cat['name'].'</option>';
		?>
		</select><br />
		<button id="submit_add_cat" class="btn">Submit</button>
	</form>
</div>

<?php
/*$Result=db_query_params("SELECT a.cat_id, a.name, a.parent_id as ParentID FROM service_categories AS a LEFT JOIN service_categories AS b ON a.parent_id=b.cat_id");

$Children=array();
while($Cat=db_fetch_array($Result,null,PGSQL_ASSOC)){
	$Children[$Cat['cat_id']][]=array($Result['cat_id']=>$Result['name']);
}

print_r($Children);*/

$Layout->endcol();
site_admin_footer();
?>