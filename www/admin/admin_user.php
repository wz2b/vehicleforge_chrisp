<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'include/html.php';
require_once $gfwww.'include/role_utils.php';
//use_stylesheet ( '/admin/admin.css' );

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));
$warning_msg = htmlspecialchars(getStringFromRequest('warning_msg'));



$HTML->addStylesheet('admin.css');
//site_admin_header(array('title'=>'Quick Pass'));
site_admin_header(array('title'=>_('User')), 2);
$Layout->col(12,true);


?>
<div class="widget">
	<div class="widget-header">
		<form name="gpsrch" action="admin_user.php" method="post" >
		      <input type="text" name="search_key"  placeholder="search..." class="form-control" style="float:left; width:90%;"/>
		      <input type="hidden" name="usersearch" value="1" />
		      <button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
	<div class="widget-body">
	<div class="table-responsive">
		<table class="table">
    		<tr>
  				<td><b>User Name</b></td>
  				<td><b>Real Name</b></td>
  				<td><b>User Status</b></td>
  				<td><b>Created Time</b></td>
  				<td><b>User Email</b></td>	
			</tr>
 			<tr>
 				<td>User Name</td>
  				<td>Real Name</td>
  				<td>User Status</td>
  				<td>Created Time</td>
  				<td>User Email</td>
			</tr>
			
  		</table>
	</div>
	<table class="table table-bordered">
		<tr>
			<td>Selenium Tester</td>
		</tr>
		<tr>
			<td>User ID</td>
			<th rowspan="7">Picture</th>
		</tr>
		<tr>
			<td>User Name</td>
		</tr>
		<tr>
			<td>Real Name</td>
		</tr>
		<tr>
			<td>Created Time</td>
		</tr>
		<tr>
			<td>Web Account Status:</td>
		</tr>
		<tr>
			<td>Enable/Disable/td>
		</tr>
		<tr>
			<td>User Email:</td>
		</tr>
		<tr>
			<td>I want to delete this user.</td>
		</tr>
		<tr>
			<td></td>
			<td>button button</td>
		</tr>
 
	</table>
	<hr />
	<div style="width:100%; height:auto; clear:both; margin-bottom: 50px;">
	<textarea class="form-control" style="width :98%;" rows="3"></textarea>
	<input  type="submit" name="post_comments" class="btn" value="send"/>
	</div>
	<hr />
	<table class="table table-bordered">
		<tr>
			<th rowspan="2">Picture</th>
			<td style="width:70%;">Selenium Tester</td>
			<td>Sep 22,2014 at 7:01am</td>
		</tr>
		<tr>
			<td>Project ID</td>
			<td>
				<input type="submit" name="delete_comment" class="btn" value="Delete"/>
				<input style="float:right;" type="submit" name="reply_comment" class="btn" value="Reply"/>
			</td>
		</tr>
	</table>
	
	
	</div> 
</div>



<?php
// user_get_object(id)
// group_get_object(id)
// comon include
// GF User
// GF Group

$Layout->endcol();
include $gfwww.'help/help.php';
site_admin_footer(array());
?>