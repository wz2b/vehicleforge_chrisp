<?php
/**
 *  quickpass.php
 * 	author: Junrong Yan
 * 	date:11/5/2014
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;


require_once ('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon . 'include/account.php';
require_once $gfwww . 'include/canned_responses.php';
require_once $gfwww . 'admin/admin_utils.php';
require_once $gfwww . 'project/admin/project_admin_utils.php';
require_once $gfcommon . 'tracker/ArtifactTypes.class.php';
require_once $gfcommon . 'forum/Forum.class.php';

use_stylesheet ( '/admin/admin.css' );


session_require_global_perm ( 'approve_projects' );
function activate_group($group_id) {
	global $feedback;
	global $error_msg;
	
	$group = group_get_object ( $group_id );
	
	if (! $group || ! is_object ( $group )) {
		$error_msg .= _ ( 'Error creating group object' );
		return false;
	} else if ($group->isError ()) {
		$error_msg .= $group->getErrorMessage ();
		return false;
	}
	
	if ($group->approve ( session_get_user () )) {
		$feedback .= sprintf ( _ ( 'Approving Project: %1$s' ), $group->getUnixName () );
	} else {
		$error_msg .= sprintf ( _ ( 'Error when approving Project: %1$s' ), $group->getUnixName () ) . '<br />';
		$error_msg .= $group->getErrorMessage ();
		return false;
	}
	
	return true;
}

if (getStringFromRequest ( 'approve', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	activate_group ( $group_id );
} else if (getStringFromRequest ( 'reject', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	$response_id = getIntFromRequest ( 'response_id' );
	$add_to_can = getStringFromRequest ( 'add_to_can' );
	$response_text = getStringFromRequest ( 'response_text' );
	$response_title = getStringFromRequest ( 'response_title' );
	
	$group = group_get_object ( $group_id );
	if (! $group || ! is_object ( $group )) {
		exit_no_group ();
	} elseif ($group->isError ()) {
		exit_error ( $group->getErrorMessage (), 'admin' );
	}
	
	$response_size= strlen($response_text);
	if ($response_size==0) {
		//print "<p>" . _ ( 'Please input your reason to reject...' ) . "</p>";
		$error_msg .= sprintf ( _ ( 'Please input your reason to reject' ), $group->getPublicName() );	
	}else{
		
		if (! $group->setStatus ( session_get_user (), 'D' )) {
		exit_error ( _ ( 'Error during group rejection: ' ) . $this->getErrorMessage (), 'admin' );
	}
	
	
	$group->addHistory ( 'rejected', 'x' );
	$group->sendRejectionEmail ( 0, $response_text );
	
	}
	
	/*
	 * // Determine whether to send a canned or custom rejection letter and send it if( $response_id == 100 ) { $group->sendRejectionEmail(0, $response_text); if( $add_to_can ) { add_canned_response($response_title, $response_text); } } else { $group->sendRejectionEmail($response_id); }
	 */
} elseif (getStringFromRequest ( 'approve-all', false )) {
	if ($Groups = getStringFromRequest ( 'list_of_groups', false )) {
		$List = explode ( ',', $Groups );
		/*echo  $List[0];
		foreach ($List as $peding_id){
			activate_group ( $peding_id );
		}*/
		array_walk ( $List, 'activate_group' );
	}
}




$JS='$(function(){

    $("#rejection_button").click(function(){
        var $me=$(this);

        if (!$me.hasClass("btn-danger")){
            $("#row_rejection_text").slideDown();
            $(this).addClass("btn-danger");
            return false;
        }
    });
});';

add_js ( $JS );

$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})        
   
});';
add_js($JS_dropdown_list);
				

site_admin_header ( array ('title' => _ ( 'Quick Pass' )), 0 );
echo '<div class="page">
		 <div class="row">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="index">Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news" selected>Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form action=" ">
							<div class="input-append">
                				<input id="search_query" size="16" name="search_key" type="text" style="width: 108px;">
								<input type="hidden" name="groupsearch" value="1" />
								<button class="btn btn-primary" type="submit">Search</button>
            				</div>
						</form>
					</div>
				</div> 	
';


site_footer ( array () );
?>