<?php
/**
 * Site Admin page for approving/rejecting new projects
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) Franck Villaume - Capgemini
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


// Show no more pending projects per page than specified here
$LIMIT = 50;

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/account.php';
require_once $gfwww.'include/canned_responses.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'project/admin/project_admin_utils.php';
require_once $gfcommon.'tracker/ArtifactTypes.class.php';
require_once $gfcommon.'forum/Forum.class.php';

session_require_global_perm ('approve_projects');

function activate_group($group_id) {
	global $feedback;
	global $error_msg;

	$group = group_get_object($group_id);

	if (!$group || !is_object($group)) {
		$error_msg .= _('Error creating group object');
		return false;
	} else if ($group->isError()) {
		$error_msg .= $group->getErrorMessage();
		return false;
	}

	if ($group->approve(session_get_user())) {
		$feedback .= sprintf(_('Approving Project: %1$s'), $group->getUnixName());
	} else {
		$error_msg .= sprintf(_('Error when approving Project: %1$s'), $group->getUnixName()).'<br />';
		$error_msg .= $group->getErrorMessage();
		return false;
	}

	return true;
}

if (getStringFromRequest('approve',false)) {
	$group_id = getIntFromRequest('group_id');
    activate_group($group_id);
} else if (getStringFromRequest('reject',false)) {
	$group_id = getIntFromRequest('group_id');
	$response_id = getIntFromRequest('response_id');
	$add_to_can = getStringFromRequest('add_to_can');
	$response_text = getStringFromRequest('response_text');
	$response_title = getStringFromRequest('response_title');
	
	$group = group_get_object($group_id);
	if (!$group || !is_object($group)) {
		exit_no_group();
	} elseif ($group->isError()) {
		exit_error($group->getErrorMessage(),'admin');
	}

	if (!$group->setStatus(session_get_user(), 'D')) {
		exit_error(_('Error during group rejection: ').$this->getErrorMessage(),'admin');
	}

	$group->addHistory('rejected', 'x');
    $group->sendRejectionEmail(0, $response_text);

	/*// Determine whether to send a canned or custom rejection letter and send it
	if( $response_id == 100 ) {

		$group->sendRejectionEmail(0, $response_text);

		if( $add_to_can ) {
			add_canned_response($response_title, $response_text);
		}

	} else {

		$group->sendRejectionEmail($response_id);

	}*/
}elseif(getStringFromRequest('approve-all',false)){
    if ($Groups=getStringFromRequest('group_list',false)){
        $List=explode(',',$Groups);
        array_walk($List,'activate_group');
    }
}

$JS='$(function(){
    $("#rejection_button").click(function(){
        var $me=$(this);

        if (!$me.hasClass("btn-danger")){
            $("#row_rejection_text").slideDown();
            $(this).addClass("btn-danger");
            return false;
        }
    });
});';
add_js($JS);

site_admin_header(array('title'=>_('Approving Pending Projects')), 'approve_projects');
$Layout->col(12,true);

// get current information
$res_grp = db_query_params("SELECT * FROM groups WHERE status='P'", array(), $LIMIT);

$rows = db_numrows($res_grp);

if ($rows){
    if ($rows > $LIMIT) {
        print "<p>"._('Pending projects:'). "$LIMIT+ ($LIMIT shown)</p>";
    } else {
        print "<p>"._('Pending projects:'). "$rows</p>";
    }

    $BsForm=new BsForm;

    while ($row_grp = db_fetch_array($res_grp)) {
        echo $BsForm->init(getStringFromServer('php_self'),'post',array('class'=>'form-horizontal'))
            ->head($row_grp['group_name'])
            ->group('Project URL',
                new Custom($row_grp['unix_group_name']),
                new Hidden(array(
                    'name'=>'group_id',
                    'value'=>$row_grp['group_id']
                ))
            )
            ->group('Description',
                new Custom($row_grp['register_purpose'])
            )
            ->group('Project Details',
                new Custom(util_make_link ('/admin/groupedit.php?group_id='.$row_grp['group_id'],_('Edit Project Details')))
            )
            ->group('Project Admin',
                new Custom(util_make_link ('/project/admin/?group_id='.$row_grp['group_id'],_('Project Admin')))
            )
            ->group('Project Members',
                new Custom(util_make_link ('/admin/userlist.php?group_id='.$row_grp['group_id'],_('View/Edit Project Members')))
            )
            ->group('Rejection Message',array('class'=>'hidden','id'=>'row_rejection_text'),
                new Textarea('',array(
                    'name'=>'response_text'
                ))
            )
            ->actions(
                new Submit('Approve',array(
                    'class'=>'btn btn-success',
                    'name'=>'approve',
                    'value'=>1
                )),
                new Submit('Reject',array(
                    'class'=>'btn',
                    'id'=>'rejection_button',
                    'name'=>'reject',
                    'value'=>1
                ))
            )
            ->render();

        echo "<hr />";
    }

    //list of group_id's of pending projects
    $arr=util_result_column_to_array($res_grp,0);
    $group_list=implode($arr,',');

    echo '
        <form action="'.getStringFromServer('PHP_SELF').'" method="post">
        <p style="text-align: center;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="'.$group_list.'" />
        <input type="submit" name="submit" value="'._('Approve All On This Page').'" />
        </p>
        </form>
        ';
}else{
    echo _('No Pending Projects to Approve');
}

$Layout->endcol();
site_admin_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
