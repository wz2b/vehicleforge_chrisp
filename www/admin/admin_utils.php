<?php
/**
 * Module of support routines for Site Admin
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
function site_admin_footer($params=array()) {
	site_footer($params);
}

function pending_project_bsform($res_grp){
	$BsForm = new BsForm;
	$i=1;
	while ( $row_grp = db_fetch_array ( $res_grp ))  {
		$u = user_get_object($row_grp['user_id'] );
		if ($u){
		echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
	
		->head ( $row_grp ['group_name'] )
		->group ( 'Project Admin',
				new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$u->getUnixName()
				))
		)
		->group ( 'Project Members',
				new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' )
				) )
		)
	
		->group ( 'Purpose',
				new Custom ( $row_grp ['register_purpose'] )
		)
		->group ( 'Register Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['register_time']))
		)
		->group ( 'Project Details',
				new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' )
				) )
		)
		->group ( 'Project URL',
				new Custom ( $row_grp ['unix_group_name'] ),
				new Hidden ( array (
						'name' => 'group_id',
						'value' => $row_grp ['group_id']
				) )
		)
		->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text'.$i ),
				new Textarea ( '', array (
						'name' => 'response_text'
				) )
		)
		->render_fieldset()
		->right (
				new Submit('Approve',array(
						'class'=>'btn btn-success',
						'name'=>'approve',
						'value'=>1
				)),
				new Submit('Reject',array(
						'class'=>'btn',
						//'id'=>'rejection_button',
						'id'=> $row_grp ['group_id'],
						'name'=>'reject',
						'value'=>'row_rejection_text'.$i,
				))
		)
		->render ();
		echo "<hr />";
		$i=$i+1;
		}else{
			
			echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
			
			->head ( $row_grp ['group_name'] )
			->group ( 'Project Admin',
					new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],_("non admin")
					))
			)
			->group ( 'Project Members',
					new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' )
					) )
			)
			
			->group ( 'Purpose',
					new Custom ( $row_grp ['register_purpose'] )
			)
			->group ( 'Register Time:',
					new Custom ( date('Y-m-d H:i',$row_grp['register_time']))
			)
			->group ( 'Project Details',
					new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' )
					) )
			)
			->group ( 'Project URL',
					new Custom ( $row_grp ['unix_group_name'] ),
					new Hidden ( array (
							'name' => 'group_id',
							'value' => $row_grp ['group_id']
					) )
			)
			->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text'.$i ),
					new Textarea ( '', array (
							'name' => 'response_text'
					) )
			)
			->render_fieldset()
			->right (
					new Submit('Approve',array(
							'class'=>'btn btn-success',
							'name'=>'approve',
							'value'=>1
					)),
					new Submit('Reject',array(
							'class'=>'btn',
							//'id'=>'rejection_button',
							'id'=> $row_grp ['group_id'],
							'name'=>'reject',
							'value'=>'row_rejection_text'.$i,
					))
			)
			->render ();
			echo "<hr />";
			$i=$i+1;
		}
	}
}


function rejected_project_bsform($res_grp){
	
	$BsForm = new BsForm;
	$i=1;
	while ( $row_grp = db_fetch_array ( $res_grp ))  {
		$u = user_get_object($row_grp['user_id'] );
		$mod_by=user_get_object($row_grp['mod_by'] );
		echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
			
		->head ( $row_grp ['group_name'] )
		->group ( 'Project Admin',
				new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$u->getUnixName()
				))
		)
		->group ( 'Project Members',
				new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' )
				) )
		)
	
		->group ( 'Purpose',
				new Custom ( $row_grp ['register_purpose'] )
		)
	
		->group ( 'Project Details',
				new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' )
				) )
		)
		->group ( 'Project URL',
				new Custom ( $row_grp ['unix_group_name'] ),
					
				new Hidden ( array (
						'name' => 'group_id',
						'value' => $row_grp ['group_id']
				) )
		)
		->group ( 'Register Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['register_time']))
		)
		->group ( 'Moderator:',
				new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$mod_by->getUnixName()
				))
		)
		->group ( 'Rejected Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['adddate']))
		)
		->group ( 'Rejected Reason:',
				new Custom ($row_grp ['old_value']
				)
		)
		->group ( 'Project Status:',
				new Dropdown(array('D'=>'rejected','A'=>'active'),null, array ('name' => $row_grp ['group_id']))
		)
		->render_fieldset()
			
		->right (
				new Submit('Change Status',array(
						//'class'=>'btn btn-success',
						'class'=>'hidden',
						'name'=>'change_status',
						'id'=>$row_grp ['group_id'],
						'value'=>'status_change'.$i,
				))
		)
		->render ();
		echo "<hr />";
		$i=$i+1;
	}
	
}

function pending_users_bsform($res_grp){
	$BsForm = new BsForm;
	$i=1;
	while ($row_grp = db_fetch_array($res_grp))  {
	
		echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
		->head ( $row_grp ['user_name'] )
		->group ( 'User Name:',
				new Custom ( util_make_link ( 'admin/useredit.php?user_id='. $row_grp ['user_id'] ,$row_grp['user_name']
				)),
				new Hidden ( array (
						'name' => 'user_id',
						'value' => $row_grp ['user_id']
				) )
		)
		->group ( 'Real Name',
				new Custom ( util_make_link ( '/admin/useredit.php?user_id=' . $row_grp ['user_id'], $row_grp['realname']
				) )
		)
	
		->group ( 'User Email',
				new Custom ( $row_grp ['email'] )
		)
		->group ( 'Register Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['add_date']))
		)
		/*
			->group ( 'User Phone:',
					new Custom ( util_make_link ( '/admin/useredit.php?user_id=' . $row_grp ['user_id'], $row_grp['title']
					) )
			)
		->group ( 'Project URL',
				new Custom ( $row_grp ['unix_group_name'] ),
				new Hidden ( array (
						'name' => 'group_id',
						'value' => $row_grp ['group_id']
				) )
		)*/
		->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text'.$i ),
				new Textarea ( '', array (
						'name' => 'response_text'
				) )
		)
		->render_fieldset()
		->right (
				new Submit('Approve',array(
						'class'=>'btn btn-success',
						'name'=>'approve',
						'value'=>1
				)),
				new Submit('Reject',array(
						'class'=>'btn',
						//'id'=>'rejection_button',
						'id'=>$row_grp ['user_id'],
						'name'=>'reject',
						'value'=> 'row_rejection_text'.$i,
				))
		)
		->render ();
		echo "<hr />";
		$i=$i+1;
	}
}


function reject_users_bsform($res_grp){
	
	$BsForm = new BsForm;
	$i=1;
	while ( $row_grp = db_fetch_array ( $res_grp ))  {
		//$u = user_get_object($row_grp['user_id'] );
		$mod_by=user_get_object($row_grp['mod_by'] );
		echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
			
		->head ( $row_grp ['user_name'] )
		->group ( 'User Name:',
				new Custom ( util_make_link ( 'admin/useredit.php?user_id='. $row_grp ['user_id'] ,$row_grp['user_name']
				)),
				new Hidden ( array (
						'name' => 'user_id',
						'value' => $row_grp ['user_id']
				))
		)
		->group ( 'Real Name',
				new Custom ( util_make_link ( '/admin/useredit.php?user_id=' . $row_grp ['user_id'], $row_grp['realname']
				) )
		)
	
		->group ( 'User Email',
				new Custom ( $row_grp ['email'] )
		)
		->group ( 'Register Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['add_date']))
		)
		->group ( 'Moderator:',
				new Custom ( util_make_link ( '/users/fforgeadmin',$mod_by->getUnixName()
				))
		)
		->group ( 'Rejected Time:',
				new Custom ( date('Y-m-d H:i',$row_grp['adddate']))
				//new Custom(_('blank'))
		)
		->group ( 'Rejected Reason:',
				new Custom ($row_grp ['old_value'])
				//new Custom(_('blank'))
		)
		->group ( 'User Status:',
				new Dropdown(array('D'=>'rejected','A'=>'active','S'=>'suspended'),null, array ('name' => $row_grp ['user_id']))
		)
		->render_fieldset()
			
		->right (
				new Submit('Change Status',array(
						//'class'=>'btn btn-success',
						'class'=>'hidden',
						'name'=>'change_status',
						'id'=>$row_grp ['user_id'],
						'value'=>'status_change'.$i,
				))
		)
		->render ();
		echo "<hr />";
		$i=$i+1;
	}
}


?>
