<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
require_once $gfwww.'include/role_utils.php';

site_admin_header(array('title'=>_('Site Admin')));

include('../include/model.php');
$ModelClass=new model(1);

//Clear model table
db_query("TRUNCATE table models");

//Clear model projects
$res=db_query_params('SELECT * FROM groups WHERE group_type=$1',array(2));
while($GroupRow=db_fetch_array($res)){
	$Group=new Group($GroupRow['group_id']);
	$Group->delete(1,1,1);
}

function insert_model($GUID,$ModelDef){
	db_query_params("INSERT INTO models(dome_guid,model_def) VALUES($1,$2)",array($GUID,$ModelDef));
	//echo "Inserting ".$ModelDef;
	return db_insertid('','models','model_id');
}

function unix_name($Name){
	$Name=str_replace(' ','',$Name);
	$Name=str_replace('/','',$Name);
	$Name=str_replace('=','',$Name);
	$Name=strtolower($Name);
	$Name=substr($Name,0,15);
	return $Name;
}

$AllModels=$ModelClass->getall_fromdome();
//print_r($AllModels);

foreach($AllModels as $i){
	$ModelDef=str_replace('\/','/',json_encode($i));
	$ModelID=insert_model($i->guid,$ModelDef);

	$scm="scmsvn";
	$scm_host = '';
	if (forge_get_config('use_scm')) {
		$plugin = false ;
		if (forge_get_config('use_scm') && $scm && $scm != 'noscm') {
			$plugin = plugin_get_object($scm);
			if ($plugin) {
				$scm_host = $plugin->getDefaultServer();
			}
		}
	}

	if ($scm_host == '') {
		$scm_host = forge_get_config('web_host');
	}

	$send_mail = ! forge_get_config ('project_auto_approval') ;

	$u = session_get_user();
	$Group=new Group();
	$UnixName=unix_name($i->name);
	echo $UnixName;
	$res=$Group->create($u,$i->name,$UnixName,$i->desc,$i->desc,'shell1',$scm_host,false,$send_mail,5,2,$ModelID);

	if ($res && forge_get_config('use_scm') && $plugin) {
		$Group->setUsesSCM (true) ;
		$res = $Group->setPluginUse ($scm, true);
	} else {
		$Group->setUsesSCM (false) ;
	}

	if (!$res) {
		echo $Group->getErrorMessage();
	} else {
		if ( ! forge_get_config ('project_auto_approval') ) {
			printf(_('<p>Your project has been submitted to the %1$s administrators. Within 72 hours, you will receive notification of their decision and further instructions.</p><p>Thank you for choosing %1$s</p>'), forge_get_config ('forge_name'));
		} else if ($Group->isError()) {
			printf(_('<div class="error">ERROR: %1$s</div>'), $Group->getErrorMessage() );
		} else {
			printf(_('Approving Project: %1$s'), $Group->getUnixName()).'<br />';

			if (!$Group->approve( user_get_object_by_name ( forge_get_config ('project_auto_approval_user') ) ) ) {
				printf(_('<div class="error">Approval ERROR: %1$s</div>'), $Group->getErrorMessage() );
			} else {
				printf(_('<p>Your project has been automatically approved.  You should receive an email containing further information shortly.</p><p>Thank you for choosing %1$s</p>'), forge_get_config ('forge_name'));
			}
		}
	}
}

/*
foreach($AllModels as $i){
	echo '<br /><br />';
	//print_r($i);
	$ModelCheck=db_query_params("SELECT * FROM models WHERE dome_guid=$1",array($i->guid));
	if (db_numrows($ModelCheck)){
		//GUID exists in DB, compare model defs
		$ModelDef=str_replace('\/','/',json_encode($i));
		while($Model=db_fetch_array($ModelCheck)){
			if ($Model['model_def']!=$ModelDef){
				//model_def is not in DB, add to db
				insert_model($i->guid,$ModelDef);
			}
		}
	}else{
		//Insert new GUID
		$ModelDef=str_replace('\/','/',json_encode($i));
		insert_model($i->guid,$ModelDef);
	}
}*/
?>