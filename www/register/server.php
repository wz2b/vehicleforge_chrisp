<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:23 PM 10/6/11
 */

require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';

if (!session_loggedin())
	exit_not_logged_in();

if (isset($_POST['server_url'])){
	//require_once $gfcommon.'include/Server.class.php';
	$URL=getStringFromRequest('server_url');
    $Name=getStringFromRequest('server_name');
	if (Server::checkOK($URL)){
		if (Server::create($URL,session_get_user()->getID(),$Name)){
			$feedback=_('Your server has been registered');
		}else{
			$error_msg='Cannot register server';
		}
	}else{
		$error_msg='Server is not valid';
	}
}

site_user_header(array('title'=>'Register Server'));
$Layout->col(12, true);

$Form=new BsForm;
echo $Form->init('server.php','post',array('class'=>'form-horizontal'))
    ->head('Register your Server')
    ->group('Server Alias',
        new Text(array(
            'name'=>'server_name'
        ))
    )
    ->group('Server URL',
        new Text(array(
            'name'=>'server_url'
        ))
    )
    ->actions(
        new Submit(),
        new Reset()
    )
    ->render();

$Layout->endcol();

site_project_footer(array());
?>