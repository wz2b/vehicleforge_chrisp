<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/DOMEApi.class.php';
require_once $gfcommon.'include/Server.class.php';
$Server=new Server(getIntFromRequest('server_id'));
$URL=$Server->getURL();

$DOMEApi=new DOMEApi($URL, false);

$Type=isset($_POST['type'])?$_POST['type']:null;
$Name=isset($_POST['name'])?$_POST['name']:null;
$Path=isset($_POST['path'])?$_POST['path']:null;
//$FolderID=isset($_POST['folderId'])?(int)$_POST['folderId']:null;

$Return=array('error'=>false);
if ($Folders=$DOMEApi->getChildren($Type, $Name, $Path)){
	$Return['data']=$Folders;
}else{
	$Return['error']=true;
	$Return['msg']=$DOMEApi->getErrorMessage();
}

echo json_encode($Return);
?>