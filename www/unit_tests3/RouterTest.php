<?php
require_once 'db_inc.inc';
require_once 'dome_config.php';
use Assert as is;

class RouterTest extends PHPUnit_Framework_TestCase {

    protected function setUp(){

    }

    protected function tearDown(){

    }

    public function testRoute(){
        //Route known URLs
        is::eq(Router::route('/components/index.php?group_id=vehicleforge&cid=456'), '/projects/vehicleforge/component/456');
        is::eq(Router::route('/components/index.php?group_id=vehicleforge'), '/projects/vehicleforge/components');
        is::eq(Router::route('/components/?group_id=vehicleforge&cid=456'), '/projects/vehicleforge/component/456');
        is::eq(Router::route('/components/?group_id=vehicleforge'), '/projects/vehicleforge/components');

        //Route unknown URL
        is::eq(Router::route('/my/diary.php'), '/my/diary.php');
    }
}
?>