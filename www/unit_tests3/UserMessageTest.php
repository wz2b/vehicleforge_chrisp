<?php
require_once 'db_inc.inc';
require_once APP_PATH.'common/include/GFUser.class.php';

use Assert as is;

class UserMessageTest extends PHPUnit_Framework_TestCase{
    protected $object, $iobject;

    protected function setUp(){
        db_begin();
        $this->object=new UserMessage;
        $X=UserMessage::compose(105,105,"test","hello!");
        $this->iobject=new UserMessage($X);
    }

    protected function tearDown(){
        db_rollback();
    }

    public function testFetchData(){
        $this->markTestSkipped();
    }

    public function testGetID(){
        is::int($this->iobject->getID());
    }

    public function testGetReceivedMessages(){
        $Messages=UserMessage::getReceivedMessages(105);
        is::arr($Messages);

        $Count=sizeof($Messages);
        UserMessage::compose(105,105,"hi again!","test");
        $Messages=UserMessage::getReceivedMessages(105);
        is::arr($Messages);

        $Count2=sizeof($Messages);

        is::eq($Count+1,$Count2);
    }

    public function testGetUnreadReplyCount(){
        $X=$this->object->getUnreadReplyCount(105);
        is::int($X);
    }

    public function testGetUnreadReceivedMessageCount(){
        $X=UserMessage::getUnreadReceivedMessageCount(105);
        is::int($X);
    }

    public function testCompose(){
        $X=UserMessage::compose(105,105,"test","hello!");
        is::int($X);
        is::int(UserMessage::compose(105,105,"test","hello!",$X));
    }

    public function testReply(){
        $X=$this->object->reply("this is a reply",105);
        is::int($X);
    }

    public function testGetMessage(){
        $X=$this->iobject->getMessage();
        is::string($X);
    }

    public function testGetFromID(){
        $X=$this->iobject->getFromID();
        is::int($X);
    }

    public function testGetTime(){
        $X=$this->iobject->getTime();
        is::int($X);
    }

    public function testGetSubject(){
        $X=$this->iobject->getSubject();
        is::string($X);
    }

    public function testGetToID(){
        $X=$this->iobject->getToID();
        is::int($X);
    }

    public function testGetRead(){
        $X=$this->iobject->getRead();
        is::bool($X);
    }

    public function testMarkRead(){
        $X=$this->iobject->markRead(105);
        is::true($X);
    }

    public function testGetReplies(){
        $X=$this->iobject->getReplies();
        is::arr($X);
    }

    public function testGetParentMessageID(){
        $X=$this->iobject->getParentMessageID();
        is::int($X);
    }

    public function testGetParentMessage(){
        $X=$this->iobject->getParentMessage();
        is::isInstanceOf($X,'UserMessage');
    }

    public function testGetLastMessageSent(){
        $X=$this->iobject->getLastMessageSent();
        is::int($X);
    }

    public function testFormatReply(){
        $X=$this->iobject->formatReply(105);
        is::string($X);
    }
}
?>
