<?php
require_once 'db_inc.inc';
require_once APP_PATH.'common/include/utils.php';
//require_once 'TextMessage.class.php';
use Assert as is;

class TextMessageTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TextMessage
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new TextMessage;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @covers TextMessage::getGateways
     * @todo   Implement testGetGateways().
     */
    public function testGetGateways()
    {
        is::arr($this->object->getGateways());
    }

    /**
     * @covers TextMessage::getGatewayByID
     * @todo   Implement testGetGatewayByID().
     */
    public function testGetGatewayByID()
    {
        is::arr($this->object->getGatewayByID(1));
    }

    /**
     * @covers TextMessage::send
     * @todo   Implement testSend().
     */
    public function testSend()
    {
        // Remove the following lines when you implement this test.
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
    }
}
