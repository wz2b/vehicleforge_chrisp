<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$HTML->addStylesheet('marketplace.css');
site_header(array('title'=>'Service Marketplace'));
?>
   
<ul id="tab" class="nav nav-tabs" style="margin:0;">
  <li>
  	<a href="components.php">
  		<strong>Components</strong>
  	</a>
  </li>
  
	<li class="active">
		<a href="services.php">
			<strong>Services</strong>
		</a>
	</li>
</ul>   
             
<?php 
	$Layout->col(12,true,true,true);
  echo "<h2 style=color:#1589FF;>Service Marketplace</h2>"; 
  echo "<h4>Find services to add dynamic simulations, validations, and analysis to your project.</h4>";        
  $Layout->endcol();      
?>

<br/><div class="row">
	<div class="span3">
		<div class="well">
			<form id="search">
				<input type="text" name="q" style="width:80%; margin: 0 auto;" class="search-query" placeholder="Find Services"/>
       </form>
		</div>
	</div>    
	<div class="span8">   
		<div id="resultSummary"></div>   
		<ul class="thumbnails" id="resultList"></ul>
	</div>
</div>  


<!--BEGIN: Services Modal-->
<div class="modal hide fade" id="modal">
	
	<div class="modal-header">
	  <a class="close" data-dismiss="modal">×</a>
	  <h3 id="service_name"></h3>
          <h3 style="font-weight: 500;"><span style="font-weight: 300">in</span> <a id="group_name" href="#"></a></h3>
	</div>
	
	<div class="modal-body">
		<div class="row">
			<div style="margin:10px;">
                           <img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">
                           <p id="service_desc">
                        </div>
		        <div class="row">
				  <div class="span3">
				  	<p style="border-bottom: 1px solid #666"><strong>Inputs</strong></p>
				  	<ul id="service_inputs"></ul>
				  </div>
				  <div class="span3">
				  	<p style="border-bottom: 1px solid #666"><strong>Outputs</strong></p>
				  	<ul id="service_outputs"></ul>
			  	</div>
			</div>
	        </div>
        </div>
  
	<div class="modal-footer">
		<?php if (session_loggedin()) {?>
			<a id="subscribe_to_service" href="#" class="btn btn-primary">Subscribe to Service</a>
	    <a id="service_url" href="#" class="btn">View Service</a>
    <?php } else { ?>
    	<p>You must <a href="/account/login">login</a> to use this service.</p>
    <?php } ?>

         </div>
  
</div>
<!--END: Services Modal-->

<!--BEGIN Service Search script-->
<script>
	$(document).ready( function() {
		//base_title = document.title;
		searchServices();
	});

	$('#search').submit( function() {
		var searchTerm = $('#search > input[name="q"]').val();
		document.title = document.title + ' - search "' + searchTerm + '"'		
		//window.history.pushState(null, '', window.location.pathname + "?q=" + searchTerm);
		searchServices();
		//return false;
	});
	
	/*
	 * function searchServices()
	 * parses GET parameters and queries Solr for search results
	 */
	function searchServices() {
		query = getParameterByName("q");

/*
		// update page title
		if (query != "") {
			document.title = document.title + ' - search "' + query + '"';
		}
*/
		// execute ajax request
		$.ajax({
		  type: 'GET',
		  url: "/solrSearch/proxy.php/select",
		  data: {
		    q: "*" + query + "*",
		    defType: 'edismax',
		    qf: 'interface_name',
		    core: 'services',
		    start: 0,
		    rows: 50
		  },
		  dataType: "json",
		  success: function(json) {
		  	// remove old results
		  	$('#resultList').children().remove();

		  	// add new results
		  	var response = json.response;
				for (var i=0; i<response.numFound; i++) {
					var doc = response.docs[i];
					makeServiceThumb(doc);
				}
				
				if (query=='')
					$('#resultSummary').html(response.numFound + ' results found.');
				else
					$('#resultSummary').html(response.numFound + ' results found for search: "' + query + '".');
		  }   
		});
	}
	
	// used to parse GET parameters
	function getParameterByName(name)
	{
	  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	  var regexS = "[\\?&]" + name + "=([^&#]*)";
	  var regex = new RegExp(regexS);
	  var results = regex.exec(window.location.search);
	  if(results == null)
	    return "";
	  else
	    return decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// builds a thumbnail view of a service
	function makeServiceThumb(doc) {
	
		var html = $('<?= makeInterfaceThumb() ?>');
				
		// set attributes
		$(html).attr('interface_name', doc.interface_name);
		$(html).attr('interface_id', doc.id);
		$(html).attr('cem_id', doc.cem_id);
		$(html).data('group_name',doc.group_name);
		$(html).data('interface_data', $.parseJSON(doc.interface_data));

		// set html
		$(html).find('.interface_name').html(doc.interface_name);
		$(html).find('.group_name').html('<span style="font-weight: 200;">in </span>' + doc.group_name);
		// append results
	  $('#resultList').append(html);
	  
	  // add a listener
	  $(html).on('click', showModal);
	}	

	/*
	 * function: showModal
	 * parses information from the selected service and displays a detailed view in modal form
	 */ 
	function showModal(event) {

		var name = $(this).attr('interface_name');
		var id = $(this).attr('interface_id');
		var group_id = -1;
      		var group_name = $(this).data('group_name');
      
		jQuery.ajax({
		  url: 'service_group_id_decoder.php?interface_id=' + id, 
		      success: function(data) {
		          group_id = data.group_id;
		        },
		      dataType: 'json',
		      async: false
		      });
		var interface_data = $(this).data('interface_data');

		// update modal values
		$('#service_name').html(name);
		$('#service_desc').html('Description of <b>'+name+'</b> service goes here');
		$('#service_url').attr('href', '/services/?group_id=' + group_id + '&diid=' + id);
		$('#group_name').html(group_name);
                $('#group_name').attr('href', '/components/?group_id=' + group_id);

		$("#subscribe_to_service").data('interface_id',id);
		
		// write inputs
		var inputs = interface_data.inParams;
		$('#service_inputs').children().remove();
		for (i=0; i<inputs.length; i++) {
			var name = inputs[i].name;
			var type = inputs[i].type;
			$('#service_inputs').append('<li>' + name + ' [' + type + ']</li');
		}

		// write outputs
		var outputs = interface_data.outParams;
		$('#service_outputs').children().remove();
		for (i=0; i<outputs.length; i++) {
			var name = outputs[i].name;
			var type = outputs[i].type;
			$('#service_outputs').append('<li>' + name + ' [' + type + ']</li');
		}
		
		// show modal
		$('#modal').modal();
	}
</script>
<!--END Service Search script-->


<?php 
	function makeInterfaceThumb() {
           return '<li class="span2"><div class="thumbnail"><img src="http://placehold.it/160x120&text=image" alt=""><div class="caption" style="padding:5px;"><h4 class="interface_name" style="text-align: center;"></h4><h5 class="group_name" style="font-weight:500; text-align: center;"></h5></div></div></li>';
	}
	
	site_footer(array());
?>


