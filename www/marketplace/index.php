<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$HTML->addStylesheet('marketplace.css');
site_header(array('title'=>'Component Marketplace'));
?>

<ul id="tab" class="nav nav-tabs" style="margin:0;">
    <li class="active">
        <a href="index.php">
            <strong>Components</strong>
        </a>
    </li>

    <li>
        <a href="services.php">
            <strong>Services</strong>
        </a>
    </li>
</ul>

<?php
$Layout->col(12,true,true,true);
echo "<h2 style=color:#1589FF;>Component Marketplace</h2>";
echo "<h4>Find components to add to your project.</h4>";
$Layout->endcol();

$Layout->col(3);
?>
<div class="well">
    <form id="search" method="get">
        <input type="text" name="q" style="width:80%; margin: 0 auto;" class="search-query" placeholder="Find Components"/>
    </form>
</div>

<?php $Layout->endcol()->col(9); ?>
<div id="resultSummary"></div>
<br/><ul class="thumbnails" id="resultList"></ul>
<?php $Layout->endcol(true); ?>

<div class="modal hide fade" id="modal">

    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h2 id="component_name"></h2>
        <h3 style="font-weight: 500;"><span style="font-weight: 300">in</span> <a id="group_name" href="#"></a></h3>
    </div>

    <div class="modal-body">
        <div class="row">
            <div style="margin:10px;">
                <img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">
                <p id="component_desc"></p>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <?php if (session_loggedin()) { ?>
        <a id="component_url" href="#" class="btn">View Component</a>

        <div class="btn-group pull-right" style="margin-left: 10px;">
            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" style="width:120px;">
                <span id="add_to_project_status">Add to Project</span>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <?php
                foreach (session_get_user()->getOwnedProjects() as $p) {
                    echo '<li><a href="#" class="add_to_project" group_id="'.$p->getID().'">'.$p->getPublicName().'</a></li>';
                }
                ?>
            </ul>
        </div>
        <?php } else { ?>
        <p>You must <a href="/account/login">login</a> to use this component.</p>
        <?php } ?>
    </div>
</div>
<!--END: Component Modal-->

<!--BEGIN script-->
<script>
    $(document).ready( function() {

        // set up listener to add component to project
    <?php if (session_loggedin()) { ?>

        $('.add_to_project').bind('click', function() {
            $(this).closest('.dropdown-toggle').attr('disabled', 'disabled');
            $('#add_to_project_status').html('Adding...');

            $.post('ajax/import_component_to_project.php',
                {
                    cid: $('#modal').data('cid'),
                    to_gid: $(this).attr('group_id')
                }
            );

            $('#add_to_project_status').html('Added!');
            window.setTimeout( function() {
                $(this).closest('.dropdown-toggle').removeAttr('disabled');
                $('#add_to_project_status').html('Add to Project');
            }, 1000);
        });
        <?php } ?>

    });


    // helper method to generate a component thumbnail view
    function makeComponentThumb(doc) {
        var html = $('<?= makeComponentThumb() ?>');

        // update the generic thumbnail
        $(html).data('component_name', doc.component_name);
        $(html).data('component_id', doc.id);
        $(html).data('group_id', doc.group_id);
        $(html).data('group_name', doc.group_name);

        $(html).find('.component_name').html(doc.component_name);
        $(html).find('.group_name').html('<span style="font-weight: 200;">in </span>' + doc.group_name);

        // append component thumbnail to results lists
        $('#resultList').append(html);

        // add an action listener
        $(html).on('click', showModal);
    }

    // helper method to display a detailed view of a component
    function showModal(event) {
        // update modal
        var name = $(this).data('component_name');
        var id = $(this).data('component_id');
        var group_id = $(this).data('group_id');
        var group_name = $(this).data('group_name');

        $('#component_name').html(name);
        $('#component_desc').html('Lorem ipsum...');
        $('#component_url').attr('href', '/components/?group_id=' + group_id + '&cid=' + id);
        $('#group_name').html(group_name);
        $('#group_name').attr('href', '/components/?group_id=' + group_id);
        $("#modal").data('cid',id);

        // show modal
        $('#modal').modal();
    }

</script>
<!--END script-->

<?php
function makeComponentThumb() {
    return '<li class="span2"><div class="thumbnail"><img src="http://placehold.it/160x120&text=image" alt=""><div class="caption" style="padding:5px;"><h4 class="component_name" style="text-align: center;"></h4><h5 class="group_name" style="font-weight:500; text-align: center;"></h5></div></div></li>';
}

site_footer(array());
?>
