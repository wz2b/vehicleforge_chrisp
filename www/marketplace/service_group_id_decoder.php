<?php

require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$interface_id = getStringFromRequest('interface_id');

$res = db_query_params("SELECT cem.group_id group_id FROM dome_interfaces iface, cem_objects cem WHERE cem.cem_id=iface.cem_id AND iface.interface_id=$1", array($interface_id));
$group_id = "not_found";

if(db_numrows($res)) {
  $row = db_fetch_array($res);
  $group_id = $row['group_id'];
}

print "{ \"group_id\":\"".$group_id."\" }";

?>