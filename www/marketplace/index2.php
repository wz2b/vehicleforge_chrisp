<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once($gfwww . 'marketplace/model_utils.php');
require_once $gfwww.'include/model.php';
require_once $gfcommon.'include/ModelGroup.class.php';

// Include javascript for rating mechanism
use_javascript('raty/jquery.raty.min.js');
use_javascript('raty.js');

// Include css/less for marketplace //TODO: include this in main CSS
$HTML->addStylesheet('/themes/gforge/css/marketplace.css');
  

// Get the list of models to display
if (isset($_REQUEST['group_id'])){
	//Get an array of all model IDs associated with a project

}else{
	//Get array of all model projects
	$ModelClass=new model;
	$ModelGroupList = $ModelClass->getall();
}

// Create the site header
if (isset($GroupID)){
	site_project_header(array('title'=>'Marketplace','group'=>$GroupID));
}else{
	site_header(array('title'=>'Marketplace'));
	$Layout->col(12,true);
	$HTML->heading("<h2 style=color:#1589FF;>Marketplace</h2>", 2);
	$HTML->heading("<h4>Find models and components from others and upload your own</h4>");
	$Layout->endcol();
}

$Layout->col(12,true);

?>

&nbsp
&nbsp
&nbsp
&nbsp
&nbsp
&nbsp


<!-- Creating a Tabbed Section for Searching, Browsing, Adding  -->

<ul id="tab" class="nav nav-tabs">
            <li class="active"><a href="#browse" data-toggle="tab"><strong>Browserrr</strong></a></li>
            <li><a href="#search" data-toggle="tab"><strong>Search</strong></a></li>
            <li><a href="#add" data-toggle="tab"><strong>Add</strong></a></li>
              </ul>
            </li>
          </ul>
          
      
<div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="browse">
             
             
              <?php // Create Category List
                           




if (!isset($GroupID)) {
	$HTML->heading("Popular categories", 3);
	echo '<ul style="list-style:none">';
	makeCategoryView("Powertrain", "Simulations of engines, transmissions, radiators, drive axles, and other powertrain components.", "/mock_images/engine-drive-line.jpg", ".?group_id=24");
	makeCategoryView("Suspension", "Simulations that aid in the determination of vehicle ride dynamics and suspension selection.", "/mock_images/generic-suspension.jpg", ".?group_id=24");
	makeCategoryView("Electrical", "Simulations of vehicle electrical systems.", "/mock_images/electrical-system.jpg", ".?group_id=24");
	
	echo '</ul>';
}
?>



<?


echo "<ul class=\"modelList\">\n";
foreach ($ModelGroupList as $ModelGroupID) {
	makeModelView($ModelGroupID);
}
echo "\t\t</ul>\n";
?>
              
              
            </div>
            <div class="tab-pane fade" id="search">
            
            
        <h3>Search
        </h3>
        

<form class="span4" action="'.util_make_uri('/search/').'" method="get">
	<div class="float-left">
		
		<input type="text"  style="width:200px;" data-provide="typeahead" data-items="4" data-source="[&quot;Electrical&quot;,&quot;Hydraulic&quot;,&quot;Mechanical&quot;,&quot;Thermal&quot;,&quot;Electromagnetic&quot;,&quot;Aero&quot;,&quot;Software&quot;]">
		
		<input type="submit" value="Search"  />		
	</div>
	</form>


	<div class="span2">
	<label class="checkbox">
                <input type="checkbox" id="optionsCheckboxList1" value="option1"> Rapid Prototyping
              </label>
    
    <label class="checkbox">
                <input type="checkbox" name="optionsCheckboxList1" value="option1">
                Data Acquisition
              </label>
    <label class="checkbox">
                <input type="checkbox" name="optionsCheckboxList1" value="option1">
                Signal Processing
              </label>
    
    <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox2" value="option2"> Statistics and Probability
              
              </div>
              
    
    <div class="span2">
    
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> Control Systems
              </label>
              
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> Test and Measurement
              </label>
    
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> CAD Geometry
              </label>
              
     <label class="checkbox">
               <input type="checkbox" id="optionsCheckbox3" value="option3"> Nonlinear Differential Equation models
              </label>
    

    </div>
    
    
     <div class="span2">
    
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> CFD              </label>
              
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> FEA
              </label>
    
     <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox3" value="option3"> iFAB Models
              </label>
              
     <label class="checkbox">
               <input type="checkbox" id="optionsCheckbox3" value="option3"> Sketches
              </label>
    

    </div>
              
              
            
              
             
     
	
	

			
	</div>
	<!--/form-->
	
	
              
           
      
      
    
   
            
            
            <div class="tab-pane fade" id="add">
              <p>Future work: add a component or model to the marketplace here.</p>
            </div>
            </div>




<?php



$Layout->endcol();

// Create site footer
site_footer(array());

/*
 * Functions used in /marketplace/index.php
 */
function makeModelView($ModelGroupID) {
	if (forge_check_perm('project_read',$ModelGroupID)){
		$ModelGroup=new ModelGroup($ModelGroupID);

		//Get the model ID linked to the project
		$model=$ModelGroup->getModel();

		//Get the model definition
		$model=getModel($model);

		echo '<div class="span4"><li class="box modelThumb">';

		// Start: first row
		echo '<div style="width:100%; height:110px;">';

		// Model image
		echo '<a class="noStyle" href="/projects/'.$ModelGroup->getUnixName().'"><img class="modelImage" src="http://placehold.it/100&text=img"/></a>';

		// Model name
		echo '<div class="modelText">';
			echo '<h3 class="modelTitle"><a class="noStyle" href="/projects/'.$ModelGroup->getUnixName().'">'. $model['name'] . '</a></h3>';


			// Model description
			echo '<div class="modelDesc">' . $model['desc'] . '</div>';

			// Model author;
			echo '<div class="modelDesc">by: ' . $model['author'] . '</div>';
		echo '</div>';

		// End: first row
		// Begin: second row
		echo '</div>';
		echo '<div style="width:100%;">';

		// Model Rating, Runs, Reviews
		echo '<div class="raty" style="float:left;" data-value="3.5"></div>';
		echo '<div class="modelText">';
			echo '<div class="stat" style="float:right">';
				echo '<div class="number">' . rand(0,30) . '</div>';
				echo '<div class="quantity">runs</div>';
			echo '</div>'; // end of stat

			echo '<div class="stat" style="float:right">';
				echo '<div class="number">' . rand(0,30) . '</div>';
				echo '<div class="quantity">users</div>';
			echo '</div>'; // end of stats

			echo '<div class="stat" style="float:right">';
				echo '<div class="number">0</div>';
				echo '<div class="quantity">projects</div>';
			echo '</div>'; // end of stat
		echo '</div>'; // end of modelText
		echo '</div>'; // end of second row;

		echo "</li></div>";
	}
} 

function makeCategoryView($cat, $desc, $image, $url) {
	global $HTML;
	echo '<div class="span4"><li class="box category" style="background-image: url(' . $image . ')">';
	echo '<a href="' . $url .'" style="text-decoration:none; color:inherit;"><div class="title">'.$cat.'</div></a>';		
	echo '<a href="' . $url .'" style="text-decoration:none; color:inherit;"><div class="desc">'.$desc.'</div></a>';		
	echo "</li></div>";
} 

?>