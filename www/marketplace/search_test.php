<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

site_header(array('title'=>'Marketplace'));
function makeComponentThumb() {
	return '<li class="span2"><div class="thumbnail" compID=component_id groupID=group_id><img src="http://placehold.it/160x120" alt=""><div class="caption" style="padding:5px;"><h5>component_name</h5></div></div></li>';
}

?>

<style>
	.thumbnail {
		height:140px;
	}
	
	.thumbnail > .caption {
		overflow-x: hidden;
	}
	
	.thumbnail > .caption * {
		text-overflow: ellipsis;
	}
	
	.thumbnail:hover {
		border: 1px solid #08C;
	}
</style>

<form id="search">
	<input type="text" name="q" class="search-query span2" placeholder="Search for components.">
</form>

<ul id="results" class="thumbnails"></ul>

<script>
	$(document).ready( function() {
		searchComponents('*');
	});

	$('#search').submit( function() {
		searchComponents( $('#search > input[name="q"]').val());
		return false;
	});
	
	function searchComponents(query) {
		$.ajax({
		  type: 'GET',
		  url: "/solrSearch/proxy.php/select",
		  data: {
		    q: query,
		    defType: 'edismax',
		    qf: 'component_name',
		    start: 0,
		    rows: 50
		  },
		  dataType: "json",
		  success: function(json) {
		  	// remove old results
		  	$('#results').children().remove();

		  	// add new results
		  	var response = json.response;
				for (var i=0; i<response.numFound; i++) {
					var doc = response.docs[i];
					makeComponentThumb(doc);
				}
		  }   
		});
	}
	
	function makeComponentThumb(doc) {
	
		var html = '<?= makeComponentThumb() ?>';
		html = html.replace("component_name", doc.component_name);
		html = html.replace("group_id", doc.group_id);
		html = html.replace("component_id", doc.id);
	  
	  $('#results').append(html);
	}
</script>

<?php
site_footer();
?>