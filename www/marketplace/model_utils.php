<?php 
	/*
	 * model_utils.php – Helper functions for dealing with VehicleForge models
	 *
	 */
	 	 
	 require_once($gfwww . 'include/model.php');
	 
 	 /*
	  * getModel – Get information (in a flat associative array) about a given model
	  * @param int model_id
	  * @returns associative array of model properties
	  */
	  
	 function getModel($mid) {
	 	$ModelClass = new model();
	 	$model = $ModelClass->get($mid);
	 	$modelDef = json_decode($model->model_def);
	 	
	 	$modelInfo = array();
	 	$modelInfo['id'] = $mid;
	 	$modelInfo['name'] = $modelDef->name;
	 	$modelInfo['desc'] = $modelDef->desc;
	 	$modelInfo['dateModified'] = $modelDef->dateModified/1000;
	 	$modelInfo['author'] = '<a href="/users/jmekler">Jeff Mekler</a>';

	 	return $modelInfo;	 	
	 }
	 
	 /*
	  * getModelsByGroup – Get the models associated with a particular group
	  * @param int group_id
	  * @returns array of model_id's
	  */
	 function getModelsByGroup($group_id) {

		// execute the sql query
	 	$res = db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1",array($group_id));
		
		// parse results
		$models = array();
	 	if (db_numrows($res)){		
			while ($row=db_fetch_array($res)){
				$models[] = $row['model_id'];
			}
		}
		
		// return results
		return $models;

	 }


?>