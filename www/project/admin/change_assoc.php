<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:05 PM 9/1/11
 */

require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';

$ModelID=$_POST['mid'];
$GroupID=$_POST['id'];

$Check=db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1 AND model_id=$2",array($GroupID,$ModelID));

if (db_numrows($Check)){
	//Update AKA remove
	db_query_params("DELETE FROM model_project_associations WHERE group_id=$1 AND model_id=$2",array($GroupID,$ModelID));
}else{
	//Add
	db_query_params("INSERT INTO model_project_associations(group_id,model_id) VALUES($1,$2)",array($GroupID,$ModelID));
}
?>