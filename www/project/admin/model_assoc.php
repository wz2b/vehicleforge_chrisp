<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 1:08 PM 9/1/11
 */
require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'project/admin/project_admin_utils.php';
require_once('../../include/model.php');
require_once('../../include/TableGen.php');
require_once $gfwww . 'include/LayoutGen.php';
$Model=new model;
$Table=new TableGen;
$LayoutGen=new LayoutGen;

$JS="
$(function(){
	$('.assoc').change(function(){
		var mid=$(this).data('mid');
		$.post('change_assoc.php',{'id':".$group_id.",'mid':mid},function(){

		});
	});
});
";
add_js($JS);

$group_id = getStringFromRequest('group_id');
session_require_perm ('project_admin', $group_id);

// get current information
$group = group_get_object($group_id);
if (!$group || !is_object($group)) {
    exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'admin');
}

$group->clearError();
project_admin_header(array('title'=>'Model Associations', 'group'=>$group->getID()));
$LayoutGen->col(2);
$HTML->boxTop();
echo '<form id="searchBox" action="model_assoc.php?group_id='.$group_id.'" method="post">
<input type="text" name="words" /><input type="submit" value="Search Models" /></form>';
$HTML->boxBottom();

$LayoutGen->endcol()->col(10);
$HTML->boxTop();
if (isset($_POST['words'])){
	$Words=getStringFromRequest('words');
	/*$Models=$Model->search($Words);*/
}else{
	$Models=$Model->getall();
	//$Models=null;
}

$Table->top(array(array('Model Name',50),array('Date Shared',25),array('Associated',25)),'%');
if (isset($_POST['words'])){
	/*foreach ($Models as $i){
		$Association=db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1 AND guid=$2",array($group_id,$i->guid));
		$Association='<input type="checkbox" data-guid="'.$i->guid.'" class="assoc" '.(db_numrows($Association)?' checked="checked"':'').' />';
		echo $Table->col(util_make_link('models/model.php?group_id='.$group_id.'&guid='.$i->guid,$i->name)).$Table->col(date('M j, Y',$i->dateShared/1000)).$Table->col($Association,true);
	}*/
}elseif($Models){
	//print_r($Models);
	foreach($Models as $i){
		//$Association=db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1 AND guid=$2",array($group_id,$i->guid));
		//$Association='<input type="checkbox" data-guid="'.$ModelRow['dome_guid'].'" class="assoc" '.(db_numrows($Association)?' checked="checked"':'').' />';
		//$ModelDef=str_replace('\/','/',str_replace('"',"'",json_encode($i)));
		$ModelDef=json_decode($i->model_def);
		$Association=db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1 AND model_id=$2",array($group_id,$i->model_id));
		$Association='<input type="checkbox" data-mid="'.$i->model_id.'" class="assoc" '.(db_numrows($Association)?' checked="checked"':'').' />';
		$Table->col(util_make_link('models/model.php?group_id='.$group_id.'&mid='.$i->model_id,$ModelDef->name))->col(date('M j, Y',$ModelDef->dateModified/1000))->col($Association);
	}
}
$Table->bottom();
$HTML->boxBottom();
$LayoutGen->endcol();

site_project_footer(array());
?>