<?php

require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';

$GroupID=getIntFromRequest('group_id');
$Folder=getStringFromRequest('name');
$DocGroup=new DocumentGroup(null,true);

$Return=array('error'=>false);

$Success=$DocGroup->create($Folder,$GroupID,'',user_getid());
if (!$Success){
	$Return['error']=true;
	$Return['msg']=$DocGroup->getErrorMessage();
}else{
	$Return['fid']=$Success->getID();
	$Return['fname']=$Success->getName();
}

echo json_encode($Return);
?>