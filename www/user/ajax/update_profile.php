<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$User=user_get_object(getIntFromRequest('uid'));

$Return=array('error'=>false);
if (session_get_user()->getID()==$User->getID()){
    $User->updateAboutMe(getStringFromRequest('about'));
}else{
    $Return['error']=true;
    $Return['msg']='You cannot edit someone else\'s profile';
}

echo json_encode($Return);
?>