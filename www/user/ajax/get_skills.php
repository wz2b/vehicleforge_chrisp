<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$User=user_get_object(getIntFromRequest('uid'));

$Return=array(
    'visible'=>$User->isSkillsPublic(),
    'skills'=>$User->getSkills()
);
echo json_encode($Return);
?>