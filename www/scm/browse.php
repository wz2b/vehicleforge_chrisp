<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:07 PM 10/28/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'scm/include/scm_utils.php';
require_once $gfplugins.'scmsvn/common/SVNPlugin.class.php';
$SVNPlugin=new SVNPlugin;

$GroupID=getStringFromGet("group_id");
$Group=group_get_object($GroupID);
if (!$Group || !is_object($Group)) {
	exit_no_group();
}

$JS='$(function(){
	var hiddenWidth=$("#main_content").outerWidth(),
	repo="'.$Group->getUnixName().'";
	$("#file_read").css({"left":hiddenWidth+20,"width":hiddenWidth-5});

	$(document).on("click", ".folder a", function(){
		var folder=$(this).parent().attr("id")+"/",
		side_folder=folder.substr(0,folder.length-1),
		$me=$(this);

		if (!$me.hasClass("open")){
			$.post("/ajax/scm_browse.php",{"repo":repo,"folder":folder},function(msg){
				var html="", side_html="";
				if (msg.length){
					for (var i in msg){
						html+="<div id=\"";
						side_html+="<div id=\"";
						if (msg[i].name.indexOf("/")!=-1){
							var sub=msg[i].name.substr(0,msg[i].name.length-1),
							author=(msg[i].author!=null)?" by "+msg[i].author:"";
							side_html+="side_"+folder+sub+"\"><span style=\"width:50%\">("+msg[i].rev+") "+msg[i].date+author+"</span><span style=\"width:50%\">actions</span><div class=\"child hide\"></div>";
							html+=folder+sub+"\" class=\"folder\"><a href=\"javascript:void(0)\">"+sub+"</a><div class=\"child\"></div>";
						}else{
							html+=folder+msg[i].name+"\" class=\"file\"><a href=\"javascript:void(0)\">"+msg[i].name+"</a>";
							side_html+="side_"+folder+msg[i].name+"\"><span style=\"width:50%\">("+msg[i].rev+") "+msg[i].date+"</span><span style=\"width:50%\">actions</span>";
						}
						html+="</div>";
						side_html+="</div>";
					}
				}else{
					html="<div class=\"empty\">empty</div>";
					side_html="<div class=\"empty\">&nbsp;</div>";
				}
				$me.siblings(".child").html(html).hide().slideDown();
				$("div[id*=\'side_"+side_folder+"\']").children(".child:first").html(side_html).hide().slideDown();
				$me.addClass("open");
			},"json");
		}else{
			$me.siblings(".child").slideUp();
			$("div[id*=\'side_"+side_folder+"\']").children(".child:first").slideUp();
			$me.removeClass("open");
		}
	});

	$(document).on("click", ".file a", function(){
		var file=$(this).parent().attr("id");

		/*$.post("/ajax/scm_view.php",{"repo":repo,"file":file},function(msg){
			$("#filename").text("/"+repo+file);
			$("#file_contents").text(msg);
			$("#tree_viewer").animate({"left":-hiddenWidth});
			$("#file_read").animate({"left":5});
		});*/

		$.post("/ajax/scm_download.php",{"repo":repo,"file":file},function(msg){
			window.open(msg);
		});
	});

	$("#back").click(function(){
		$("#tree_viewer").animate({"left":5});
		$("#file_read").animate({"left":hiddenWidth+20});
	});
});';
add_js($JS);

scm_header(array('group'=>$GroupID));
$Layout->col(12,true);

echo '<style>
#main_content{height:400px;overflow: hidden}
#file_read{position:absolute;left:600px}
#tree_viewer{/*position:absolute;*/width:100%}
#tree,#tree_side{width:50%;float:left}
#tree_side span{display:inline-block}
#tree div {padding-left:12px;}
#tree .folder{background: url("/images/ic/folder.png") no-repeat 0 3px}
#tree .file{background:url("/images/ic/file.png") no-repeat 0 3px}
.file_nav{background:#CCC;border-bottom:1px solid #666;margin: -5px 0 0 -5px;padding:3px;}
.file_nav span{display:inline-block;font-weight:bold}
#filename{font-weight:bold}
</style>';

echo '<div id="tree_viewer">
<div class="file_nav">
<span style="width:50%">Path</span><span style="width:25%">Last Modification</span><span style="width:25%">Actions</span>
</div>
<div id="tree">';

require_once $gfplugins.'scmsvn/common/SVNPlugin.class.php';
$SVNPlugin=new SVNPlugin;

//var_dump($SVNPlugin->repoCheck($Group->getUnixName()));

if (isset($Group)){
	$Repo=$Group->getUnixName();
}else{
	$Repo=getStringFromRequest("repo");
}

$Folder=isset($_REQUEST['folder'])?getStringFromRequest("folder"):'/';

$Tree=$SVNPlugin->treeGetChildren($Repo,$Folder);

if (sizeof($Tree)){
	foreach($Tree as $File){
		if (strpos($File->name,'/')){
			//Is folder
			$SubFolder=substr($File->name,0,strlen($File->name)-1);
			echo '<div id="'.$Folder.$SubFolder.'" class="folder"><a href="javascript:void(0)">'.$SubFolder.'</a><div class="child hide"></div></div>';
		}else{
			//Is file
			echo '<div id="'.$Folder.$File->name.'" class="file"><a href="javascript:void(0)">'.$File->name.'</a></div>';
		}
	}
}else{
	echo '<div class="empty">empty</div>';
}

echo '</div>
<div id="tree_side">';
if (sizeof($Tree)){
	foreach($Tree as $File){
		if (strpos($File->name,'/')){
			//Is folder
			$SubFolder=substr($File->name,0,strlen($File->name)-1);
			echo '<div id="side_'.$Folder.$SubFolder.'"><span style="width:50%">('.$File->rev.') '.$File->date.' by '.$File->author.'</span><span style="width:50%"></span><div class="child hide"></div></div>';
		}else{
			//Is file
			echo '<div id="side_'.$Folder.$File->name.'"><a href="javascript:void(0)">'.$File->name.'</a></div>';
		}
	}
}else{
	echo '<div>empty</div>';
}
echo '</div>
</div>
<!--<div id="file_read">
<div class="file_nav"><a href="javascript:void(0)" id="back">&lt; Back</a> <span id="filename"></span></div>
<div id="file_contents"></div>
</div>-->';
$Layout->endcol();
scm_footer(); 
?>
