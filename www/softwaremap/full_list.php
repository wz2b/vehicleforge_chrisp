<?php
/*
 * Copyright (C) 2008-2009 Alcatel-Lucent
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
 * Standard Alcatel-Lucent disclaimer for contributing to open source
 *
 * "The Full List ("Contribution") has not been tested and/or
 * validated for release as or in products, combinations with products or
 * other commercial use. Any use of the Contribution is entirely made at
 * the user's own responsibility and the user can not rely on any features,
 * functionalities or performances Alcatel-Lucent has attributed to the
 * Contribution.
 *
 * THE CONTRIBUTION BY ALCATEL-LUCENT IS PROVIDED AS IS, WITHOUT WARRANTY
 * OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, COMPLIANCE,
 * NON-INTERFERENCE AND/OR INTERWORKING WITH THE SOFTWARE TO WHICH THE
 * CONTRIBUTION HAS BEEN MADE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * ALCATEL-LUCENT BE LIABLE FOR ANY DAMAGES OR OTHER LIABLITY, WHETHER IN
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * CONTRIBUTION OR THE USE OR OTHER DEALINGS IN THE CONTRIBUTION, WHETHER
 * TOGETHER WITH THE SOFTWARE TO WHICH THE CONTRIBUTION RELATES OR ON A STAND
 * ALONE BASIS."
 */


require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'include/trove.php';
require_once $gfwww.'include/TableGen.php';

if (!forge_get_config('use_project_full_list')) {
	exit_disabled();
}

if (!session_loggedin()) { // || $sf_user_hash) {
	//exit_not_logged_in();
}
$HTML->header(array('title'=>_('Project Listing'),'pagename'=>'softwaremap'));

//$Layout->col(9,true);

if (!session_loggedin()) { // || $sf_user_hash) {
  $Layout->col(12,true);
  //exit_not_logged_in();
  echo '<div class="alert alert-info" style="text-align:center; margin-bottom: 0;">There are currently no public projects. <strong><a href="../account/login.php">Log in</a></strong> to see your projects.</div>';
  $Layout->endcol();
} else{
  $Layout->col(9,true);


//$HTML->printSoftwareMapLinks();
$res_grp = db_query_params ('
	SELECT group_id, group_name, unix_group_name, short_description, register_time, export_control
	FROM groups
	WHERE status = $1 AND type_id=1 AND group_id>4 AND register_time > 0
	ORDER BY group_name ASC',
    array ('A'),$TROVE_HARDQUERYLIMIT);
$projects = array();

while ($row_grp = db_fetch_array($res_grp)) {
  //TODO: REMOVE THESE ECHO LINES
  echo "\n<!--forge_check_perm=".forge_check_perm('project_read', $row_grp['group_id']).", ".$row_grp['group_name']."-->\n";
  echo "<!--session_loggedin=".session_loggedin()."-->\n";
  echo "<!--util_export_control_check=".util_export_control_check($row_grp['group_id'],session_get_user()->getID()).", ".$row_grp['group_id'].", ".session_get_user()->getID()."-->\n";
  //END OF TODO
    if (forge_check_perm('project_read', $row_grp['group_id']) && (session_loggedin() && util_export_control_check($row_grp['group_id'], session_get_user()->getID()))) {
        $projects[] = $row_grp;
    }
}
$querytotalcount = count($projects);
if($querytotalcount <= 0 ){
  echo '<div class="alert alert-info" style="text-align:center; margin-bottom: 0;">You are not a part of any projects.</div>';
} else{	

// #################################################################
// limit/offset display

$page = getIntFromRequest('page',1);

// store this as a var so it can be printed later as well
$html_limit = '<span style="text-align:center;font-size:smaller">';
if ($querytotalcount == $TROVE_HARDQUERYLIMIT){
	$html_limit .= sprintf(_('More than <strong>%1$s</strong> projects in result set.'), $querytotalcount);
}
$html_limit .= sprintf(_('<strong>%1$s</strong> projects in result set.'), $querytotalcount);

// only display pages stuff if there is more to display
if ($querytotalcount > $TROVE_BROWSELIMIT) {
	$html_limit .= sprintf(_(' Displaying %1$s per page. Projects sorted by alphabetical order.'), $TROVE_BROWSELIMIT).'<br/>';

	// display all the numbers
	for ($i=1;$i<=ceil($querytotalcount/$TROVE_BROWSELIMIT);$i++) {
		$html_limit .= ' ';
		if ($page != $i) {
			$html_limit .= '<a href="'.$_SERVER['PHP_SELF'];
			$html_limit .= '?page='.$i;
			$html_limit .= '">';
		} else $html_limit .= '<strong>';
		$html_limit .= '&lt;'.$i.'&gt;';
		if ($page != $i) {
			$html_limit .= '</a>';
		} else $html_limit .= '</strong>';
		$html_limit .= ' ';
	}
}

$html_limit .= '</span>';

//echo $html_limit.'<br /><br />';

// print actual project listings

//$ProjectLayout=new LayoutGen;
for ($i_proj=0;$i_proj<$querytotalcount;$i_proj++) { 
	$row_grp = $projects[$i_proj];

	// check to see if row is in page range
	if (($i_proj >= (($page-1)*$TROVE_BROWSELIMIT)) && ($i_proj < ($page*$TROVE_BROWSELIMIT))) {
		$viewthisrow = 1;
	} else {
		$viewthisrow = 0;
	}	

	if ($viewthisrow) {
		
		//$ProjectLayout->col(2);
		$Group=new group($row_grp['group_id']);
		?>
			<div class="cell">
				<div class="pull-left">
					<a href="/projects/<?=$row_grp['unix_group_name']?>"><?=$Group->getImageHTML(100,100)?></a>
				</div>
				
				<div style="margin-left: 120px;">
					<h2><a href="/projects/<?=$row_grp['unix_group_name']?>"><?=$row_grp['group_name']?></a></h2>
					<p><?=$row_grp['short_description']?></p>
				</div>
			</div>
		<?php
	}
}
}
$Layout->endcol()->col(3);
?>

<div class="well" id="relation-pane">
   <h4 style="text-align: center;"><?php printf (_('Collaborate through %1$s'), forge_get_config ('forge_name')); ?> </h4>
   <div class="btn; margin: 0 auto;" align="center">
      <a  href="<?=util_make_url ('register/'); ?>"><?=_('Create Project'); ?></a>
   </div>
</div>

<?php
$Layout->endcol();

// print bottom navigation if there are more projects to display
if ($querytotalcount > $TROVE_BROWSELIMIT) {
	print $html_limit;
}
}

$HTML->footer(array());

?>
