<!DOCTYPE html>
<html>
    <head>
        <script src="js/jquery-1.7.1.min.js"></script>
        <script src="js/pusher-1.11.2.js"></script>
        <script src="js/jquery-ui-1.8.18.custom.min.js"></script>
        <script>
$(function(){
    var Connections=[],
        pusher=new Pusher('38f1f65e2e6c0a432027'),
        Channel=pusher.subscribe('PartChannel');

    Channel.bind('LockPart',function(data){
        lockPart($("#"+data.message));
    });

    Channel.bind('UnlockPart',function(data){
        unlockPart($("#"+data.pid), data.pos);
    });

    Channel.bind('ConnectPart',function(data){
        connectPart(data.child, data.parent);
    });


    var $List=$("#list");

    var y=0;

    $(".part").each(function(){
        $(this).css('top',y+'px');
        y+=20;
    }).draggable({
        revert:"valid",
        start:function(){
            lockPart($(this));
            $.post('ajax/lock_part.php',{pid:$(this).attr("id")});
        },
        stop:function(ev,ui){
            var pos=$(this).position();
            unlockPart($(this), pos);
            $.post('ajax/unlock_part.php',{pid:$(this).attr("id"),pos:pos});
        }
    }).droppable({
        drop:function(event,ui){
            //connectPart(ui.draggable.text(), $(this).text());
            $.post('ajax/connect.php',{child:ui.draggable.text(), parent:$(this).text()});
        }
    });

    function lockPart($object){
        $object.addClass('locked');
    }

    function unlockPart($object, pos){
        $object.removeClass('locked').animate({
            left:pos.left+"px",
            top:pos.top+"px"
        });
    }

    function connectPart(ChildPart, ParentPart){
        Connections.push([ChildPart, ParentPart]);
        $List.append("<li>"+ParentPart+" -> "+ChildPart+"</li>");
    }
});
        </script>
        <style>
            #container  {
                width: 600px;
                height: 600px;
                border: 1px solid #000;
                overflow: hidden;
                position:relative;
            }

            .part   {
                border: 1px solid #000;
                width: 100px;
                height: 20px;
                cursor: pointer;
                position: absolute;
            }

            .locked {
                background: #CCC;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <div id="Enclosure" class="part">Enclosure</div>
            <div id="Fan" class="part">Fan</div>
            <div id="Blades" class="part">Blades</div>
            <div id="Magnets" class="part">Magnets</div>
            <div id="Stand" class="part">Stand</div>
            <div id="Wires" class="part">Wires</div>
            <div id="FanHead" class="part">Fan Head</div>
            <div id="Motor" class="part">Motor</div>
        </div>
        <ul id="list">
            
        </ul>
    </body>
</html>
