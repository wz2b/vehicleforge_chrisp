<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:14 PM 10/10/11
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/GroupJoinRequest.class.php';
$GroupID=getIntFromGet('group_id');
$Group=group_get_object($GroupID);
$Comment=getStringFromGet('comment');

$User=user_getid();
$GJR=new GroupJoinRequest($Group,$User);

$Return=array();
if ($GJR->create($User,$Comment,false)){
	$Return['status']=true;
}else{
	$Return['status']=false;
	$Return['msg']=$GJR->getErrorMessage();
}

echo json_encode($Return);
?>