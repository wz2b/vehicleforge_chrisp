<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:41 AM 9/30/11
 */
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/session.php';
require_once $gfcommon.'include/User.class.php';
require_once EMAIL_CONFIG_INC;

if (user_getid()){
	$User=user_get_object(user_getid());
	$User=$User->getRealName();
}else{
	$User='anonymous';
}

$Text=$_POST['text'];
$Page=$_POST['page'];
$Time=time();

$Text="Feedback: ".$Text."\n
Page: ".$Page."\n
Time: ".date("n/j g:ia",$Time)."\n
User: ".$User;
echo $Text;

$to      = $feedback_addres;
$subject = 'VehicleForge Feedback';
$headers = 'From:' . $feedback_address . "\r\n" .
    'Reply-To: ' . $site_admin_address . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

echo mail($to, $subject, $Text, $headers);
?>