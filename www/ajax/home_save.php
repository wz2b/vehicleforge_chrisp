<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:09 PM 11/29/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/HomeComments.class.php';

$NewContent=getStringFromRequest('new');
$CommentID=getIntFromRequest('cid');

$Return=array();
$Return['error']=true;

if (update_single($CommentID,$NewContent)){
	$Return['error']=false;
}else{
	$Return['msg']='Could not update the DB';
}

echo json_encode($Return);
?>