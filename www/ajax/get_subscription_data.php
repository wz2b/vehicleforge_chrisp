<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:13 PM 11/30/11
 */

//Gets a single subscription HTML

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

/*$RefID=getIntFromRequest('rid');
$TypeID=getIntFromRequest('tid');
$PostUserID=getIntFromRequest('uid');*/
$CommentID=getIntFromRequest('cid');
/*$Comment=getStringFromRequest('comment');
$Time=getIntFromRequest('time');*/

$User=session_get_user();
echo json_encode($User->formatSubscriptionContent($User->getSingleSubscriptionData($CommentID)));
//echo json_encode($User->formatSubscriptionContent(array('user_id'=>$PostUserID,'comment_id'=>$CommentID,'type_id'=>$TypeID,'ref_id'=>$RefID,'comment'=>$Comment,'time_posted'=>$Time)));
?>