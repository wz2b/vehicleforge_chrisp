<?php
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon.'include/RecommendationManager.class.php';

$Value=getIntFromRequest("value");
$TypeID=getIntFromRequest("type_id");
$UserID=getIntFromRequest("user_id");

$Return=array('error'=>false);

if (is_int($TypeID) && $UserID){
	if ($Value==0 || $Value==1){
		if ($TypeID==1){
			$UR=new UserRecommendation;
			if (!$UR->set(user_getid(),$UserID,$Value)){
				$Return['error']=true;
				$Return['msg']=$UR->getErrorMessage();
			}
		}
	}else{
		$Return['error']=true;
		$Return['msg']='Value must be 0 or 1';
	}
}else{
	$Return['error']=true;
	$Return['msg']='Must have type and user_id';
}

$Rec=$UR->getRec($UserID);
$Return['yes']=$Rec[0];
$Return['no']=$Rec[1];

echo json_encode($Return);
?>