<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once 'community_util.php';

use_javascript('bootstrap/tabs.js');
use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('/themes/gforge/css/forums.css');
use_stylesheet('community_style.css');
site_community_header(array('title'=>'Discussion Board', 'subnav'=>2), 2);

// Get query details
$GroupID=1;
$Group=group_get_object($GroupID);
$ForumID=getIntFromRequest('forum',false);
$TopicID=getIntFromRequest('topic',false);
$SectionID=getIntFromRequest('section',false);
$IsLoggedIn = (session_get_user()) ? true : false;

// Create objects based on query
if ($ForumID) {
    $Forum=new Forum($ForumID);
    $Section=CommunityForumSection::getSectionByForum($ForumID);
}
if ($TopicID) {
    $Topic=new ForumTopic($TopicID);
}
if ($ForumID && $TopicID){
    if (getIntFromRequest('topic-reply',false))
        ForumMessage::create($TopicID, session_get_user()->getID(), getStringFromRequest('body'), getIntFromRequest('message',0));
}

/****************** SECTION PAGE **************************/

if ($SectionID){
    // Display a list forums in a section
    $Layout->col(12, true);
    $Section = CommunityForumSection::getSection($SectionID);
	
	
	// Display breadcrumb navigation
    echo util_make_breadcrumbs(array(
        'Message Boards Home'       => 'boards.php',
        $Section['section_name']    => 'boards.php?section='.$SectionID
    ));

    $Forums=CommunityForumSection::getForums($SectionID);
	
	// Check for forums
	if(!empty($Forums)){
		// Display table containing details of each forum in this section
		$Table=new BsTable(array('class'=>'table table-striped'));
    	$Table->head(array('Forum', 'Description', 'Last Activity'));

	    foreach($Forums as $f){
	        $Table->col('<a href="?forum='.$f->getID().'">'.$f->getName().'</a>')
	            ->col($f->getDescription())
	            ->col(date('M d, Y g:ia',$f->getLastActivity()));
	    }
	
	    echo $Table->render();
	} else{
		// There are no forums in this section; display message
	?>
		<div class="alert alert-info" style="text-align:center"><strong>Sorry,</strong> this section is empty.</div>
	<?php
	}    
}

/****************** TOPIC PAGE **************************/

elseif ($ForumID && $TopicID){
	// Display a topic or reply form
    $Layout->col(12,true);

	// Display breadcrumb navigation
    echo util_make_breadcrumbs(array(
        'Message Boards Home'       => 'boards.php',
        $Section['section_name']    => 'boards.php?section='.$Section['section_id'],
        $Forum->getName()           => '?forum='.$ForumID,
        $Topic->getName()           => '?forum='.$ForumID.'&topic='.$TopicID
    ));
	
    if (getStringFromRequest('cmd')){
        switch(getStringFromRequest('cmd')){
            case 'reply':
            	// Check that the user is logged in
            	if($IsLoggedIn){
					// Display form fields to reply to topic
	                $Form=new BsForm();
	                echo $Form->init('?forum='.$ForumID.'&topic='.$TopicID.'&message='.getIntFromRequest('message') ,'post',array('class'=>'form form-horizontal'))
	                    ->head(_('Reply to Topic <em>'.$Topic->getName().'</em>'))
	                    ->group(_('Message'),
	                        new Textarea('',array(
	                            'name'      => 'body'
	                        )),
	                        new Hidden(array(
	                            'name'      => 'topic-reply',
	                            'value'     => 1
	                        ))
	                    )
	                    ->actions(
	                        new Submit()
	                    )
	                    ->render();
				} else{
					// User is not logged in
				?>
					<div class="page">
					<div class="alert alert-error" style="text-align:center"> 
						<strong>You must be logged in to reply to this topic.</strong> <br/><br/>
						<a class="btn btn-large" href="?forum='<?php echo $ForumID;?>'&topic='<?php echo $TopicID;?>">Return to Topic</a> <a class="btn btn-large" href="../account/login.php">Log In</a>
					</div>
					</div>
				<?php
				}
	            break;
        }
    } else{
    	// Display topic page
    	// Check that the user is logged in
        if($IsLoggedIn){
    		// Display controls to create a new topic in this forum or reply to this topic
        	echo '<a href="?forum='.$ForumID.'&cmd=topic">New Topic</a> | <a href="?forum='.$ForumID.'&topic='.$TopicID.'&cmd=reply">Reply</a>';
		} else {
			// Display option to log in
			echo '<a href="../account/login.php">Log In to Reply</a>';
		}
    	
		
		// Display topic and responses		
        $HTML->heading($Topic->getName(),2);
        echo $Topic->getMessagesThreaded(true);  // $Topic->getMessagesFlat();
    }
}

/****************** FORUM PAGE **************************/
elseif($ForumID){
    // Display a list topics in a forum
    $Layout->col(12,true);

	// Display a list forums in a section
    echo util_make_breadcrumbs(array(
        'Message Boards Home'       => 'boards.php',
        $Section['section_name']    => 'boards.php?section='.$Section['section_id'],
        $Forum->getName()           => '?forum='.$ForumID
    ));

    if (getStringFromRequest('cmd')){
        switch (getStringFromRequest('cmd')){
            case 'topic':
				// Check that the user is logged in
            	if($IsLoggedIn){
					// Display form fields to create new topic in this forum
	                $Form=new BsForm();
	                echo $Form->init('?forum='.$ForumID.'&cmd=topic-post','post', array('class'=>'form form-horizontal'))
	                    ->head(_('Create a New Topic'))
	                    ->group(_('Title'),
	                        new Text(array(
	                            'name'      => 'title'
	                        ))
	                    )
	                    ->group(_('Message'),
	                        new Textarea('',array(
	                            'name'      => 'body'
	                        ))
	                    )
	                    ->actions(
	                        new Submit()
	                    )
	                    ->render();
				} else{
					// User is not logged in
				?>
					<div class="page">
					<div class="alert alert-error" style="text-align:center"> 
						<strong>You must be logged in to create a topic.</strong> <br/><br/>
						<a class="btn btn-large" href="?forum='<?php echo $ForumID;?>'">Return to Forum</a> <a class="btn btn-large" href="../account/login.php">Log In</a>
					</div>
					</div>
				<?php
				}
                break;

            case 'topic-post':
				if($IsLoggedIn){
					// Add new topic to DB (??)
	                db_begin();
	                $UserID=session_get_user()->getID();
	                if ($NewTopicID=ForumTopic::create($ForumID, getStringFromRequest('title'), $UserID)){
	                    if (ForumMessage::create($NewTopicID, $UserID, getStringFromRequest('body'))){
	                        echo '<div class="alert alert-success" style="text-align:center">You successfully created a new topic. <a href="?forum='.$ForumID.'&topic='.$NewTopicID.'">Click here to visit the new topic.</a></div>';
	                        db_commit();
	                    }
	                }
	                db_rollback();
				} else{
					// User is not logged in
				?>
					<div class="page">
					<div class="alert alert-error" style="text-align:center"> 
						<strong>You must be logged in to create a topic.</strong> <br/><br/>
						<a class="btn btn-large" href="?">Return to Discussion Boards</a> <a class="btn btn-large" href="../account/login.php">Log In</a>
					</div>
					</div>
				<?php
				}
				
                break;
        }
    }else{
    	// Display forum page
    	
    	// Check that the user is logged in
        if($IsLoggedIn){
    		// Display controls to create a new topic in this forum
	        echo '<a href="?forum='.$ForumID.'&cmd=topic">New Topic</a>';
		} else {
			// Display option to log in
			echo '<a href="../account/login.php">Log In to Create New Topic</a>';
		}
			
		$Topics=$Forum->getTopics();
		
		// Check for topics
		if(!empty($Topics)){
			// Display table containing details of each topic in this forum
	        $Table=new BsTable(array('class'=>'table table-striped'));
	        $Table->head(array('Topic', 'Author', 'Replies', 'Last Activity'));
	        
	        foreach($Topics as $t){
	            $Table->col('<a href="?forum='.$ForumID.'&topic='.$t->getID().'">'.$t->getName().'</a>')
	                ->col(user_get_object($t->getUserID())->getRealName())
	                ->col($t->getMessageCount())
	                ->col($t->getLastMessage()->getUser()->makeLink().'<br />'.date('M d, Y g:ia',$t->getLastActivity()));
	        }
	
	        echo $Table->render();
		} else{
			// There are no topics in this forum; display message
		?>
			<div class="alert alert-info" style="text-align:center"><strong>Sorry,</strong> this forum is empty. <?php if($IsLoggedIn){ echo 'Would you like to create a <a href="?forum='.$ForumID.'&cmd=topic">new topic</a>?'; } ?></div>
		<?php
		}   
    }
}

/****************** DISCUSSION BOARDS HOME PAGE **************************/
else{
	if (getStringFromRequest('cmd')){
        switch (getStringFromRequest('cmd')){
            case 'topic':
				$Layout->col(12,true);
				
				// Check that the user is logged in
            	if($IsLoggedIn){
					// Get list of forums
					$Sections=CommunityForumSection::getSections();
					$ForumOptions = array();
			        foreach($Sections as $s){
			        	foreach(CommunityForumSection::getForums($s['section_id']) as $f){
			                $ForumOptions[$f->getID()] = $s['section_name'] .' / '. $f->getName();
			            }
			        }
					
					// Display form fields to create new topic
	                $Form=new BsForm();
	                echo $Form->init('?cmd=topic-post','post', array('class'=>'form form-horizontal'))
	                    ->head(_('Create a New Topic'))
	                    ->group(_('Title'),
	                        new Text(array(
	                            'name'      => 'title'
	                        ))
	                    )
						->group(_('Forum'),
	                        new Dropdown($ForumOptions, null, array(
	                            'name'	=> 'forum_id'
	                        ))
	                    )
	                    ->group(_('Message'),
	                        new Textarea('',array(
	                            'name'      => 'body'
	                        ))
	                    )
	                    ->actions(
	                        new Submit()
	                    )
	                    ->render();
				} else{
					// User is not logged in
				?>
				<div class="page">
					<div class="alert alert-error" style="text-align:center"> 
						<strong>You must be logged in to create a topic.</strong> <br/><br/>
						<a class="btn btn-large" href="?">Return to Discussion Boards</a> <a class="btn btn-large" href="../account/login.php">Log In</a>
					</div>
				</div>
				<?php
				}
                break;

            case 'topic-post':
				if($IsLoggedIn){
					// Add new topic to DB
					db_begin();
	                $UserID=session_get_user()->getID();
					
	                if ($NewTopicID=ForumTopic::create(getStringFromRequest('forum_id'), getStringFromRequest('title'), $UserID)){
	                    if (ForumMessage::create($NewTopicID, $UserID, getStringFromRequest('body'))){
	                    	echo '<div class="alert alert-success" style="text-align:center">You successfully created a new topic. <a href="?forum='.getStringFromRequest('forum_id').'&topic='.$NewTopicID.'">Click here to visit the new topic.</a></div>';
	                        db_commit();
	                    }
	                }
	                db_rollback();
				} else{
					// User is not logged in
				?>
					<div class="page">
					<div class="alert alert-error" style="text-align:center"> 
						<strong>You must be logged in to create a topic.</strong> <br/><br/>
						<a class="btn btn-large" href="?">Return to Discussion Boards</a> <a class="btn btn-large" href="../account/login.php">Log In</a>
					</div>
					</div>
				<?php
				}
                break;
        }
    } else{
		// Display discussion boards home
	    // Show summary of most popular and most recent posts.
	    $Sections=CommunityForumSection::getSections();
	
	    $Layout->col(3,true);
		
		// Javascript that allows for collapsable navigation.
	    ?>
		<script>
		    $(function(){
		        $('.section').click(function(){
		            var $me=$(this);
		            if (!$me.hasClass('active')){
		                $('.section').removeClass('active');
		                $('.section').parent().removeClass('active');
		                $('.section i').removeClass('icon-chevron-down');
		                $('.section i').addClass('icon-chevron-right');
		                $('.sub').slideUp();
		                $me.parent().addClass('active');
		                $me.addClass('active').siblings('.sub').slideDown();
		                $("i", this).removeClass();
		                $("i", this).addClass('icon-chevron-down');
		            } else{
		                $('.section').removeClass('active');
		                $('.section').parent().removeClass('active');
		                $('.section i').removeClass('icon-chevron-down');
		                $('.section i').addClass('icon-chevron-right');
		                $('.sub').slideUp();
		            }
		        });
		    });
		</script>
		<?php // Discussion board navigation ?>
		<div class="well">
		    <ul class="nav nav-list" id="forum_nav">
		        <li class="nav-header">Forum Navigation</li>
		        <?php
		        
		        foreach($Sections as $s){
		        	// Display category as a collapsable heading
		            echo '<li>
		                <a href="javascript:void(0)" class="section">'.$s['section_name'].'</a>
		                <ul class="hidden sub">';
		
		            foreach(CommunityForumSection::getForums($s['section_id']) as $f){
		            	// Display forums in each category
		                //$Forum=new Forum($f);
		                echo '<li><a href="?forum='.$f->getID().'">'.$f->getName().'</a></li>';
		            }
		            echo '</ul>
		            </li>';
		        }
		        ?>
		    </ul>
		    <?php // Check if user is logged in to show new topic button 
			if($IsLoggedIn){ ?>
				<hr />
				<a class="btn" style="width: 160px;" href="?cmd=topic">New Topic</a>
			<?php } ?>
		</div>
		<?php
		    $Layout->endcol()->col(9);
			
			// Discussion board content - tabs for latest and most popular topics
		?>
		
	    <div class="hero-unit">
	        <h2>Welcome to the Discussion Boards</h2>
	        <h4>Talk with other members of the community.</h4>
	    </div>

		<div class="tabbable">
		    <ul class="nav nav-tabs">
		        <li class="active"><a href="#newest" data-toggle="tab">Newest Topics</a></li>
		        <li><a href="#popular" data-toggle="tab">Popular Topics</a></li>
		    </ul>
		    <div class="tab-content">
		        <div class="tab-pane active" id="newest">
		            <?php
		            /** Display list of topics in order of the most recent first. **/
		            
		            $Topics = getLatestDiscussions();
					
					// Check for topics
					if(!empty($Topics)){
					
			            $Table=new BsTable(array('class'=>'table table-striped'));
			            // Table Header
			            $Table->head(array('Topic', 'Author', 'Replies', 'Latest Activity'));
						// Topics
						foreach($Topics as $t){
			                $Table->col($t->makeLink(true))
			                    ->col($t->getUser()->getRealName())
			                    ->col($t->getMessageCount())
			                    ->col($t->getLastMessage()->getUser()->makeLink().'<br />'.date('M d, Y g:ia',$t->getLastActivity()));
				        }
						// Display table of topics
			            echo $Table->render();
					} else{
						// There are no topics; display message
					?>
						<div class="alert alert-info" style="text-align:center"><strong>Sorry,</strong> this discussion board is empty. <?php if($IsLoggedIn){ echo 'Would you like to create a <a href="?cmd=topic">new topic</a>?'; } ?></div>
					<?php
					}
		            
		            ?>
		        </div>
		        <div class="tab-pane" id="popular">
		            <?php
		            /** Display list of topics in order of the most popular first. **/
		            
		            $Topics = getPopularDiscussions();
					
					// Check for topics
					if(!empty($Topics)){
					
			            $Table=new BsTable(array('class'=>'table table-striped'));
			            // Table Header
			            $Table->head(array('Topic', 'Author', 'Replies', 'Latest Activity'));
						// Topics
						foreach($Topics as $t){
			                $Table->col($t->makeLink(true))
			                    ->col($t->getUser()->getRealName())
			                    ->col($t->getMessageCount())
			                    ->col($t->getLastMessage()->getUser()->makeLink().'<br />'.date('M d, Y g:ia',$t->getLastActivity()));
				        }
						// Display table of topics
			            echo $Table->render();
					} else{
						// There are no topics; display message
					?>
						<div class="alert alert-info" style="text-align:center"><strong>Sorry,</strong> this discussion board is empty. Would you like to create a <a href="?forum='0'&cmd=topic">new topic</a>?</div>
					<?php
					}
		            
		            ?>
		        </div>
		    </div>
		</div>
		<?php
	}
}


$Layout->endcol();
site_community_footer();

db_display_queries();
?>