<?php

require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once 'community_util.php';

$IsLoggedIn = (session_get_user()) ? true : false;

// Check if page id from query is valid
$pageID = getStringFromRequest('id');
$searchQuery = getStringFromRequest('q');
if($pageID && validWikiID($pageID)){
    $page = new WikiPage($pageID);
    $catID = $page->getCatID();
}

use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('community_style.css');
use_javascript('bootstrap/tabs.js');
site_community_header(array('title'=>'Resources'), 3);                   

$Layout->col(3,true);
?>

<script>
    $(function(){
        $('.section').click(function(){
            var $me=$(this);
            if (!$me.hasClass('active')){
               $('.current').removeClass('active');
               $('.current').parent().removeClass('active');               
               $('.current').siblings('.sub').slideUp();
               $('.current').removeClass('current');            
               <?php            
                       if(isset($pageID)){
                           echo '$("#toc'.$pageID.'").removeAttr("style")';              
                       }
               ?>             
                $me.parent().addClass('active');
                $me.addClass('active').addClass('current').siblings('.sub').slideDown();
                
            } else{
            	 $me.parent().removeClass('active');
                 $me.removeClass('active').siblings('.sub').slideUp();
            }
        });
        
        <?php 
            if(isset($catID)){
                echo '$("#cat'.$catID.'").click();';
                echo '$("#cat'.$catID.'").addClass("current").next().addClass("current");';
            }
            if(isset($pageID)){
                echo '$("#toc'.$pageID.'").css("font-weight","bold").css("color","#666");';
                //echo '$("#toc'.$pageID.'").addClass("current")';
            }
        ?>
        
        $('#searchQuery').keyup(function(event){
            if(event.keyCode == 13){
                window.location = $('#searchBtn').attr('href');
            }else{
                $('#searchBtn').attr('href', 'resources.php?q='+ $(this).val());
            }
        });
    });
</script>

    <div class="container">
            <div class="row">
                <div class="span3">
                    <style>
                        .nav .nav-header {
                            padding: 3px 0px;
                        }
                    </style>
                    <div class="well">
                        <ul class="nav nav-list" id="forum_nav">
                            <li class="nav-header">Search Resources</li>
                        </ul>
                        <!-- TODO: Search Resources for keywords -->
                        <div class="input-append">
                            <input id="searchQuery" size="16" type="text" style="width: 108px;"><a id="searchBtn" href="resources.php?q=" class="btn btn-primary">Search</a>
                        </div>
                        <hr />
                        <ul class="nav nav-list" id="forum_nav">
                            <li class="nav-header">Browse Resources</li>
                            <li>
                                <a href="#" class="section">Resources Home</a>
                            </li>
                            <?php
                                $Categories = getResourceCategories();
                                foreach($Categories as $c){
                                    echo '<li><a href="#" class="section" id="cat'. $c["cat_id"] .'">'.$c["name"].'</a><ul class="hidden sub">';
                                    $Resources = getResources($c["cat_id"]);
                                    foreach($Resources as $r){
                                        echo '<li><a href="resources.php?id='.$r->getID().'" id="toc'.$r->getID().'">'.$r->getPageName().'</a></li>';
                                    }
                                    echo '</ul></li>';
                                }
                            ?>
                            <li>
                                <a href="?title=all">View All Resources</a>
                            </li>
                        </ul>
                        <?php if($IsLoggedIn){ ?>
                            <hr />
                            <a href="editor.php?action=create" class="btn" style="width: 156px;">New Page</a>
                        <?php
                        } ?>
                    </div>
                    
                </div> <!-- end col-->
                <div class="span9">
                    <?php
                    if(isset($searchQuery) && !empty($searchQuery)){
                        // Display search results
                        echo "<h2>Search Results for \"". $searchQuery ."\"</h2><ul>";
                        $Resources = searchResources($searchQuery);
                        if(empty($Resources)){
                            echo "<p>There are no results for '".$searchQuery."'.</p>";
                        }else{
                            foreach($Resources as $r){
                                echo '<li><a href="?id='.$r->getID().'">'.$r->getPageName().'</a></li>';
                            }
                        }
                        echo '</ul>';
                    } elseif(isset($pageID) && isset($page)) {
                        if($IsLoggedIn){ 
                            // Display edit button?>
                            <a href="editor.php?action=edit&id=<?php echo $pageID; ?>" class="btn btn-small pull-right">Edit</a>
                        <?php } ?>
                        <h2>
                            <?php
                                // Display page contents
                                echo $page->getPageName();
                            ?>
                        </h2>
                        <p>
                            <?php
                                // Display page contents
                                echo $page->getPageContents();
                            ?>
                        </p>
                        <hr />
                        <p>
                            <?php
                                // Display modification information if it exists
                                if($page->getModifiedTime() > 0){
                                    $modifierUser = user_get_object($page->getModifiedUserID());
                                    echo '<small>Last modified by <a href="'.$modifierUser->getURL().'">'.$modifierUser->getRealName().'</a> on '. date('M d, Y g:ia',$page->getModifiedTime()).' | </small>';
                                }

                                // Display creator information
                                $creator = user_get_object($page->getUserID());
                                if( $creator != null){
                                    echo '<small>Created by <a href="'.$creator->getURL().'">'.$creator->getRealName().'</a> on '. date('M d, Y g:ia',$page->getCreationTime()).'</small>';
                                } else {
                                    echo '<small>No Creator information found</small>';
                                }
                            ?>
                        </p>
                        
                    <?php } elseif(getStringFromRequest('title')=="all"){
                        // Display the list of all articles?>
                        <h2>All Resources</h2>
                        <?php
                            $Categories = getResourceCategories();
                            foreach($Categories as $c){
                                echo '<h6>'.$c["name"].'</h6><ul>';
                                $Resources = getResources($c["cat_id"]);
                                if(empty($Resources)){
                                    echo "<p><small>There are no resources to display.</small></p>";
                                }else{
                                    foreach($Resources as $r){
                                        echo '<li><a href="?id='.$r->getID().'">'.$r->getPageName().'</a></li>';
                                    }
                                }
                                echo '</ul>';
                            }
                        ?>
                        
                        
                    <?php }else{
                        // Display front page ?>
                        <div class="hero-unit">
                            <h2>Welcome to the Resource Center</h2>
                            <h4>Discover more about the community. </h4>
                        </div>

                        <div class="tabbable">
                            <ul class="nav nav-tabs" style="margin-bottom: -1px;">
                                <li class="active"><a href="#newest" data-toggle="tab">Recent Changes</a></li>
                                <li><a href="#popular" data-toggle="tab">Popular Resources</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="newest">
                                    <table class="table table-striped table-condensed">
                                        <?php
                                            $Resources = getLatestResources();
                                            if(empty($Resources)){
                                                echo "<tr><td style='text-align:center'><small>There are no resources to display.</small></td></tr>";
                                            }else{
                                                foreach($Resources as $r){
                                                    echo '<tr><td><a href="resources.php?id='.$r->getID().'">'.$r->getPageName().'</a></td>';
                                                    if($r->getModifiedTime() > 0){
                                                        echo '<td style="text-align: right;">updated '. date('M d, Y g:ia',$r->getModifiedTime()) .'</td></tr>';
                                                    }else {
                                                        echo '<td style="text-align: right;">created '. date('M d, Y g:ia',$r->getCreationTime()) .'</td></tr>';
                                                    }
                                                }   
                                            }
                                        ?>
                                    </table>
                                </div>
                                <div class="tab-pane" id="popular">
                                    <table class="table table-striped table-condensed">
                                    <?php
                                            $Resources = getPopularResources();
                                            if(empty($Resources)){
                                                echo "<tr><td style='text-align:center'><small>There are no resources to display.</small></td></tr>";
                                            }else{
                                                foreach($Resources as $r){
                                                    echo '<tr><td><a href="resources.php?id='.$r->getID().'">'.$r->getPageName().'</a></td><td style="text-align: right;">viewed X times</td></tr>';
                                                }   
                                            }
                                   ?>
                                    </table>
                                </div>
                                <!-- end Mockup Data -->
                            </div>
                        </div>
                    <?php }?>
                </div><!-- end col-->
            </div><!-- end row-->
    </div>

<?php
$Layout->endcol();
include $gfwww.'help/help.php';
site_community_footer();
?>
