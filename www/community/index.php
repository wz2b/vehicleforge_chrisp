<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:19 AM 11/4/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'news/news_utils.php';
require_once $gfwww.'include/features_boxes.php';
require_once 'community_util.php';

$GroupID=1;
$Group=group_get_object($GroupID);


/* if (!session_loggedin())exit_not_logged_in(); */

use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('community_style.css');
site_community_header(array('title'=>_('Community')), 0);

?>

<div class="container">
    <div class="page">
        <div class="row">
            <div class="span9">
                <div class="btn-group">
                    <a href="index.php" class="btn active btn-info"><b>Popular</b></a>
                    <a href="latest.php" class="btn"><b>Latest</b></a>
                </div>
            </div>
        </div>
    
        <div class="row">
            <div class="span9">
                <div class="cell" helptip-text="If you're looking for a little inspiration or a big cash prize, check out the current challenges, find one you're interested in, and create a new project addressing that challenge." helptip-position="right">
                    <div class="widget features">
                        <div class="widget-header">
                            Popular Challenges
                            <div class="pull-right">
                                <a href="<?=util_make_url ('index2.php'); ?>"><?=_('view all'); ?></a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <?php // Display links to the 5 challenges with the most submissions ?>
                            <!-- Mockup Data -->
                            <div class="feature-block">
                                <img class="pull-left" src="/mock_images/drivetrain.jpg" width="50px"/>
                                <div class="pull-left description">
                                    <strong><a href="">Mobility/Drivetrain Challenge</a></strong> <br />
                                    <small>$1M prize | 1,404 submissions | closes May 29, 2012 12PM EST</small>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-info" href="">Read More</a>
                                </div>
                            </div>
                            <div class="feature-block">
                                <img class="pull-left" src="/mock_images/engine.png" width="50px"/>
                                <div class="pull-left description">
                                    <strong><a href="">Thermal Management for Hybrid XV Ground Vehicles</a></strong> <br />
                                    <small>$500K prize | 213 submissions | closes July 6, 2012 12PM EST</small>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-info" href="">Read More</a>
                                </div>
                            </div>
                            <!-- end Mockup Data -->
                        </div>
                    </div>
                </div>
            
                <div class="cell" helptip-text="Check out some of the most popular projects to see what other folks are doing." helptip-position="right">
                    <div class="widget features">
                        <div class="widget-header">
                            Popular Projects
                            <div class="pull-right">
                                <a href="<?=util_make_url ('softwaremap/full_list.php'); ?>"><?=_('view all'); ?></a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <?php
                                // Display links to the 5 project pages used the most
                                $Projects = getPopularProjects(5);
                                if(empty($Projects)){
                                    echo "<p style='text-align:center'><small>There are no projects.</small></p>";
                                }else{
                                    foreach($Projects as $p){
                                        //$p_data = $p->data_array;
                                        $Group = group_get_object($p->data_array['group_id']);
                                        ?>
                                        <div class="feature-block">
                                            <span class="pull-left" /><?=$Group->getImageHTML(50,50)?></span>
                                            <div class="pull-left description">
                                                <strong><a href="/projects/<?=$Group->getUnixName()?>"><?=$Group->getPublicName()?></a></strong> <br />
                                                <small><?=$Group->getDescription()?></small>
                                            </div>
                                            <div class="pull-right">
                                                <a class="btn btn-info" href="/projects/<?=$Group->getUnixName()?>">Read More</a>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                                ?>
                        </div>
                    </div>
                </div>
            
                <div class="cell" helptip-text="Try out popular components and scout out component creators to find an experienced designer for your team." helptip-position="right">
                    <div class="widget features">
                        <div class="widget-header">
                            Popular Components
                            <div class="pull-right">
                                <a href="<?=util_make_url ('marketplace/components.php'); ?>"><?=_('view all'); ?></a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <?php
                                // Display links to the 5 component pages used the most
                                // TODO: Get rigged up.  Dependant on marketplace.
                                //$Components = getPopularComponenets(5);
                                //foreach($Components as $c){
                                //...
                                //}
                                ?>
                            <!-- Mockup Data -->
                            <div class="feature-block">
                                <img class="pull-left" src="http://placehold.it/50x50" width="50px"/>
                                <div class="pull-left description">
                                    <strong><a href="">Component 1</a></strong> <br />
                                    <small>submitted by Jason Kaczmarsky, May 14, 2012</small>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-info" href="">Read More</a>
                                </div>
                            </div>
                            <div class="feature-block">
                                <img class="pull-left" src="http://placehold.it/50x50" width="50px"/>
                                <div class="pull-left description">
                                    <strong><a href="">Component 1</a></strong> <br />
                                    <small>submitted by Jason Kaczmarsky, May 14, 2012</small>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-info" href="">Read More</a>
                                </div>
                            </div>
                            <!-- end Mockup Data -->
                        </div>
                    </div>
                </div>
            
            </div><!--end col-->
            
            
            <div class="span3">
                
                <div class="cell">
                    <div class="widget featurette">
                        <div class="widget-header">
                            Hot Discussions
                            <div class="pull-right">
                            <a href="boards.php">view all</a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <?php
                                // Display links to the 5 discussion topics with the most posts
                                $Topics=getPopularDiscussions(5);
                                if(empty($Topics)){
                                    echo "<p style='text-align:center'><small>There are no discussion topics.</small></p>";
                                }else{
                                    foreach($Topics as $t){
                                        echo'<a href="'.$t->getURL(true).'">'.$t->getName().'</a>';
                                    }
                                }
                                ?>
                        </div>
                    </div>
                </div>
                
                <div class="cell">
                    <div class="widget featurette">
                        <div class="widget-header">
                            Popular Resources
                            <div class="pull-right">
                                <a href="resources.php">view all</a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <?php
                                // Display links to the 5 resource pages with the most page views
                                $Resources = getPopularResources(5);
                                if(empty($Resources)){
                                    echo "<p style='text-align:center'><small>There are no resources.</small></p>";
                                }else{
                                    foreach($Resources as $r){
                                        echo '<a href="resources.php?id='.$r->getID().'">'.$r->getPageName().'</a>';
                                    }   
                                }
                                ?>
                        </div>
                    </div>
                </div>
                
                <div class="cell">
                    <div class="widget featurette">
                        <div class="widget-header">
                            Active Members
                            <div class="pull-right">
                                <a href="members.php">view all</a>
                            </div>
                        </div>
                        <div class="widget-body">
                        <?php
                            // Display thumbnails for the top 12 active members
                            $Users = getActiveMembers(0, 12);
                            if(empty($Users)){
                                echo "<p style='text-align:center'><small>There are no members.</small></p>";
                            }else{
                                foreach($Users as $u){
                                    $usr = user_get_object($u['user_id']);
                                    echo '<a href="'.$usr->getURL().'" class="inline">'.$usr->getImageHTML(44,44).'</a>';
                                }
                            }
                        ?>
                        </div>
                    </div>
                </div>
            
            </div><!--end col-->
        
        </div><!--end row-->
    </div><!-end page->
</div><!--.container-->

<?php
// user_get_object(id)
// group_get_object(id)
// comon include
// GF User
// GF Group

$Layout->endcol();
include $gfwww.'help/help.php';
site_community_footer(array());
?>
