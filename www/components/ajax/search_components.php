<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/Group.class.php';

function getComponentNames($components) {
	$names = array();
	foreach($components as $c) {
		$names[]= $c->getName();
		$names = array_merge($names, getComponentNames($c->getSubComponents()));
	}
	return $names;
}

$group_id=getIntFromRequest('group_id');
$group=new Group($group_id);

$Return=array('error'=>false);
$Return['data']= getComponentNames($group->getComponents());
$Return['data']['length'] = sizeof($Return['data']);


echo json_encode($Return);
?>