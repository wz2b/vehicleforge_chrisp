<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Access=getStringFromRequest('data');
$cid=getIntFromRequest('cid');
$G=new Gitolite;

$Return=array('error'=>false);

$Now=time();

$Return['status']=3;

$CEM=new CEM($cid);
if (!$CEM->getRepoExists()){
    $CEM->setRepoCreationTime($Now);
    $Return['status']=2;
}

$CEM->setRepoLastUpdate($Now);

//print_r($Access);

foreach($Access as $i){
    $Permissions='';
    $Remove=(!$i[1] && !$i[2]);

    if ($Remove){
        $G->removeAllPermissions($i[0],$cid);
    }else{
        if ($i[1])
            $Permissions.='R';

        if ($i[2])
            $Permissions.='W';

        if ($i[3])
            $Permissions.='+';

        //echo $i[0].'='.$Permissions;

        if ($Permissions){
            if (!$G->setPermissions(intval($i[0]), $cid, $Permissions)){
                $Return['error']=true;
                $Return['msg']=$G->getErrorMessage();
            }
        }
    }
}

echo json_encode($Return);
?>