<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
/*require_once $gfcommon.'include/CEM.class.php';*/

$cem_id=getIntFromRequest('cem_id');
$group_id=getIntFromRequest('group_id');
$tag_id=getIntFromRequest('tag_id');

$Return=array();
$Return['error'] = false;

$CEM=new CEM($cem_id);
if (!$CEM->detachTag($tag_id)){
    $Return['error']=true;
    $Return['msg']=$CEM->getErrorMessage();
}

echo json_encode($Return);
?>