<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'Amazon/SQS.class.php';
require_once $gfcommon.'include/CEMMessages.class.php';
require_once $gfcommon.'stomp-php-1.0.0/Stomp.php';

$QueueURL=getStringFromRequest('queue_url');
$RunnableID=getIntFromRequest('runnable_id');
$RunningID=getIntFromRequest('running_id');

$useSQS = false;

$Return=array('error'=>false);

if ($useSQS == true) {
	$SQS=new SQS;
	//Receive new messages from queue
	if ($Messages=$SQS->getMessages($QueueURL)){
		$Return['messages']=array();
	
		//Loop through received messages
		foreach($Messages as $i){
			//Delete the message in the queue
			$SQS->deleteMessage($QueueURL, $i->getReceiptHandle());
	
			$Body=json_decode($i->getBody());
			$Return['messages'][]=json_encode($Body);
	
			//Log the message body in the DB
			CEMMessages::create($i->getBody(),$RunningID);
		}
	}else{
		//$Return['error']=true;
		$Return['msg']=$SQS->getErrorMessage();
	}
}
else {
	$con = new Stomp($vf_messageque_server);
	try {
		$con->setReadTimeout(5, 0);
		// connect
		$con->connect();
		$con->subscribe($QueueURL);
			
		// receive all messages from the queue
		while ( $msg = $con->readFrame() ) {
			$Body=json_decode($msg->body);
			$Return['messages'][]=json_encode($Body);
	
			//Log the message body in the DB
			CEMMessages::create($msg->body,$RunningID);
			// mark the message as received in the queue
			$con->ack($msg);
		}
		$con->unsubscribe($QueueURL);
		
	} catch (StompException $e) {
		$Return['msg']= $e->getMessage();
	}
	// disconnect
	$con->disconnect();
}

//Return all message data to page
echo json_encode($Return);
?>