<?php
require_once('env.inc.php');
require_once $gfcommon . 'pre.php';
require_once 'community/community_util.php';

$HTML->addStylesheet('marketplace/marketplace.css');
$HTML->addStylesheet('community/community_style.css');
site_header(array('title'=>'GEForge: Search'));
?>
<!--   
<ul id="tab" class="nav nav-tabs" style="margin:0;">
  <li class="active">
  	<a href="components.php">
  		<strong>Components</strong>
  	</a>
  </li>
  
	<li>
		<a href="services.php">
			<strong>Services</strong>
		</a>
	</li>
</ul>   
-->
<?php 
	$Layout->col(12,true,true,true);
  echo "<h2 style=color:#1589FF;>Search GE Forge</h2>"; 
  echo "<h4>What are you seeking?</h4>";        
	$Layout->endcol();
	
	$Layout->col(3);
?>

<!--
<br/>
<div class="row">
	<div class="span3">
-->
		<div class="well">
			<form id="search" method="get">
				<input type="text" name="q" style="width:80%; margin: 0 auto;" class="search-query" placeholder="Search..."/>
       </form>
		</div>
<!-- 	</div>     -->
<!-- 	<div class="span8">      -->
<?php $Layout->endcol()->col(9); ?>
		<div id="componentResultSummary"></div>    
		<br/><ul class="thumbnails" id="componentResultList"></ul>
		<div id="serviceResultSummary"></div>    
		<br/><ul class="thumbnails" id="serviceResultList"></ul>
		<div id="projectResultSummary"></div>    
		<br/><ul class="thumbnails" id="projectResultList"></ul>
		<div id="userResultSummary"></div>    
		<br/><ul class="thumbnails" id="userResultList"></ul>
<!-- 	</div> -->
<!-- </div>  -->

<?php $Layout->endcol(true); 
$HTML->footer(array());
?>



<?php 
global $url;
global $port;
global $path;
$solrComponentClient = new SolrComponentClient($url, $port, $path);
$solrComponentClient->writeJavaScriptInclude();
$solrComponentClient->writeModal();
$solrServiceClient = new SolrServiceClient($url, $port, $path);
$solrServiceClient->writeJavaScriptInclude();
$solrServiceClient->writeModal();
$solrUserClient = new SolrUserClient($url, $port, $path);
$solrUserClient->writeJavaScriptInclude();
$solrUserClient->writeModal();
$solrProjectClient = new SolrProjectClient($url, $port, $path);
$solrProjectClient->writeJavaScriptInclude();
$solrProjectClient->writeModal();
?>

<!--BEGIN script-->
<script>
  // initialization
  $(document).ready( function() {
      // return search results
      //searchComponents();
      searchAll();      
    });


function searchAll() {
  searchComponents("componentResultList","componentResultSummary");
  searchServices("serviceResultList","serviceResultSummary");
  searchUsers("userResultList","userResultSummary");
  searchProjects("projectResultList","projectResultSummary");
  //searchUsers();
}	

</script>