<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

$JS='$(function(){
	var imgWidth=0,
        imgHeight=0;

	$("#image_upload_form").ajaxForm({
        success: function(data) {
            data=$.parseJSON(data);
            if (data.error===false){
				$("#preview").attr("src",data.path);
				$("#temp_image").attr("src",data.path);		
		
				imgWidth=$("#temp_image").width();
				imgHeight=$("#temp_image").height();		
				$("#step1").slideUp();
				$("#step2").slideDown();
				preload(data.path);
				$("#temp_image").Jcrop({
					aspectRatio: 1,
					onChange: showPreview,
					onSelect: updateCoords,
					boxHeight:300					
				});
			}
			else {
				alert("Error submitting: " + data.msg);
			}
        },
		error: function(err) {
			alert("Error submitting: " + err);
		}
    });		

    function updateCoords(c){
        $("#x").val(c.x);
        $("#y").val(c.y);
        $("#w").val(c.w);
        $("#h").val(c.h);
    }

    function showPreview(coords){
        var rx = 100 / coords.w,
            ry = 100 / coords.h;

        $("#preview").css({
            width: Math.round(rx * imgWidth) + "px",
            height: Math.round(ry * imgHeight) + "px",
            marginLeft: "-" + Math.round(rx * coords.x) + "px",
            marginTop: "-" + Math.round(ry * coords.y) + "px"
        });
    }

    function getImgSize(src){
        $("<img/>",{
            "src":src
        }).hide().appendTo("body").load(function(){
            return {w: this.width, h: this.height};
        });
    }

	
		
    function preload(src){
		var img=$("<img/>",{
			"src":src
		}).hide().appendTo("body");

		var t=setInterval(function(){
			if (img[0].complete===true){
				clearInterval(t);
				imgWidth=img.width(),
				imgHeight=img.height();
				img.remove();
			}
		},100);
    }

    $("#go_crop").click(function(){
        $.ajax({
            url         : "ajax/crop.php",
            type        : "post",
            dataType    : "json",
            data        : {
                x       : $("#x").val(),
                y       : $("#y").val(),
                w       : $("#w").val(),
                h       : $("#h").val(),
                file    : $("#temp_image").attr("src")
            }
        }).done(function(data){
            if (data.error===false){
				$.post("ajax/update_profile_image.php",{img:data.image},function(){
					window.location="/my/";});
            }
        }).fail(function(){alert("Failed to call ajax/crop.php")});
    });
});';
add_js($JS);

use_javascript('jcrop/jquery.Jcrop.min.js');
use_javascript('jquery.form.js');

use_stylesheet('/js/jcrop/jquery.Jcrop.css');

site_user_header(array('title'=>'Change Profile Image'));
$Layout->col(12,true);
?>
<h1>Change Profile Image</h1>
<div id="step1">
	<h2>1) Upload Image</h2>
	<form id="image_upload_form" action="ajax/image_upload.php" enctype="multipart/form-data" method="post">
		<input id="image_file" type="file" name="image_file" />
		<input type="submit" class="btn btn-primary" value="Upload" />
	</form>
</div>
<div id="step2" class="hidden">
	<h2>2) Crop Image</h2>

    <div style="float:left">
        Tip: Click and drag to select an area
        <img id="temp_image"/> 
       
        <input type="hidden" id="x" name="x" value=""/>
        <input type="hidden" id="y" name="y" value="" />
        <input type="hidden" id="w" name="w" value=""/>
        <input type="hidden" id="h" name="h" value=""/>
        <button id="go_crop" class="btn btn-primary">Crop Image</button>     
        
    </div>
    <div style="float:right">
        <h3>Preview</h3>
        <div style="width:100px;height:100px;overflow:hidden">
            <img id="preview" />
        </div>
    </div>
</div>
<div id="step3" class="hidden">
	<h2>3) Success!</h2>
	Your profile image is now changed!
</div>
<?php
$Layout->endcol();
site_user_footer();
?>