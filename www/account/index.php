<?php
/**
 * User account main page - show settings with means to change them
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) Franck Villaume
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/timezones.php';
require_once $gfwww.'include/FormGen.php';
require_once $gfcommon.'include/TextMessage.class.php';
//$FormGen=new FormGen();

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));

session_require_login () ;

// get global users vars
$u =& user_get_object(user_getid());
if (!$u || !is_object($u)) {
    exit_error(_('Could Not Get User'));
} elseif ($u->isError()) {
    exit_error($u->getErrorMessage(),'my');
}

if (getStringFromRequest('submit')) {
	if (!form_key_is_valid(getStringFromRequest('form_key'))) {
		exit_form_double_submit('my');
	}


	$firstname = getStringFromRequest('firstname');
	$lastname = getStringFromRequest('lastname');
	$language = getIntFromRequest('language');
	$timezone = getStringFromRequest('timezone');
	$theme_id = getIntFromRequest('theme_id');
	$ccode = getStringFromRequest('ccode');
	$address = getStringFromRequest('address');
	$address2 = getStringFromRequest('address2');
	$phone = getStringFromRequest('phone');
	$fax = getStringFromRequest('fax');
	$title = getStringFromRequest('title');
	$jabber_address = getStringFromRequest('jabber');
	$jabber_only = getStringFromRequest('jabber');
	$mail_site = getStringFromRequest('mail_site');
	$mail_va = getStringFromRequest('mail_va');
	$remember_user = getStringFromRequest('remember_user');
	$use_ratings = getStringFromRequest('use_ratings');
	$about_me=getStringFromRequest('about_me');
	$cell_gateway=getIntFromRequest('gateway_id');

	$image=null;
/*
	if (isset($_FILES['image']) && $_FILES['image']['name']!=""){
		$ext=end(explode('.', $_FILES['image']['name']));
		$target_path = GF_IMAGE_STORE."profile_images/".$u->getUnixName().'/profile_image.'.$ext;

		if(move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
			//echo "The file ".  basename( $_FILES['uploadedfile']['name'])." has been uploaded";
			$image='profile_image.'.$ext;
		} else{
			$HTML->setError("There was an error uploading the file, please try again!");
		}

	}
*/

/*
//needs security audit
	if ($remember_user) {
		// set cookie, expire in 3 months
		setcookie("sf_user_hash",$u->getID().'_'.substr($u->getMD5Passwd(),0,16),time()+90*24*60*60,'/');
	} else {
		// remove cookie
		setcookie("sf_user_hash",'',0,'/');
	}
*/
	// Refresh page if language or theme changed
	$refresh = ($language != $u->getLanguage() || $theme_id != $u->getThemeID());

	if (!$u->update($firstname, $lastname, $language, $timezone, $mail_site, $mail_va, $use_ratings,
		$jabber_address,$jabber_only,$theme_id,$address,$address2,$phone,$fax,$title,$ccode,$about_me,$image,$cell_gateway)) {		
		form_release_key(getStringFromRequest('form_key'));
		$error_msg = $u->getErrorMessage();
		$refresh_url = '/account/?error_msg='.urlencode($error_msg);
	} else {
		$feedback = _('Updated');
		$refresh_url = '/account/?feedback='.urlencode($feedback);
	}

	if ($refresh) {
		session_redirect($refresh_url);
	}
}

use_stylesheet('/themes/gforge/css/widget.css');
/*

$JS='$(function(){
    $("#test_text_message").click(function(){
        $.post("ajax/test_text.php",{uid:$(this).data("user_id")});
    });
});';
add_js($JS);
*/

site_user_header(array('title'=>_('Account Maintenance')));

$Layout->col(9,true);

// top nav menu
$HTML->tertmenu_add('Account Settings','/account/');
$HTML->tertmenu_add('Public Profile',$u->getURL());
if(forge_get_config('use_people'))
    $HTML->tertmenu_add('Edit Skills','/people/editprofile.php');
$HTML->tertmenu_add('Edit Servers','/account/servers.php');
$HTML->tertmenu_add('Edit Public Keys','/account/publickeys.php');
echo $HTML->tertiary_menu(0);

$LanguageOptions=array();
$Languages=util_get_languages();
foreach($Languages as $i)
    $LanguageOptions[$i['language_id']]=$i['name'];

$TZOptions=array();
global $TZs;
foreach($TZs as $i)
    $TZOptions[$i]=$i;

$CCodeOptions=array();
$CCodes=util_get_country_codes();
foreach($CCodes as $i)
    $CCodeOptions[$i['ccode']]=$i['country_name'];

/*
$GatewayOptions=array();
$Gateways=util_get_cell_gateways();
foreach($Gateways as $i)
    $GatewayOptions[$i['gateway_id']]=$i['name'];
*/

$Form=new BsForm;
echo $Form->init('/account/','post',array(
/*         'enctype'=>'multipart/form-data', */
        'class'=>'form-horizontal'
    ))
    ->head('Account Settings')
    ->group('Join Date',new Custom(date('g:ia M d, Y',$u->getAddDate())))
    ->group('User ID',new Custom($u->getID()))
    ->group('Login name',new Custom($u->getUnixName()))
    ->group('First Name',new Text(array(
        'name'=>'firstname',
        'value'=>$u->getFirstName()
    )))
    ->group('Last Name',new Text(array(
        'name'=>'lastname',
        'value'=>$u->getLastName()
    )))
    ->group('Language',new Dropdown(
        $LanguageOptions,
        $u->getLanguage(),
        array(
            'name'=>'language',
            'autocomplete'=>false
        )
    ))
    ->group('Timezone',new Dropdown(
        $TZOptions,
        $u->getTimeZone(),
        array(
            'name'=>'timezone',
            'autocomplete'=>false
        )
    ))
    ->group('Country',new Dropdown(
        $CCodeOptions,
        $u->getCountryCode(),
        array(
            'name'=>'ccode',
            'autocomplete'=>false
        )
    ))
    ->group('Email',new Custom($u->getEmail().' <a href="change_email.php">['._('Change Email Address').']</a>'))
    /*
->group('Cell Phone',
        new Text(array(
            'name'=>'phone',
            'value'=>$u->getPhone()
        )),
        new Dropdown(
            $GatewayOptions,
            $u->getGateway(),
            array(
                'autocomplete'=>false,
                'name'=>'gateway_id'
            )
        ),
        new Custom('<a href="javascript:void(0)" id="test_text_message" data-user_id="'.$u->getID().'">[Send test message]</a>')
    )
*/
    ->group('About Me',new Textarea(
        $u->getAbout(),
        array(
            'name'=>'about_me'
        )
    ))
    ->group('Profile Image',new Custom($u->getImageHTML(80,80)))
    ->hidden_group(
        new Hidden(array(
            'name'=>'address',
            'value'=>$u->getAddress()
        )),
        new Hidden(array(
                'name'=>'fax',
                'value'=>$u->getFax()
        )),
        new Hidden(array(
                'name'=>'title',
                'value'=>$u->getTitle()
        )),
        new Hidden(array(
                'name'=>'form_key',
                'value'=>form_generate_key()
        )),
        new Hidden(array(
                'name'=>'theme_id',
                'value'=>1
        ))
    )
    ->actions(
        new Submit('Submit',array(
            'name'=>'submit',
            'value'=>1
        )),
        new Reset()
    )
    ->render();

/*
if (forge_get_config('use_jabber')) {
	$FormGen->input('text',_('Jabber Address'),array('name'=>'jabber_address','value'=>$u->getJabberAddress()))
		->insert_input('<input type="checkbox" name="jabber_only" value="1" '.(($u->getJabberOnly()) ? 'checked="CHECKED"' : '' ).' />'._('Send auto-generated notices only to my Jabber address'),'');
}

$FormGen->insert_input('<input type="checkbox" name="mail_site" value="1"'.($u->getMailingsPrefs('site')?' checked="checked"':'').'/>'._('Receive Email about Site Updates <i>(Very low traffic and includes security notices. Highly Recommended.)</i>'),'')
	->insert_input('<input type="checkbox"  name="mail_va" value="1"'.($u->getMailingsPrefs('va')?' checked="checked"':'').'/>'._('Receive additional community mailings. <i>(Low traffic.)</i>'),'');
/*
<div>
<input type="checkbox"  name="remember_user" value="1"<?php
	if ($sf_user_hash) print " checked=\"checked\""; ?> />
<?php printf(_('"Remember me". <i>(Allows to access your <a href="%s">personal page</a> without being logged in. You will still need to login explicitly before making any changes.)</i>'),util_make_url ('/my/'));
echo "\n</div>";


if (forge_get_config('use_ratings')) {
	$FormGen->insert_input('<input type="checkbox"  name="use_ratings" value="1"'.($u->usesRatings()?' checked="checked"':'').'/>Participate in peer ratings. <i>(Allows you to rate other users as well as to be rated by others. Information is available on your <a href="'.util_make_url_u ($u->getUnixName(),$u->getId()).'">user page</a> if you have chosen to participate.)</i>','');
}

/* each hook emits a complete table row
$hookParams['user']= user_get_object(user_getid());
if (getStringFromRequest('submit')) {//if this is set, then the user has issued an Update
	plugin_hook("userisactivecheckboxpost", $hookParams);
} else {
	plugin_hook("userisactivecheckbox", $hookParams);
}


// ############################### Shell Account

if (($u->getUnixStatus() == 'A') && (forge_get_config('use_shell'))) {
	$Keys=explode("\n", $u->getAuthorizedKeys());
	$Keys=($Keys[0])?sizeof($Keys):'0';
	$FormGen->insert('<br /><br />')
		->insert_input('<strong>'.$u->getUnixBox().'</strong>',_('Shell box'))
		->insert_input('<strong>'.$Keys.'</strong>',_('SSH Shared Authorized Keys'))
		->insert_input(util_make_link ("account/editsshkeys.php",_('Edit Keys')),'');
}


$FormGen->insert('<br />')
	->input('submit','',array('class'=>'btn primary','name'=>'submit','value'=>_('Update')))
	->input('reset','',array('class'=>'btn','name'=>'reset','value'=>_('Reset Changes')))
	->display();*/

$Layout->endcol()->col(3);
echo $HTML->widget_list('More Settings',array(
    '<a href="change_pw.php">Change Password</a>',
    '<a href="change_profile_image.php">Change Profile Image</a>',
));
$Layout->endcol();
site_user_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>