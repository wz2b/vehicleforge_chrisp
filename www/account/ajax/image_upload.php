<?
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';

$Return=array('error'=>false);
$FileData=new FileData;
if ($FileData->load('image_file')){
    $FileUploader=new FileUploader(false);
    if (!$FileUploader->upload($FileData, '/usr/share/gforge/www/images/temp_profile_images/'.$FileData->get('name'), array('jpg','jpeg'))){
        $Return['error']=true;
        $Return['msg']=$FileUploader->getErrorMessage();
    }else{
        $Return['path']='/images/temp_profile_images/'.$FileData->get('name');
    }
}

echo json_encode($Return);
?>