<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$KeyID=getStringFromRequest('keyid');

$Return=array('error'=>false);

if (!PubKey::removeKey($KeyID)){
    $Return['error']=true;
}

echo json_encode($Return);
?>