<?php
/**
 * Change user's password
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) Franck Villaume - Capgemini
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/account.php';

session_require_login () ;

$u =& user_get_object(user_getid());
if (!$u || !is_object($u)) {
	exit_error(_('Could Not Get User'),'my');
} elseif ($u->isError()) {
	exit_error($u->getErrorMessage(),'my');
}

if (getStringFromRequest('old_passwd')) {
    $form_key=getStringFromRequest('form_key');

    if (!form_key_is_valid($form_key)) {
		exit_form_double_submit('my');
	}

	$old_passwd = getStringFromRequest('old_passwd');
	$passwd = getStringFromRequest('passwd');
	$passwd2 = getStringFromRequest('passwd2');

	/*if ($u->getMD5Passwd() != md5($old_passwd)) {
		form_release_key(getStringFromRequest('form_key'));
		exit_error(_('Old password is incorrect'),'my');
	}*/

    $KADM=new kadmin;
    if (!$KADM->testLogin(session_get_user()->getUnixName(), $old_passwd)){
        form_release_key($form_key);
        $error_msg=_('Old password is incorrect');
    }else{
        if (strlen($passwd)<6) {
            form_release_key($form_key);
            exit_error(_('You must supply valid password (at least 6 chars)'),'my');
        }else{
            if ($passwd != $passwd2) {
                form_release_key($form_key);
                exit_error(_('New passwords do not match.'),'my');
            }else{
                if (!$u->setPasswd($passwd)) {
                    form_release_key($form_key);
                    exit_error(_('Could not change password: ').$u->getErrorMessage(),'my');
                }else{
                    $feedback=_('Your password has been changed');
                }
            }
        }
    }
}

// Show change form
site_user_header(array('title'=>_('Change Password')));
$Layout->col(12,true);
$Form=new BsForm();
echo $Form->init('change_pw.php','post',array('class'=>'form-horizontal'))
    ->head('Change Password')
    ->group('Old Password',
        new Hidden(array(
            'value'=>form_generate_key(),
            'name'=>'form_key'
        )),
        new Password(array(
            'name'=>'old_passwd',
            'autocomplete'=>false
        ))
    )
    ->group('New Password',
        new Password(array(
            'name'=>'passwd',
            'autocomplete'=>false
        ))
    )
    ->group('Repeat New Password',
        new Password(array(
            'name'=>'passwd2',
            'autocomplete'=>false
        ))
    )
    ->actions(
        new Submit('Change Password')
    )
    ->render();

$Layout->endcol();
site_user_footer(array());

?>
