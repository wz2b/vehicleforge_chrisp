<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

site_user_header(array('title'=>'Service Run History'));
$Layout->col(12,true);

$FinishedModels=session_get_user()->getFinishedModels(true);
$Table=new BsTable(array('class'=>'table table-striped'));
$Table->head(array('Time', 'Service', 'Actions'));

foreach($FinishedModels as $i){
    $Interface=new DOMEInterface($i->getInterfaceID());
    $CEM=new CEM($Interface->getCEMID());
    $Table->col(date('M j, Y g:ia',$i->getTimeStarted()))
        ->col($Interface->getName())
        ->col('<a href="'.Router::route('/services/?group_id='.$CEM->getGroupID().'&diid='.$Interface->getID().'&runid='.$i->getID()).'">View Results</a>');
}

echo $Table->render();

$Layout->endcol();
site_user_footer();
?>