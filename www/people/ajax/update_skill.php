<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$SkillID=getIntFromRequest('skill_id');
$Name=getStringFromRequest('name');
$Keywords=json_decode(getStringFromRequest('keywords'));
$ExpLevel=getIntFromRequest('exp_level');
$Return=array('error'=>false);

$Skill=new UserSkill($SkillID);
//var_dump($Keywords);
if (!$Skill->update($Name, $Keywords, $ExpLevel)){
    $Return['error']=true;
}

echo json_encode($Return);
?>