<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 9:55 AM 12/2/11
 */

require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$SiteActivity=new SiteActivity();
$ActivityID=getIntFromRequest('aid');

echo json_encode($SiteActivity->formatData(SiteActivity::getSingle($ActivityID)));
?>