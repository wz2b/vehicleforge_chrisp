<?php
require('../../common/Solarium/Autoloader.php');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of vfSearch
 *
 * @author 501985140
 */
class vfSearch {
  const COMPONENTS = "components";
  const USERS = "users";
  const PROJECTS = "projects";
  const SERVICES = "services";

    private $client;
    private $fieldNameMap;
    function __construct(){
    }

    function searchComponents( $searchTarget, $limit){
      return $this->searchAnd( $searchTarget, $limit, self::COMPONENTS);
    }

    function searchServices( $searchTarget, $limit){
      return $this->searchAnd( $searchTarget, $limit, self::COMPONENTS);
    }

    function search( $searchTarget, $limit, $core){
        Solarium_Autoloader::register();
        
        $config
        = array(
            'adapteroptions'=>
            array(
                  'host' => 'ip-10-38-90-140.ec2.internal',
                  'port' => 8080,
                  'path' => '/solr/',
                  'core' => $core
                ));

        // create a client instance
        $this->client = new Solarium_Client($config);
	

        $queryString = $searchTarget;
	//	$altQueryString = "*:*";
	//	if( $searchTarget == ""){
	  //	  $queryString = "user_name:*";
	//	} else {
      //	  $searchTerm = strtok( $searchTarget, " ");
	//	  while ( $searchTerm !== false){
	//            $queryString .=  $searchTerm;
	//            $searchTerm = strtok( " ");
	//            if( $searchTerm !== false){
	//                $queryString .= (" " . $operator . " ");
	//            }
	//	  }
	//	}
	//	echo "Core: " . $core . "<br />\n";
	//	echo "QueryString: " . $queryString . "<br />\n";
	//        echo "AltQueryString: " . $altQueryString . "<br />\n";
        $query=$this->client->createSelect();
	$dismax = $query->getDisMax();
	$dismax->setQueryParser( 'edismax');
        $query->setQuery( $queryString);
	//	$dismax->setQueryAlternative( $altQueryString);
	$query->setRows( $limit);
	$resArray = array();
	//	echo "query set, sending...<br />\n";
	try {
	  $resultSet = $this->client->select($query);
	  //	  echo "query done<br />\n";
	  //	  echo "num found: " . $resultSet->getNumFound() . "<br />";
	  foreach( $resultSet as $document){
	    foreach( $document as $field => $value){
	      $docArray[$field] = $value;
	    }
	    $resArray[] = $docArray;
	  }
        } catch ( Solarium_Exception $e){
	  echo $e ;
	}
	return $resArray;
    }

    function resultArrayToHTML( $resArray){
      $fieldNameMap = array();
      $fieldNameMap["group_name"] = "Group Name";
      $fieldNameMap["unix_group_name"] = "Unix Group Name";
      $fieldNameMap["short_description"] = "Short Description";
      $fieldNameMap["is_public_b"] = "Is Public";
      $fieldNameMap["homepage"] = "Homepage";
      $fieldNameMap["score"] = "Score";
      $fieldNameMap["id"] = "ID";
      $fieldNameMap["tag_name"] = "Tag Name";
      $fieldNameMap["user_name"] = "User Name";
      $fieldNameMap["realname"] = "Real Name";
      $fieldNameMap["skill"] = "Skill";
      $fieldNameMap["component_name"] = "Name";
      $fieldNameMap["group_id"] = "Group Id";
      $fieldNameMap["tag_name"] = "Tag Name";
      $fieldNameMap["category_name"] = "Category Name";

      $type = "";

      $html = "";
      foreach( $resArray as $document){
	$html .= "<table class='searchResult'>\n";
	foreach( $document as $field => $value){
	  if( is_array( $value)){
	    // handle multi value results
	    foreach( $value as $v){
	      $html .= "<tr><td class='field'>";
	      $html .= $fieldNameMap[$field];

	      $html .="</td><td class='value'>";
	      $html .= $v;
	      $html .= "</td></tr>\n";
	    }
	  } else {
	      $html .= "<tr><td class='field'>";
	      $html .= $fieldNameMap[$field];

	      $html .="</td><td class='value'>";
	      $html .= $value;
	      $html .= "</td></tr>\n";
	  }
	  if( $field == "unix_group_name"){
	    $type = "project";
	  } else if( $field == "user_name"){
	    $type = "user";
	  } else if( $field == "group_id"){
	    $type = "component";
	  }
	}
	$html .= "<td class='field'>Link</td><td class='value'>";
	$html .= "<a href='";
	$link = "";
	if( $type == "component"){
	  $link = "/components/?group_id=" . $document["group_id"]
	    . "&cid=" . $document["id"];
	} else if( $type == "project"){
	  $link = "/groups/" . $document["unix_group_name"];
	} else if( $type == "user"){
	  $link = "/users/" . $document["user_name"];
	}
	$html .=  $link . "'>"  . $link;
	$html .=  "</a></td></tr>";
	$html .= "</table>\n<br />\n";
	}
      return $html;
      }
}
?>
