$(function(){
	var raty_id=0;
	$(".raty").each(function(){
		var $me=$(this);
		if (!$me.attr("id")){
			$me.attr("id","custom_raty_"+raty_id);
			raty_id++;
		}
		$(this).raty({
			readOnly: true,
			start: $me.data("value"),
			halfShow: true,
			half: true,
			path: "/js/raty/img/"
		});
	});
});