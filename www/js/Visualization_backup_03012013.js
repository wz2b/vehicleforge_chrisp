//<script>

/* Graph Visualization - 
 * ** These variables will typically be the same across any graph **
 * margin: Defines the dimensions surrounding a graph
 * width:  Sets the width of the graph in dimensions
 * height:  Sets the height of the graph dimensions
 * x:  Sets the width of the x axis
 * y:  Sets the height of the y axis
 * xAxis:  Sets the Scale for the xAxis
 * yAxis:  Sets the Scale for the yAxis
 * type: Input the Type of Graph.  This variable will set the 
 * 		 graph to instantiate.
 * legend:  Sets the Legend for a Graph
 * div:  
 * 
 *  Member Functions
 *  makeLineGraph() - Creates a Line Graph
 *  makeBarGraph() - Creates a Bar Graph
 *  makeScatterGraph() - Creates a Scatter Graph
 *  getType() - Returns the type of Graph
 *  
 */

/**
 * Graph - Creates a D3 Graph Object
 */

(function(){
	
	modelViz = {
			version : "0.1.0",
			graph : {
				margin : {},
				width : 0, 
				height: 0, 
				plotType : "",
				axisType : "",
				xValueNames : [],
				yValueNames : [],
				xAggregateArray : [],
				yAggregateArray : [],
				graphData : []
			}
	}
	
	modelViz.lineGraph = function() {
		var graph = runLineVisualization();
		return graph;
	};	
	modelViz.scatterGraph = function() {
		var graph = runScatterVisualization();
		// var graph = scatterPlotExample();
		return graph;
	};
	
	// New structure for plotting Graphs
	modelViz.plotGraph = function() {
		var graph = plotGraph();
		return graph;
	};
	
	modelViz.saveGraph = function () {
		var success = saveGraph();
		return success;
	};
	
	// Helper Functions for populating the visualization tool
	modelViz.populateSelectors = function() {
		var selectors = populateSelectors();
		return selectors;
	};
	
	modelViz.createJSONData = function() {
		var jsonData = createJSONData();
		return jsonData;
	};
	
	function saveGraph() {
		var jsonData = createJSONData();
		alert(jsonData);
		
		// Insert saving code here
		alert("Done Saving");
		return jsonData;
	}

	/**
	 * A function used to sort data (x,y) data points by their x values
	 */
	function sortFunction(a, b){
		return (a.x - b.x);
		}

	/**
	 * A function used to plot a line graph.  (Deprecated)
	 */
	function runLineVisualization() {

	var margin = {top:20, right: 20, bottom: 30, left: 50},
	    width = 500 - margin.left - margin.right,
	    height = 250 - margin.top - margin.bottom;

	// Persist the settings for the graph
	modelViz.graph.margin = margin;
	modelViz.graph.width = width;
	modelViz.graph.height = height;
	
	
	var x,y;
	//.range([0, width]);
	//.range([height, 0]);

	// This needs to change.  We do not want to pull any
	// information directly from the form.  Working
	// to become a standalone module.
	
	if (modelViz.graph.axisType === "log") {
		x = d3.scale.log().range([0, width]);
		y = d3.scale.log().range([height, 0]);
	} else {
		x = d3.scale.linear().range([0, width]);
		y = d3.scale.linear().range([height, 0]);
	}


	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");

	var color = d3.scale.category10();
	
	var line = d3.svg.line()
	.interpolate("basis")
	    .x(function(d) { return x(d.x); })
	    .y(function(d) { return y(d.y); });

	    
	var svg = d3.select("#visual").append("svg:svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .attr( "id", "svgGraph")
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	
	/*
	 * Not sure what to set the X and Y axis names to in 
	 * the graph.  setting to the very first values selected
	 */
	var xName = modelViz.graph.xValueNames[0];
	var yName = modelViz.graph.yValueNames[0];
	var xValueNames = modelViz.graph.xValueNames;
	var yValueNames = modelViz.graph.yValueNames;
	// Assumes that there is one set of values to garner
	var xArray = getValuesFromSelector(xValueNames[0]);
	var yArrayConcatenated = [];
	var plotData = [];
	// Here is where you want to loop to create the plotData
	
	for (var i = 0, yLength = yValueNames.length; i < yLength; i++ ) {
		var yArray = getValuesFromSelector(yValueNames[i]);
		yArrayConcatenated = yArrayConcatenated.concat(yArray);
		// Very important to ensure that the x and y values are aligned properly 
		// for plotting the data
		if (xArray.length != yArray.length) {
			alert("X and Y Array Values need to be the same length!")
			return;
		}
		
		// Populate plotData array with pairs for plotting
		// But first, initialize to an empty array
		plotData[i] = [];
		for (var j = 0, xLength = xArray.length; j < xLength; j++) {
			// Define an empty object to push x,y pairs
			 plotData[i].push( { x: xArray[j], y: yArray[j]})
		}

		plotData[i].sort(sortFunction)	
	}
	

	// Persist the Settings!
	modelViz.graph.graphData = plotData;
	modelViz.graph.yAggregateArray = yArrayConcatenated;
	// Formats the data to output to D3

	 x.domain(d3.extent(xArray, function(d) { return d; })).nice();
	 
	 // For the y Domain, need to concatenate the yArray Values 
	 // And then get the domain
	 
	 y.domain(d3.extent(yArrayConcatenated, function(d) { return d; })).nice();

	  // This is assuming the same xNames for plotting data

	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	      .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .text(xName);

	  // This is assuming the same yNames for plotting data
	  // Need to change
	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text(yName);

	  var lineValues = svg.selectAll(".data")
	     .data(plotData)
	     .enter().append("g")
	      .attr("class", "line");

	  lineValues.append("path")
      .attr("class", "line")
      .attr("d", function(d) { return line(d); })
      .style("stroke", function(d, i) { return color(i); });
	  
	  var legend = svg.selectAll(".legend")
      .data(yValueNames)
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 10)
      .attr("height", 10)
      .style("fill", function (d, i) { return color(i);});

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d, i) {return d;});

	}

	function runScatterVisualization() {

		var margin = {top: 20, right: 20, bottom: 30, left: 40},
	    width = 500 - margin.left - margin.right,
	    height = 250 - margin.top - margin.bottom;

		// Need to programatically change the scale to the 
		// log, linear, etc. scales
		
		// Persist the settings
		modelViz.graph.margin = margin;
		modelViz.graph.width = width;
		modelViz.graph.height = height;
		
		var x,y;
	    //.range([0, width]);
	    //.range([height, 0]);

		if (modelViz.graph.axisType === "log") {
			x = d3.scale.log().range([0, width]);
			y = d3.scale.log().range([height, 0]);
		} else {
			x = d3.scale.linear().range([0, width]);
			y = d3.scale.linear().range([height, 0]);
		}
		
		//x.range([0, width]);
		//y.range([height, 0]);
		

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");

	var svg = d3.select("#visual").append("svg:svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .attr("id", "svgGraph")
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	/*
	 * Not sure what to set the X and Y axis names to in 
	 * the graph.  setting to the very first values selected
	 */
	var xName = modelViz.graph.xValueNames[0];
	var yName = modelViz.graph.yValueNames[0];
	var xValueNames = modelViz.graph.xValueNames;
	var yValueNames = modelViz.graph.yValueNames;
	// Assumes that there is one set of values to garner
	var xArray = getValuesFromSelector(xValueNames[0]);
	var yArrayConcatenated = [];
	var plotData = [];
	// Here is where you want to loop to create the plotData
	
	for (var i = 0, yLength = yValueNames.length; i < yLength; i++ ) {
		var yArray = getValuesFromSelector(yValueNames[i]);
		yArrayConcatenated = yArrayConcatenated.concat(yArray);
		// Very important to ensure that the x and y values are aligned properly 
		// for plotting the data
		if (xArray.length != yArray.length) {
			alert("X and Y Array Values need to be the same length!")
			return;
		}
		
		// Populate plotData array with pairs for plotting
		// But first, initialize to an empty array
		plotData[i] = [];
		for (var j = 0, xLength = xArray.length; j < xLength; j++) {
			// Define an empty object to push x,y pairs
			 plotData[i].push( { x: xArray[j], y: yArray[j]})
		}

		plotData[i].sort(sortFunction)	
	}
	

	// Persist the Settings!
	modelViz.graph.graphData = plotData;
	modelViz.graph.yAggregateArray = yArrayConcatenated;
	
	// Formats the data to output to D3

	 x.domain(d3.extent(xArray, function(d) { return d; })).nice();
	 
	 // For the y Domain, need to concatenate the yArray Values 
	 // And then get the domain
	 
	 y.domain(d3.extent(yArrayConcatenated, function(d) { return d; })).nice();

	  // This is assuming the same xNames for plotting data

	 
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .text(xName);

	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text(yName)
	      
	      svg.selectAll(".series")
	      .data(plotData)
	    .enter().append("g")
	      .attr("class", "series")
	      .style("fill", function(d, i) { return color(i); })
	    .selectAll(".point")
	      .data(function(d) { return d; })
	    .enter().append("circle")
	      .attr("class", "point")
	      .attr("r", 4.5)
	      .attr("cx", function(d) { return x(d.x); })
	      .attr("cy", function(d) { return y(d.y); })
	      .on("click", function() {
	    		 window.open("http://www.ge.com");
	    		 })
	  	  .append("svg:title")
	  	  .text(function(d) { return "(" + d.x + "," + d.y + ")"; });
	// Add the key name to the text
	      // Also, programmatically, replace www.ge.com with a link
	      // to something in the database.
	      
	      /*
	  for (var k = 0, yLength = yValueNames.length; k < yLength; k++ ) {
	  svg.selectAll(".dot")
	      .data(plotData[k])
	    .enter().append("svg:circle")
	      .attr("class", "dot")
	      .attr("r", 3.5)
	      .attr("cx", function(d){return x(d.x);})
	      .attr("cy", function(d){return y(d.y);})
	      .style("fill", "blue")
	      .on('mouseover', function() {
	    	  d3.select(this).style("fill","red")
	    	  })
	      .on('mouseout', function() {
	    	  d3.select(this).style("fill","blue")
	    	  })
	    	 .on("click", function() {
	    		 window.open("http://www.ge.com");
	    	 })
	  	  .append("svg:title")
	  	  .text(function(d) { return "(" + d.x + "," + d.y + ")"; });
	  	  
	  }
	  */
	  var legend = svg.selectAll(".legend")
	      .data(yValueNames)
	    .enter().append("g")
	      .attr("class", "legend")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

	  legend.append("rect")
	      .attr("x", width - 18)
	      .attr("width", 10)
	      .attr("height", 10)
	      .style("fill", function (d, i) {return color(i);});

	  legend.append("text")
	      .attr("x", width - 24)
	      .attr("y", 9)
	      .attr("dy", ".35em")
	      .style("text-anchor", "end")
	      .text(function(d, i) {return d});
	}
	
	/**
	 * This function is used to send the JSON Data back to a visualization tool.  (Deprecated)
	 */
	function createJSONData() {
		return JSON.stringify(modelViz.graph);
	}

	function populateSelectors() {
		
		// Get the Label names 
		var numLabels = [];
		$("#outputs label").each( function(index, v) {
			if ($(this).attr("title")=="Matrix" || $(this).attr("title")=="Vector" || $(this).attr("title")=="Array") {
				numLabels.push($(this).html());
			}
		});
		
		// Get the X and Y Selectors and set them to the values in numLabels array
		$.each(numLabels, function(key, value) {   
		     $('#selectY')
		         .append($("<option></option>")
		         .attr("value",value)
		         .text(value)); 
		     $('#selectX')
		     	.append($("<option></option>")
	         	.attr("value",value)
	         	.text(value)); 
		});
	}

	function getArrayFromSelectorID(id) {
		var selectedValues = [];    
		$(id +" :selected").each(function(){
		        selectedValues.push($(this).val()); 
		    });    
		return selectedValues;
		
	}
	
	function getValuesFromSelector(selector) {
		var flattenedData = [], unflattenedData = [];
		$("input:text").each(function(){
			if ($(this).attr("id") == selector){
				unflattenedData = eval($(this).val());
			}
		});
		
		flattenedData = unflattenedData;
		if (unflattenedData[0] instanceof Array) {
			flattenedData = unflattenedData.reduce(function(a, b) {return a.concat(b);});
		} 
		return flattenedData
		
	}
	
	function scatterPlotExample() {
		var margin = {top: 20, right: 20, bottom: 30, left: 40},
	    width = 960 - margin.left - margin.right,
	    height = 500 - margin.top - margin.bottom;

	var x = d3.scale.linear()
	    .range([0, width]);

	var y = d3.scale.linear()
	    .range([height, 0]);

	var z = d3.scale.category10();

	var svg = d3.select("#visual").append("svg:svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	d3.csv("dataScatter2.csv", function(data) {

	  // Compute the series names ("y1", "y2", etc.) from the loaded CSV.
	  var seriesNames = d3.keys(data[0])
	      .filter(function(d) { return d !== "x"; })
	      .sort();

	  // Map the data to an array of arrays of {x, y} tuples.
	  var series = seriesNames.map(function(series) {
	    return data.map(function(d) {
	      return {x: +d.x, y: +d[series]};
	    });
	  });

	  // Compute the scales� domains.
	  x.domain(d3.extent(d3.merge(series), function(d) { return d.x; })).nice();
	  y.domain(d3.extent(d3.merge(series), function(d) { return d.y; })).nice();

	  // Add the x-axis.
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(d3.svg.axis().scale(x).orient("bottom"));

	  // Add the y-axis.
	  svg.append("g")
	      .attr("class", "y axis")
	      .call(d3.svg.axis().scale(y).orient("left"));

	  // Add the points!
	  svg.selectAll(".series")
	      .data(series)
	    .enter().append("g")
	      .attr("class", "series")
	      .style("fill", function(d, i) { return z(i); })
	    .selectAll(".point")
	      .data(function(d) { return d; })
	    .enter().append("circle")
	      .attr("class", "point")
	      .attr("r", 4.5)
	      .attr("cx", function(d) { return x(d.x); })
	      .attr("cy", function(d) { return y(d.y); });
	});
	}
	
})();

