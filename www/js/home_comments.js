$(function(){
	var $home_comment_submit=$("#home_comment_submit"),
	$home_comment_text=$("#home_comment_text"),
	$c_edit=$(".c_edit"),
	$edit_cancel=$(".edit_cancel"),
	$edit_save=$(".edit_save"),
	$c_delete=$(".c_delete");

	$home_comment_submit.click(function(){
		$.getJSON('/ajax/home_post.php',{'rid':$home_comment_submit.data('rid'),'tid':$home_comment_submit.data('tid'),'comment':$home_comment_text.val()},function(msg){
			if ($("#no_comments").length)$("#no_comments").remove();

			var $comment=$('<div class="hidden newcomment">'+msg.comment+'</div>');
			$("#home_comments").prepend($comment);
			$comment.slideDown().removeClass('hidden');
			$home_comment_text.val('');
		});
	});

	$document.on("click", "c_edit", function(){
		//var cid=$(this).parent().parent().parent().parent().attr('id').substr(2);

		var $text=$(this).parent().siblings(".message").children('p').hide(),
		content=$text.text();

		$(this).parent().parent().parent().append('<div class="comment_editing"><textarea rows="3" cols="38" class="new_content">'+content+'</textarea><input type="submit" value="Save" class="edit_save" /><input type="submit" value="Cancel" class="edit_cancel" /></div>');
	});

	$(document).on("click", "edit_cancel", function(){
		$(this).parent().siblings('.text').show();
		$(this).parent().remove();
	});

	$(document).on("click", "edit_save", function(){
		var content=$(this).siblings(".new_content").val(),
		cid=$(this).parent().parent().parent().attr('id').substr(2);
		$(this).parent().siblings(".text").text(content).show();

		$.getJSON('/ajax/home_save.php',{'cid':cid,'new':content},function(msg){

		});

		$(this).parent().remove();
	});

	$(document).on("click", "c_delete", function(){
		var cid=$(this).parent().parent().parent().parent().attr('id').substr(2);
		$.getJSON("/ajax/home_delete.php",{'cid':cid},function(msg){
			if (msg.error==false){
				$("#c_"+cid).slideUp(function(){
					$(this).remove();
				});
			}
		});
	});
});