<?php 
/**
 * FusionForge Project Home
 *
 * Copyright 1999-2001 (c) VA Linux Systems 
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//require_once $gfwww.'news/news_utils.php';
//require_once $gfwww.'include/trove.php';
//require_once $gfwww.'include/project_summary.php';
//require_once $gfcommon.'include/tag_cloud.php';
//require_once $gfcommon.'include/HTTPRequest.class.php';
//require_once $gfcommon.'widget/WidgetLayoutManager.class.php';
//require_once $gfwww.'news/news_utils.php';

$ONPROJECTPAGE=true;

$ProjectActivity=new ProjectActivity($group_id);

use_javascript('activity_comments.js');
//use_javascript('pirobox/pirobox-1.1b.js');

//use_stylesheet('/js/pirobox/css_pirobox/style_5/style.css');
use_stylesheet('/themes/css/colorbox.css');
use_stylesheet('/themes/gforge/css/project.css');
use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('/themes/gforge/css/feed.css');

//if (!session_loggedin())exit_not_logged_in();

//session_require_perm ('project_read', $group_id);
$group = group_get_object($group_id);

$JS='$(function(){
    '.$ProjectActivity->generateActivityUpdateJS($group_id, "#activity");

if (session_loggedin()) {
	$User=user_get_object(user_getid());
	$IsMemberOfProject=$User->isMember($group);

	$JS.='
	$("#subscribe_button").click(function(){
		var $me=$(this);
		$.getJSON("/user/ajax/channel_subscribe.php",{"rid":'.$group->getID().', "tid":2},function(msg){
			if (msg.status==true){
				$me.text("STOP FOLLOWING");
			}else{
				$me.text("FOLLOW");
			}
		});
	});';

	if (!$IsMemberOfProject){
		$JS.='
	$("#join_request").click(function(){
		$("#join_request_form").dialog("open");
	});

	$("#join_request_form").dialog({
		autoOpen: false,
		modal: true,
		show: "fade",
		hide: "fade",
		minWidth: 400,
		position: ["center",100],
		buttons:{
			Send:function(){
				var $this=$(this);
				$.getJSON("/ajax/send_join_request.php",{"group_id":$("#jr_comment").data("group_id"),"comment":$("#jr_comment").val()},function(m){
					$this.dialog("close");
				});
			}
		}
	});';
	}
}

$JS.='});';
add_js($JS);

/*
//use_javascript('/scripts/prototype/prototype.js');
//use_javascript('/scripts/scriptaculous/scriptaculous.js');
//use_javascript('/scripts/codendi/Tooltip.js');
//use_javascript('/scripts/codendi/LayoutManager.js');
//use_javascript('/scripts/codendi/ReorderColumns.js');

//$request =& HTTPRequest::instance();
//$request->set('group_id',$group_id);

$params['submenu'] = '';

if (session_loggedin()) {
	$group = group_get_object($group_id);
	if (!$group || !is_object($group)) {
		exit_no_group();
	} elseif ($group->isError()) {
		exit_error($group->getErrorMessage(), 'home');
	}
	
	$perm =& $group->getPermission( session_get_user() );
	if ($perm && is_object($perm) && $perm->isAdmin()) {
		$sql = "SELECT l.* 
				FROM layouts AS l INNER JOIN owner_layouts AS o ON(l.id = o.layout_id) 
				WHERE o.owner_type = $1 
				AND o.owner_id = $2 
				AND o.is_default = 1
				";
		$res = db_query_params($sql,array('g', $group_id));
		if($res && db_numrows($res)<1) {
			$lm = new WidgetLayoutManager();
			$lm->createDefaultLayoutForProject($group_id,1);
			$res = db_query_params($sql,array('g', $group_id));
		}
		$id = db_result($res, 0 , 'id');
		/*$params['submenu'] = $HTML->_subMenu(
			array(_("Add widgets"),
				_("Customize Layout")),
			array('/widgets/widgets.php?owner=g'. $group_id .'&amp;layout_id='. $id,
				'/widgets/widgets.php?owner=g'. $group_id .'&amp;layout_id='. $id.'&amp;update=layout'));
	}
}*/

site_project_header(array('group'=>$group_id));
$Layout->col(9,true);


?>
<script src="../js/colorbox/jquery.colorbox.js"></script>
<script>
$(document).ready(function(){
    $(".group1").colorbox({rel:'group1'});
  });
</script>
<div class="cell">
	<div class="pull-left" style="margin-right:10px"><?=$group->getImageHTML(200,200)?></div>
	<?=$group->getDescription()?>
</div>

<?php
if ($group->getGalleryGroupID()){
?>
	<div class="cell">
		<h4>Gallery</h4>
		<ul class="thumbnails">
	<?php
	$DocGroup=new DocumentGroup($group->getGalleryGroupID());
	$Docs=$DocGroup->getContents($group_id,$group->getGalleryGroupID(),6);

	foreach($Docs as $Doc){
	    $URL='/image_store/project_files/'.$group->getUnixName().'/'.$DocGroup->getName().'/'.$Doc['filename'];
	    echo '<li class="span3"><a class="group1 cboxElement" href="'.$URL.'" title="'.$Doc['description'].'"><img src="'.$URL.'" class="thumbnail" width="95%" /></a></li>';
	      // echo '<li class="span3"><a href="'.$URL.'" rel="gallery" class="pirobox_gall" title=""><img src="'.$URL.'" class="thumbnail" width="95%" /></a></li>';
	}
	?>
		</ul>
		<div class="pull-right"><a href="gallery.php">View Entire Gallery</a></div>
	</div>
<?php
}
?>

<div class="cell">
	<h4 helptip-text="View recent activity from this project, including: user comments, file uploads, etc" helptip-position="top">Activity</h4>
</div>
<?php
HomeComments::displayForm($group_id, 2);
$ProjectActivity->getContent(10);


$Layout->endcol()->col(3);
$Head=_('Follow Us');
if (session_loggedin()){
	$SubMsg=($User->getSubscriptionStatus($group->getID(),2))?'STOP FOLLOWING':'FOLLOW';
	$Head=array(_('Follow Us'),util_make_link('javascript:void(0)',$SubMsg,array('id'=>'subscribe_button', 'helptip-text'=>'Follow this project to get updates about it in your dashboard. You can stop following it at any time', 'helptip-position'=>'right'),true));
}
echo $HTML->widget_list($Head,array('<img src="'.util_make_uri('/images/ic/rss_16.png').'" /> <a href="javascript:void(0)">RSS</a>',
										'<img src="'.util_make_uri('/images/ic/facebook_16.png').'" /> <a href="javascript:void(0)">Facebook</a>',
										'<img src="'.util_make_uri('/images/ic/twitter_16.png').'" /> <a href="javascript:void(0)">Twitter</a>'));

$Head=_('Our Team');
if (session_loggedin()){
	if ($IsMemberOfProject){
	
	$Head=array(_('Our Team'), '<span class="label update pull-right"><a id="invite users" href="/project/admin/?group_id='.$group->getID().'">+ INVITE USERS</a></span>');
	}
	


	else {
		$Head=array(_('Our Team'),'<a href="javascript:void(0)" id="join_request">REQUEST TO JOIN</a><div id="join_request_form" title="Send a join request">Leave a comment to the project administrator<textarea id="jr_comment" cols="50" rows="3" data-group_id="'.$group_id.'"></textarea></div>');
	}
}

$List=array();
$Admins=$group->getAdmins();
$Users=$group->getUsers();
$Seen=array();
if (count($Admins)){
	foreach($Admins as $i){
		$List[]=util_display_user($i->getUnixName(),$i->getID(),$i->getRealName()).' <span class="label pull-right">Admin</span>';
		$Seen[]=$i->getID();
	}
}

if (count($Users)){
	foreach($Users as $i){
		if (!in_array($i->getID(),$Seen))
			$List[]=util_display_user($i->getUnixName(),$i->getID(),$i->getRealName());
	}
}

echo $HTML->widget_list($Head,$List);

/*$List=$group->getSubProjects();
if (sizeof($List)){
	echo $HTML->widget_list(array(_('Subprojects'),'<a href="/graphs/relationships.php?group_id='.$group_id.'">Graph</a>'), $List);
}*/

$SimilarProjects=$group->getSimilarProjects();
if ($SimilarProjects){
	$List=array();

	foreach($SimilarProjects as $i)
		$List[]=util_make_link_g($i['unix_group_name'],$i['group_id'],$i['group_name']);

	echo $HTML->widget_list(_('Similar Projects'),$List);
}


$Layout->endcol();

/*$HTML->boxTop()->heading(_('News'),2);
require_once('www/news/news_utils.php');
echo news_show_latest($group_id,10,false);
/*<hr />';
$HTML->heading(_('Latest Releases'),2);
echo '<section>';


$unix_group_name = $project->getUnixName();
		//
		//  Members of projects can see all packages
		//  Non-members can only see public packages
		//
		$public_required = 1;
		if (session_loggedin() &&
		    (user_ismember($group_id) || user_ismember(1,'A'))) {
			$public_required = 0 ;
		}

		$res_files = db_query_params ('SELECT frs_package.package_id,frs_package.name AS package_name,frs_release.name AS release_name,frs_release.release_id AS release_id,frs_release.release_date AS release_date
			FROM frs_package,frs_release
			WHERE frs_package.package_id=frs_release.package_id
			AND frs_package.group_id=$1
			AND frs_release.status_id=1
			AND (frs_package.is_public=1 OR 1 != $2)
			ORDER BY frs_package.package_id,frs_release.release_date DESC',
			array ($group_id,
				$public_required));
		$rows_files=db_numrows($res_files);
		if (!$res_files || $rows_files < 1) {
			echo db_error();
			// No releases
			echo '<strong>'._('This Project Has Not Released Any Files').'</strong>';

		} else {
			echo '<table class="width-100p100">
		<tr class="table-header">
			<th class="align-left" scope="col">
				'._('Package').'
			</th>
			<th scope="col">
				'._('Version').'
			</th>
			<th scope="col">
				'._('Date').'
			</th>
			<th scope="col">
				'._('Notes').'
			</th>
			<th scope="col">
				'._('Monitor').'
			</th>
			<th scope="col">
				'._('Download').'
			</th>
		</tr>';
			/*
				This query actually contains ALL releases of all packages
				We will test each row and make sure the package has changed before printing the row

			for ($f=0; $f<$rows_files; $f++) {
				if (db_result($res_files,$f,'package_id')==db_result($res_files,($f-1),'package_id')) {
					//same package as last iteration - don't show this release
				} else {
					$rel_date = getdate (db_result ($res_files, $f, 'release_date'));
					$package_name = db_result($res_files, $f, 'package_name');
					$package_release = db_result($res_files,$f,'release_name');
					echo '
                        <tr class="align-center">
						<td class="align-left">
							<strong>' . $package_name . '</strong>
						</td>';
					// Releases to display
//print '<div about="" xmlns:sioc="http://rdfs.org/sioc/ns#" rel="container_of" resource="'.util_make_link ('/frs/?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files,$f,'release_id').'">';
					echo '
                        <td>'
						.$package_release.'
						</td>
						<td>'
						. $rel_date["month"] . ' ' . $rel_date["mday"] . ', ' . $rel_date["year"] .
						'</td>
						<td class="align-center">';
//echo '</div>';

					// -> notes
					// accessibility: image is a link, so alt must be unique in page => construct a unique alt
					$tmp_alt = $package_name . " - " . _('Release Notes');
					$link = '/frs/shownotes.php?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files, $f, 'release_id');
					$link_content = $HTML->getReleaseNotesPic($tmp_alt, $tmp_alt);
					echo util_make_link ($link, $link_content);
					echo '</td>
						<td class="align-center">';

					// -> monitor
					$tmp_alt = $package_name . " - " . _('Monitor this package');
					$link = '/frs/monitor.php?filemodule_id=' .  db_result($res_files,$f,'package_id') . '&amp;group_id='.$group_id.'&amp;start=1';
					$link_content = $HTML->getMonitorPic($tmp_alt, $tmp_alt);
					echo util_make_link ($link, $link_content);
					echo '</td>
						<td class="align-center">';

					// -> download
					$tmp_alt = $package_name." ".$package_release." - ". _('Download');
					$link_content = $HTML->getDownloadPic($tmp_alt, $tmp_alt);
					$t_link_anchor = $HTML->toSlug($package_name)."-".$HTML->toSlug($package_release)."-title-content";
					$link = '/frs/?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files, $f, 'release_id')."#".$t_link_anchor;
					echo util_make_link ($link, $link_content);
					echo '</td>
					</tr>';

				}
			}
			echo '</table>';
		}

echo '</section>';
$HTML->boxBottom();



$HTML->heading(_('News'),2);
echo news_show_latest($group_id,10,false);




echo '<style>ul li{list-style:none}</style>';
$Layout->col(12);
echo $HTML->boxTop();
$Size=$project->usesTracker()+$project->usesForum()+$project->usesMail()+$project->usesPm()+$project->usesSurvey()+$project->usesFTP();
$Size=round(100/$Size-(1/$Size),2);

if ($project->usesTracker()){
	echo '<div class="project_public_nav" style="width:'.$Size.'%"><div>Tracker</div>';
	$result=db_query_params ('SELECT agl.*,aca.count,aca.open_count
			FROM artifact_group_list agl
			LEFT JOIN artifact_counts_agg aca USING (group_artifact_id)
			WHERE agl.group_id=$1
			AND agl.is_public=1
			ORDER BY group_artifact_id ASC',
			array($group_id));

	$rows = db_numrows($result);

	if (!$result || $rows < 1) {
		echo "<em>"._('No public trackers available').'</em>';
	} else {
		echo "\n".'<ul class="tracker" rel="doap:bug-database">'."\n";
		for ($j = 0; $j < $rows; $j++) {
			$group_artifact_id = db_result($result, $j, 'group_artifact_id');
			$tracker_stdzd_uri = util_make_url('/tracker/cm/project/'. $project->getUnixName() .'/atid/'. $group_artifact_id);
			echo "\t".'<li about="'. $tracker_stdzd_uri . '" typeof="sioc:Container">'."\n";
			print '<span rel="http://www.w3.org/2002/07/owl#sameAs">'."\n";
			echo util_make_link ('/tracker/?atid='. $group_artifact_id . '&amp;group_id='.$group_id.'&amp;func=browse',db_result($result, $j, 'name')) . ' ' ;
			echo "</span>\n"; // /owl:sameAs
			printf(ngettext('(<strong>%1$s</strong> open / <strong>%2$s</strong> total)', '(<strong>%1$s</strong> open / <strong>%2$s</strong> total)', (int) db_result($result, $j, 'open_count')), (int) db_result($result, $j, 'open_count'), (int) db_result($result, $j, 'count'));
			echo '<br />'; //.db_result($result, $j, 'description');
			print '<span rel="sioc:has_space" resource="" ></span>'."\n";
			echo "</li>\n";
		}
		echo "</ul>\n";
	}
	echo '</div>';
}

if ($project->usesForum()) {
	echo '<div class="project_public_nav" style="width:'.$Size.'%"><div>Forum</div>';
	$messages_count = project_get_public_forum_message_count($group_id);
	$forums_count = project_get_public_forum_count($group_id);
	printf(ngettext("<strong>%d</strong> message","<strong>%d</strong> messages",$messages_count),$messages_count);
	print ' in ';
	printf(ngettext("<strong>%d</strong> forum","<strong>%d</strong> forums",$forums_count),$forums_count);
	print "</div>";
}

if ($project->usesMail()) {
	echo '<div class="project_public_nav" style="width:'.$Size.'%"><div>Mailing Lists</div>';
	$n = project_get_mail_list_count($group_id);
	printf(ngettext('<strong>%1$s</strong> public mailing list', '<strong>%1$s</strong> public mailing lists', $n), $n);
	echo "</div>";
}

if ($project->usesPm()) {
	echo '<div class="project_public_nav" style="width:'.$Size.'%"><div>Tasks</div>';
	$result = db_query_params ('SELECT * FROM project_group_list WHERE group_id=$1 AND is_public=1',array ($group_id));
	$rows = db_numrows($result);
	if (!$result || $rows < 1) {
		echo "<em>"._('No public subprojects available').'</em>';
	} else {
		echo "\n".'<ul class="task-manager" style="list-style:none">';
		for ($j = 0; $j < $rows; $j++) {
			echo "\n\t<li>";
			print util_make_link ('/pm/task.php?group_project_id='.db_result($result, $j, 'group_project_id').'&amp;group_id='.$group_id.'&amp;func=browse',db_result($result, $j, 'project_name'));
			echo '</li>' ;
		}
		echo "\n</ul>";
	}
	echo "\n</div>\n";
}

if ($project->usesSurvey()) {
	echo '<div class="project_public_nav" style="width:'.$Size.'%"><div>Surveys</div>';
	echo '<strong>'. project_get_survey_count($group_id) .'</strong> ' . _('surveys');
	echo "</div>";
}

if ($project->usesFTP()) {
	if ($project->isActive()) {
		//echo '<div class="project_public_nav" style="width:'.$Size.'%">'.util_make_link('/scm/?group_id='. $group_id,'<div>FTP</div>');
		echo '<div class="public-area-box">'."\n";

		$link_content = $HTML->getFtpPic('') . ' ' . _('Anonymous FTP Space');
		//		print '<a rel="doap:anonymous root" href="ftp://' . $project->getUnixName() . '.' . forge_get_config('web_host') . '/pub/'. $project->getUnixName() .'/">';
		print util_make_link('ftp://' . $project->getUnixName() . '.' . forge_get_config('web_host') . '/pub/'. $project->getUnixName(), $link_content, false, true);
		echo "\n</div>\n";
	}
}
echo $HTML->boxBottom();

$Layout->endcol()->col(4);

echo $HTML->boxTop('Project Description');
echo $group->getDescription();
if (session_loggedin()){
	if (!$UserCheck->isMember($group)){
		echo '<br /><br /><input type="button" id="join_request" value="Request project access" data-group="'.$group->getID().'" />';
	}
}
echo $HTML->boxBottom();

$Layout->endcol()->col(4);

echo $HTML->boxTop('Project Info');
if (forge_get_config('use_project_tags')) {
	$list_tag = list_project_tag($group_id);
	echo '<p>'.html_image('ic/tag.png'). ' ';
	if ($list_tag) {
		echo _('Tags').': '. $list_tag;
	}
	else {
		$project = group_get_object($group_id);
		if (forge_check_perm ('project_admin', $project->getID())) {
			echo '<a href="/project/admin/?group_id=' . $group_id . '" >' . _('No tag defined for this project') . '</a>.';
		}else {
			echo _('No tag defined for this project');
		}
	}
	echo "</p>";
}

if(forge_get_config('use_trove')) {
	echo '<p>'.stripslashes(trove_getcatlisting($group_id,0,1,1))."</p>";
}

$project_start_date = $project->getStartDate();
		echo '<p>'._('Registered: ').'<span property="doap:created" content="'.date('Y-m-d', $project_start_date).'">'.date(_('Y-m-d H:i'), $project_start_date)."</span></p>";

if ($project->usesStats()) {
	echo '<p>';
	$actv = db_query_params ('SELECT ranking FROM project_weekly_metric WHERE group_id=$1',array($group_id));
	if (db_numrows($actv) > 0){
		$actv_res = db_result($actv,0,"ranking");
	} else {
		$actv_res = 0;
	}
	if (!$actv_res) {
		$actv_res=0;
	}
	print sprintf (_('Activity Ranking: %d'), $actv_res)."\n";
	print '<br />'.sprintf(_('View project <a href="%1$s" >Statistics</a>'),util_make_url ('/project/stats/?group_id='.$group_id))."\n";
	if ( ($project->usesTracker() && forge_get_config('use_tracker')) || ($project->usesPm() && forge_get_config('use_pm')) ) {
		print sprintf(_(' or <a href="%1$s">Activity</a>'),util_make_url ('/project/report/?group_id='.$group_id))."\n";
	}
	print '<br />'.sprintf(_('View list of <a href="%1$s">RSS feeds</a> available for this project.'), util_make_url ('/export/rss_project.php?group_id='.$group_id)). ' ' . html_image('ic/rss.png',16,16,array())."\n";
	echo '</p>';
}

if(forge_get_config('use_people')) {
	$jobs_res = db_query_params ('SELECT name
			FROM people_job,people_job_category
			WHERE people_job.category_id=people_job_category.category_id
			AND people_job.status_id=1
			AND group_id=$1
			GROUP BY name',
			array ($group_id),
			2);
	if ($jobs_res) {
		$num=db_numrows($jobs_res);
		if ($num>0) {
			print '<p>';
			printf(
					ngettext('HELP WANTED: This project is looking for a <a href="%1$s">"%2$s"</a>.',
						'HELP WANTED: This project is looking for people to fill <a href="%1$s">several different positions</a>.',
						$num),
					util_make_url ('/people/?group_id='.$group_id),
					db_result($jobs_res,0,"name"));
			print "</p>\n";
		}
	}
}

echo $HTML->boxBottom();

$Layout->endcol()->col(4);

echo $HTML->boxTop('Project Members');
$Admins=$group->getAdmins();
$Users=$group->getUsers();
$Seen=array();
if (count($Admins)){
	echo '<p><strong>'._('Admins').'</strong><br />';
	foreach($Admins as $i){
		echo util_display_user($i->getUnixName(),$i->getID(),$i->getRealName())."<br />";
		$Seen[]=$i->getID();
	}
	echo '</p>';
}

$HasUser=false;
if (count($Users)){
	foreach($Users as $i){
		if (!in_array($i->getID(),$Seen)){
			if (!$HasUser){
				echo '<br /><p><strong>Members</strong><br />';
				$HasUser=true;
			}
			echo util_display_user($i->getUnixName(),$i->getID(),$i->getRealName())."<br />";
		}
	}
	if ($HasUser)
		echo '</p>';
}
echo $HTML->boxBottom();

$Layout->endcol();

if ($project->usesNews()){
	$Layout->col(6);
	echo $HTML->boxTop('Latest News');
	require_once('www/news/news_utils.php');
	echo news_show_latest($group_id,10,false);
	echo $HTML->boxBottom();
	$Layout->endcol();
}

if ($project->usesFRS()){
	$Layout->col(6);
	echo $HTML->boxTop('Latest File Releases');
	$unix_group_name = $project->getUnixName();
		//
		//  Members of projects can see all packages
		//  Non-members can only see public packages
		//
		$public_required = 1;
		if (session_loggedin() &&
		    (user_ismember($group_id) || user_ismember(1,'A'))) {
			$public_required = 0 ;
		}

		$res_files = db_query_params ('SELECT frs_package.package_id,frs_package.name AS package_name,frs_release.name AS release_name,frs_release.release_id AS release_id,frs_release.release_date AS release_date
			FROM frs_package,frs_release
			WHERE frs_package.package_id=frs_release.package_id
			AND frs_package.group_id=$1
			AND frs_release.status_id=1
			AND (frs_package.is_public=1 OR 1 != $2)
			ORDER BY frs_package.package_id,frs_release.release_date DESC',
			array ($group_id,
				$public_required));
		$rows_files=db_numrows($res_files);
		if (!$res_files || $rows_files < 1) {
			echo db_error();
			// No releases
			echo '<strong>'._('This Project Has Not Released Any Files').'</strong>';

		} else {
			echo '<table summary="Latest file releases" class="width-100p100">
		<tr class="table-header">
			<th class="align-left" scope="col">
				'._('Package').'
			</th>
			<th scope="col">
				'._('Version').'
			</th>
			<th scope="col">
				'._('Date').'
			</th>
			<th scope="col">
				'._('Notes').'
			</th>
			<th scope="col">
				'._('Monitor').'
			</th>
			<th scope="col">
				'._('Download').'
			</th>
		</tr>';
			/*
				This query actually contains ALL releases of all packages
				We will test each row and make sure the package has changed before printing the row

			for ($f=0; $f<$rows_files; $f++) {
				if (db_result($res_files,$f,'package_id')==db_result($res_files,($f-1),'package_id')) {
					//same package as last iteration - don't show this release
				} else {
					$rel_date = getdate (db_result ($res_files, $f, 'release_date'));
					$package_name = db_result($res_files, $f, 'package_name');
					$package_release = db_result($res_files,$f,'release_name');
					echo '
                        <tr class="align-center">
						<td class="align-left">
							<strong>' . $package_name . '</strong>
						</td>';
					// Releases to display
//print '<div about="" xmlns:sioc="http://rdfs.org/sioc/ns#" rel="container_of" resource="'.util_make_link ('/frs/?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files,$f,'release_id').'">';
					echo '
                        <td>'
						.$package_release.'
						</td>
						<td>'
						. $rel_date["month"] . ' ' . $rel_date["mday"] . ', ' . $rel_date["year"] .
						'</td>
						<td class="align-center">';
//echo '</div>';

					// -> notes
					// accessibility: image is a link, so alt must be unique in page => construct a unique alt
					$tmp_alt = $package_name . " - " . _('Release Notes');
					$link = '/frs/shownotes.php?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files, $f, 'release_id');
					$link_content = $HTML->getReleaseNotesPic($tmp_alt, $tmp_alt);
					echo util_make_link ($link, $link_content);
					echo '</td>
						<td class="align-center">';

					// -> monitor
					$tmp_alt = $package_name . " - " . _('Monitor this package');
					$link = '/frs/monitor.php?filemodule_id=' .  db_result($res_files,$f,'package_id') . '&amp;group_id='.$group_id.'&amp;start=1';
					$link_content = $HTML->getMonitorPic($tmp_alt, $tmp_alt);
					echo util_make_link ($link, $link_content);
					echo '</td>
						<td class="align-center">';

					// -> download
					$tmp_alt = $package_name." ".$package_release." - ". _('Download');
					$link_content = $HTML->getDownloadPic($tmp_alt, $tmp_alt);
					$t_link_anchor = $HTML->toSlug($package_name)."-".$HTML->toSlug($package_release)."-title-content";
					$link = '/frs/?group_id=' . $group_id . '&amp;release_id=' . db_result($res_files, $f, 'release_id')."#".$t_link_anchor;
					echo util_make_link ($link, $link_content);
					echo '</td>
					</tr>';

				}
			}
			echo '</table>';
		}


	$HTML->boxBottom();
	$Layout->endcol();
}
echo '</div></div>';
*/
include $gfwww.'help/help.php';
site_project_footer(array());

db_display_queries();
?>
