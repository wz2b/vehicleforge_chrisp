<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 3:45 PM 8/22/11
 */
 
class model {
    private $Curl, $URL, $Debug;

	function __construct($Debug=false){
		$this->URL=$GLOBALS['DOME_URL'];
		$this->Debug=$Debug;

		require_once('curl.php');
		$this->Curl=new curl();
	}

	/**
	 * arrayToObject() - Returns an object converted from an array
	 * Returns false if nothing needs to be changed
	 *
	 * @param $array
	 * @return bool|array
	 */
	function arrayToObject($array) {
		if(!is_array($array)) {
			return $array;
		}

		$object = new stdClass();
		if (is_array($array) && count($array) > 0) {
		  foreach ($array as $name=>$value) {
			 $name = strtolower(trim($name));
			 if (!empty($name)) {
				$object->$name = $this->arrayToObject($value);
			 }
		  }
		  return $object;
		}
		else {
		  return FALSE;
		}
    }

	/**
	 * error() - Gives information when an error occurs while retrieving model data
	 *
	 * @param $Msg
	 * @param $Data
	 * @return void
	 */
	private function error($Msg,$Data){
		echo $Msg.'<br />';
		var_dump($Data);
		exit;
	}

	/**
	 * get_def() - Returns the full defintion of a model from DOME
	 *
	 * @param int $ModelID
	 * @return object
	 */
	public function get_def($ModelID){
		$Model=$this->get($ModelID);

		$this->Curl->opts(Array(
	                            CURLOPT_URL=>$this->URL."getModel",
								CURLOPT_POSTFIELDS=>$this->Curl->format_data(array("data"=>$Model->model_def))
	                      ));
		$Data=json_decode($this->Curl->exec());
		if ($this->Debug){
			echo '<b>Requesting:</b> '.$this->URL.'getModel?'.$this->Curl->format_data(array("data"=>$Model->model_def)) .' from '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'<br><br>';
			echo '<b>Response:</b> ';
			print_r($Data);
			echo '<br><br>';
		}
		if (isset($Data->status) && $Data->status!="success"){
			$this->error('Error in model->get_def()',$Data);
		}
		return $Data->pkg[0];
	}

	/**
	 * get() - Returns model_id, dome_guid, model_def
	 * Returns the model_def WITHOUT parameters
	 *
	 * @param $ModelID
	 * @return object
	 */
	public function get($ModelID){
		$Model=db_query_params("SELECT * FROM models WHERE model_id=$1",array($ModelID));
		return $this->arrayToObject(db_fetch_array($Model));
	}

	/**
	 * getall() - Returns the group IDs of all ModelGroups
	 *
	 * @return array of ints
	 */
    public function getall(){
	    /*$Models=db_query_params("SELECT * FROM models",array());
	    $ModelsArray=Array();
	    while ($Model=db_fetch_array($Models)){
		    $ModelsArray[]=$this->arrayToObject($Model);
	    }
	    return $ModelsArray;*/

	    $Models=db_query_params("SELECT * FROM groups WHERE group_type=$1",array(2));
	    $ModelsArray=array();
	    while($PModel=db_fetch_array($Models)){
		    $ModelsArray[]=$PModel['group_id'];
	    }

	    return $ModelsArray;
    }

	/**
	 * getall_fromdome() - Gets all models from DOME with full model definitions
	 *
	 * @param bool $Associative
	 * @return array
	 */
	public function getall_fromdome($Associative=false){
		$this->Curl->opts(Array(CURLOPT_URL=>$this->URL."getModels"));
		$Data=$this->Curl->exec();
		if ($this->Debug){
			echo '<b>Requesting:</b> '.$this->URL.'getModels from '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'<br><br>';
			echo '<b>Response:</b> '.$Data.'<br><br>';
		}
	    $Data=json_decode($Data,$Associative);
	    if ($Data->status!="success"){
		    $this->error("Error in model->getall_fromdome(): ".$Data->message,$Data);
	    }
        return $Data->pkg;
	}

	/**
	 * run() - Runs a model through DOME and returns new outputs
	 *
	 * @param json $Data
	 * @return json
	 */
	public function run($Data){
		$this->Curl->opts(Array(
								CURLOPT_URL=>$this->URL."runModel",
			                    CURLOPT_POSTFIELDS=>$this->Curl->format_data(array("data"=>$Data))
		                  ));
		$Data=$this->Curl->exec();
		if ($this->Debug){
			echo '<b>Requesting:</b> '.$this->URL.'runModel?'.$this->Curl->format_data(array("data"=>$Data)).' from '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'<br><br>';
			echo '<b>Response:</b> '.$Data.'<br><br>';
		}
		return $Data; //Returns JSON
	}

	public function search($Name){
		//TODO: rewrite or remove, see Tom if there will be a search in DOME
		$ReturnModels=array();
		$Models=$this->getall();
		foreach($Models as $i){
			if (stripos($i->name,$Name)){
				$ReturnModels[]=$i;
			}
		}

		return $ReturnModels;
	}

	private function strNthPos($Haystack, $Needle, $Nth){
		$Count=substr_count($Haystack,$Needle);
		if ($Count<1 || $Nth > $Count) return false;
		$Len=strlen($Haystack);
		$NeedleCount=0;
		for($i=0;$i<$Len;$i++){
			if ($Haystack[$i]==$Needle){
				$NeedleCount++;
				if ($NeedleCount==$Nth)
					return $i;
			}
		}

		return false;
	}

	/**
	 * matrix_explode($String)
	 * Takes a string and formats it into an array
	 * EX: [1,2,[3,[4]],5] -> array(1,2,array(3,array(4)),5)
	 *
	 * @param string $String
	 * @return array
	 */
	private function matrix_explode($String){
		$String=str_replace('[','array(',$String);
		$String=str_replace(']',')',$String);
		$String='$Matrix='.$String.';';
		eval($String);

		return $Matrix;
	}

	/**
	 * matrix_implode($Array,$Deep)
	 * Takes an array containing a matrix and converts it to a string
	 * EX: array(1,2,array(3,array(4)),5) -> [1,2,[3,[4]],5]
	 *
	 * @param array $Array
	 * @param int $Deep
	 * @return string
	 */
	function matrix_implode($Array,$Deep=0){
		$Deep++;
		$String=($Deep==1)?'[':'';

		foreach($Array as $i){
			if (is_array($i)){
				$String.='['.$this->matrix_implode($i,$Deep).']';
			}else{
				$String.=$i;
			}

			$String.=',';
		}

		$String=substr($String,0,strlen($String)-1);
		if($Deep==1)$String.=']';

		return $String;
	}

	/**
	 * format_value() - Takes a value and converts it to the correct format
	 *
	 * @param array|string|int|float $Value
	 * @param string $Type
	 * @return array|bool|float|int|string
	 */
	public function format_value($Value,$Type){
		switch ($Type){
			case "Real":
				return floatval($Value);
				break;

			case "Vector":
				$Vector=explode(',',$Value);
				$NewVector=Array();
				foreach($Vector as $v)
					$NewVector[]=floatval($v);

				return $NewVector;
				break;

			case "VectorString":
				$String="";
				$Size=sizeof($Value);
				for($i=0;$i<$Size;$i++){
					$String.=$Value[$i].",";
					if ($i>=$Size-1)
						$String=substr($String,0,strlen($String)-1);
					else
						$String.="\n";
				}

				return $String;
				break;

			case "MatrixString":
				return $this->matrix_implode($Value);
				break;

			case "Matrix":
				return $this->matrix_explode($Value);
				break;

			case "Int":
				return intval($Value);
				break;

			default:
				echo "Unknown format type";
				return false;
		}
	}
}
?>