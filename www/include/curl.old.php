<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:48 AM 8/30/11
 */
 
class curl {
	private $Curl;

	function __construct($URL=null){
		$this->Curl=curl_init($URL);
		return $this->Curl;
	}

	function format_data($DataArray){
		$Data="";
		foreach($DataArray as $key=>$value)
			$Data.=$key.'='.$value.'&';

		$Data=substr($Data,0,strlen($Data)-1);

		return $Data;
	}

	function opts($OptionsArray){
		curl_setopt_array($this->Curl,$OptionsArray);
	}

	function exec($Return=true){
		curl_setopt($this->Curl,CURLOPT_RETURNTRANSFER,$Return);
		return curl_exec($this->Curl);
	}

	public function info(){
		$Data=curl_getinfo($this->Curl);
		echo '<b>Curl Data</b><br />:';
		foreach ($Data as $key=>$value){
			echo $key.'='.$value.'<br />';
		}
	}

	public function error(){
		echo '<b>CURL Error (#'.curl_errno($this->Curl).'): </b>'.curl_error($this->Curl);
	}

	function __destruct(){
		curl_close($this->Curl);
	}
}
?>
