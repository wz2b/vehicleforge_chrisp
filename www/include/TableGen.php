<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 3:00 PM 8/26/11
 */
 
class TableGen {
	private $Widths, $CurCol=0, $Count, $Type, $ColOpen=0, $Chain;

	function __construct($Chain=true){
		$this->Chain=$Chain;
	}

	function __destruct(){
		if ($this->ColOpen!=0)echo '<strong>TableGen(): '.$this->ColOpen.' columns open</strong>';
	}

	/**
	 * top($Columns, $Type, $Attributes)
	 * Creates the head of the table and sets the widths
	 *
	 * @param array $Columns
	 * @param string $Type
	 * @param array $Attributes
	 * @return TableGen
	 */
	function top($Columns=array(),$Type='px',$Attributes=array(),$RowHighlight=true){
		$this->Type=$Type;
		$this->Count=count($Columns);
		$Result='<div class="table';
		if ($RowHighlight)$Result.=' rowhighlight';
		$Result.='"';

		$HeadEmpty=true;
		foreach ($Columns as $i){
			if ($i[0]!=''){
				$HeadEmpty=false;
				break;
			}
		}

		foreach($Attributes as $Key=>$Value)
			$Result.=$Key.'="'.$Value.'"';
		$Result.='><div class="head';
		if ($HeadEmpty)$Result.=' hide';
		$Result.='">';

		foreach ($Columns as $i){
			$Result.='<span style="width:'.$i[1].$Type.'">'.$i[0].'</span>';
			$this->Widths[]=$i[1];
		}
		$Result.='</div><div class="body">';
		if ($this->Chain){
			echo $Result;
			return $this;
		}else{
			return $Result;
		}
	}

	/**
	 * col($Data, $Attributes)
	 * Creates a column in the table
	 * Automatically creates new rows based on table width
	 *
	 * @param string $Data
	 * @param array $Attributes
	 * @return TableGen
	 */
	function col($Data='', $Attributes=array()){
		$Result="";
		if ($this->CurCol==0)$Result.='<div class="tablerow">';
		$Result.='<span style="width:'.$this->Widths[$this->CurCol].$this->Type.'"';
		foreach($Attributes as $Key=>$Value)
			$Result.=$Key.'="'.$Value.'"';
		$Result.='>'.$Data.'</span>';
		$this->CurCol++;
		if ($this->CurCol>$this->Count-1){
			$Result.='</div>';
			$this->CurCol=0;
		}

		if ($this->Chain){
			echo $Result;
			return $this;
		}else{
			return $Result;
		}
	}

	/**
	 * bottom()
	 * Ends the table
	 * Clears table data
	 *
	 * @return TableGen
	 */
	function bottom(){
		$this->clear();
		$Result='</div></div>';

		if ($this->Chain){
			echo $Result;
			return $this;
		}else{
			return $Result;
		}
	}

	/**
	 * clear()
	 * Clears table data
	 *
	 * @return void
	 */
	private function clear(){
		$this->Widths=null;
		$this->ColOpen=0;
		$this->Count=null;
		$this->CurCol=0;
		$this->Type=null;
	}
}
?>