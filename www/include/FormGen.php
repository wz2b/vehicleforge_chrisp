<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 1:25 PM 10/3/11
 */

class FormGen {
	private $Code='', $IsAJAX=true, $SelectOpen=false, $TextareaOpen=false;

	/**
	 * build($Action, $Method)
	 * Builds the top of the form with an action and method
	 * If this function is not called, it is assumed to be an AJAX form and <form> is not added to code
	 *
	 * @param string $Action
	 * @param string $Method
	 * @return Form
	 */
	function build($Action="#",$Method="post"){
		$this->IsAJAX=false;
		$this->Code.='<form action="'.$Action.'" method="'.strtolower($Method).'" enctype="multipart/form-data">'."\n";
		return $this;
	}

	/**
	 * input($Type, $Label, $Attributes)
	 * Creates an input of type $Type with a label $Label
	 * Other attributes can be specified via the $Attributes associated array (ex: array('id'=>'SomeID'))
	 *
	 * @param string $Type
	 * @param string $Label
	 * @param array $Attributes
	 * @return Form
	 */
	function input($Type,$Label="",$Attributes=null){
		$Code='';

		//Close any open <select> or <textarea>
		if ($this->SelectOpen){
			$Code.='</select></div>';
			$this->SelectOpen=false;
		}
		if ($this->TextareaOpen){
			$Code.='</textarea></div>';
			$this->TextareaOpen=false;
		}

		//Add labels
		if ($Type!='submit' && $Type!='hidden' && $Type!='reset' && $Type!='button'){
			$ID=isset($Attributes['id'])?$Attributes['id']:'';
			$Code.=$this->get_label($Label,$ID);
		}

		//Add start input HTML
		$Code.='<';
		switch ($Type){
			case 'text':
			case 'file':
			case 'submit':
			case 'hidden':
			case 'checkbox':
			case 'reset':
			case 'password':
			case 'button':
				$Code.='input type="'.$Type.'"';
				break;

			case 'select':
				$Code.='select';
				$this->SelectOpen=true;
				break;

			case 'textarea':
				$Code.='textarea';
				$this->TextareaOpen=true;
				break;
		}

		//Add in any associated attributes
		if ($Attributes){
			foreach($Attributes as $Key=>$Value){
				if ($Key=='checked'){
					if ($Value){
						$Value='checked';
					}else{
						continue;
					}
				}else if ($Key=='disabled'){
					if ($Value){
						$Value='disabled';
					}else{
						continue;
					}
				}

				$Code.=' '.$Key.'="'.$Value.'"';
			}
		}

		//Add end input HTML
		switch ($Type){
			case 'text':
			case 'file':
			case 'submit':
			case 'hidden':
			case 'checkbox':
			case 'reset':
			case 'password':
			case 'button':
				$Code.=' />';
				break;
			
			case 'select':
			case 'textarea':
				$Code.='>';
				break;
		}

		if (!$this->SelectOpen && !$this->TextareaOpen && $Type!='submit' && $Type!='hidden' && $Type!='reset' && $Type!='button')
			$Code.='</div>';

		$this->Code.=$Code."\n";
		return $this;
	}

	/**
	 * insert($Text)
	 * Inserts $Text into the generated code
	 *
	 * @param string $Text
	 * @return Form
	 */
	function insert($Text){
		$this->Code.=$Text;
		return $this;
	}

	function insert_input($Code,$Label,$Attributes=null){
		$ID=isset($Attributes['id'])?$Attributes['id']:'';
		$this->Code.=$this->get_label($Label,$ID).$Code.'</div>';
		return $this;
	}

	private function get_label($Text,$ID=''){
		$Code='<div><label';
		if ($ID)
			$Code.=' for="'.$ID.'"';
		$Code.='>'.$Text.'</label>';

		return $Code;
	}

	/**
	 * display()
	 * Displays the generated form
	 *
	 * @return FormGen
	 */
	function display(){
		if ($this->SelectOpen){
			$this->Code.='</select>';
			$this->SelectOpen=false;
		}
		if ($this->TextareaOpen){
			$this->Code.='</textarea>';
			$this->TextareaOpen=false;
		}

		if (!$this->IsAJAX)$this->Code.='</form>';
		echo $this->Code."\n";
		return $this;
	}

	private function strToBool($String){
		return (boolean)$String;
	}

	function clear(){
		$this->Code="";
		$this->IsAJAX=true;
		$this->SelectOpen=false;
		$this->TextareaOpen=false;
	}
}
?>
