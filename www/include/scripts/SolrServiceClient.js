function makeServiceThumb(doc, resultList) {
    var group_id = -1;
    var group_img_src = '<img src="http://placehold.it/100x100&text=GEForgeService" alt="">';
    jQuery.ajax({
	    url: '/marketplace/service_group_id_decoder.php?interface_id=' + doc.id, 
		success: function(data) {
		group_id = data.group_id;
		$(html).attr('group_id', doc.group_id);
		jQuery.ajax({
			url: '/project/project_picture_decoder.php?group_id=' + group_id, 
			    success: function(data) {
			    group_img_src = data.group_img_src;
			    $(html).find('.serviceImage').html('<img src="' + group_img_src + '" width=100 height=100 alt="">');
			},
			    dataType: 'json'
			    });
	    },
		dataType: 'json'
		});
    var interface_data = $(this).data('interface_data');
    var html = $(getServiceThumbPlaceholder());
    // set attributes
    $(html).attr('interface_name', doc.interface_name);
    $(html).attr('interface_id', doc.id);
    $(html).data('interface_data', $.parseJSON(doc.interface_data)); 

    // set html
    $(html).find('.interface_name').html(doc.interface_name);
		
    // append results
    $('#' + resultList).append(html);
	  
    // add a listener
    $(html).on('click', showServiceModal);
}	

function searchServices(resultList, resultSummary) {
    query = getParameterByName("q");
    
    // execute ajax request
    $.ajax({
	    type: 'GET',
		url: "/solrSearch/searchServices.php?q=" + query,
		dataType: "json",
		success: function(json) {
		// remove old results
		$('#' + resultList).children().remove();

		// add new results
		var response = json.response, doc;
		for (var i=0; i<response.numFound; i++) {
		    doc = response.docs[i];
		    makeServiceThumb(doc, resultList);
		}
				
		// update results summary
		if (query == '') {
		    $('#' + resultSummary).html('<h4>Services</h4>' + response.numFound + ' results found.');
		} else {
		    $('#' + resultSummary).html('<h4>Services</h4>' + response.numFound + ' results found for search: "' + query + '".');
		}
	    }   
	});
}

function getServiceThumbPlaceholder() {
    var thumb = '';
    thumb = thumb + '<li class="span2">';
    thumb = thumb + '<div class="thumbnail">';
    thumb = thumb + '<center><div class="serviceImage">';
    thumb = thumb + '<img src="http://placehold.it/100x100&text=GForgeService" alt="">';
    thumb = thumb + '</div></center>';
    thumb = thumb + '<div class="caption" style="padding:5px;">';
    thumb = thumb + '<h5 class="interface_name"></h5>';
    thumb = thumb + '</div>';
    thumb = thumb + '</div>';
    thumb = thumb + '</li>';
    return thumb;
}

function showServiceModal(event) {

    var name = $(this).attr('interface_name');
    var id = $(this).attr('interface_id');
    var group_id = -1;
    console.log(id);
    /* var group_id = $(this).attr('group_id'); */
    jQuery.ajax({
	    url: '/marketplace/service_group_id_decoder.php?interface_id=' + id, 
		success: function(data) {
		group_id = data.group_id;
		$('#service_url').attr('href', '/services/?group_id=' + group_id + '&diid=' + id);
	    },
		dataType: 'json'
		});
    var interface_data = $(this).data('interface_data');

    // update modal values
    $('#service_name').html(name);
    $('#service_desc').html('Service description goes here...');
    $("#subscribe_to_service").data('interface_id',id);
		
    // write inputs
    var inputs = interface_data.inParams;
    $('#service_inputs').children().remove();
    for (i=0; i<inputs.length; i++) {
	var name = inputs[i].name;
	var type = inputs[i].type;
	$('#service_inputs').append('<li>' + name + ' [' + type + ']</li');
    }

    // write outputs
    var outputs = interface_data.outParams;
    $('#service_outputs').children().remove();
    for (i=0; i<outputs.length; i++) {
	var name = outputs[i].name;
	var type = outputs[i].type;
	$('#service_outputs').append('<li>' + name + ' [' + type + ']</li');
    }
		
    // show modal
    $('#serviceModal').modal();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null)
	return "";
    else
	return decodeURIComponent(results[1].replace(/\+/g, " "));
}
