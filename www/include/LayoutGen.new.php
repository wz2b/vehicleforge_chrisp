<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:04 AM 10/3/11
 */

class LayoutGen {
	private $ColsOpen=0, $RowsOpen=0, $Chain=true, $CurCols=0;

	function __construct($Chain=true){
		$this->Chain=$Chain;
	}

	function __destruct(){
		if ($this->ColsOpen!=0 || $this->RowsOpen!=0)
			echo '<strong>'.$this->RowsOpen.' endRow() missing, '.$this->ColsOpen.' endCol() missing</strong>';
	}

	function header($Text, $Weight=1){
		echo '<h'.$Weight.'>'.$Text.'</h'.$Weight.'>';
		return $this;
	}

	/**
	 * @param int $X
	 * @return bool|string
	 */
	private function numToCol($X){
		switch ($X){
			case 12:return'twelvecol';
			case 11:return'elevencol';
			case 10:return'tencol';
			case 9:return'ninecol';
			case 8:return'eightcol';
			case 7:return'sevencol';
			case 6:return'sixcol';
			case 5:return'fivecol';
			case 4:return'fourcol';
			case 3:return'threecol';
			case 2:return'twocol';
			case 1:return'onecol';
		}

		return false;
	}

	/**
	 * @return string
	 */
	private function row(){
		$Code='<div class="row">';
		$this->RowsOpen++;

		/*if ($this->Chain){
			echo $Code;
			return $this;
		}*/

		return $Code;
	}

	/**
	 * @param int $Size
	 * @param bool $Last
	 * @return LayoutGen|string
	 */
	function col($Size,$Last=false){
		$Code='';
		if ($this->RowsOpen==0)$Code=$this->row();

		$this->CurCols+=$Size;
		if ($Last)$this->CurCols=12;
		if ($this->CurCols>12)echo '<strong>More than 12 columns specified in this row. Create a new row or reduce columns</strong>';

		$Code.='<div class="'.$this->numToCol($Size).'';
		if ($this->CurCols==12)$Code.=' last';
		$Code.='">';
		$this->ColsOpen++;

		if ($this->Chain){
			echo $Code;
			return $this;
		}

		return $Code;
	}

	/**
	 * @return LayoutGen|string
	 */
	function endcol(){
		$Code="</div><!--end col-->\n";
		$this->ColsOpen--;

		if ($this->CurCols==12){
			$Code.=$this->endrow();
			$this->CurCols=0;
		}

		if ($this->Chain){
			echo $Code;
			return $this;
		}

		return $Code;
	}

	/**
	 * @return string
	 */
	private function endrow(){
		$Code="</div><!--end row-->\n";
		$this->RowsOpen--;

		/*if ($this->Chain){
			echo $Code;
			return $this;
		}*/

		return $Code;
	}
}
?>
