<?php
require_once 'env.inc.php';
require_once DB_CONFIG_FILE;
require_once APP_PATH.'common/include/database-pgsql.php';

function checkS3(){
    //Test the S3
    if (file_exists(GF_IMAGE_STORE.'status_check_file')){
        //echo 's3 good';
        return true;
    }else{
        //echo 's3 bad';
        return false;
    }
}

function reconnectS3(){
    passthru("sh ".APP_PATH."common/scripts/s3_reconnect");
}

function checkDB(){
    $ConnectString=pg_connectstring(DB_NAME, DB_USER, DB_PASS, DB_HOST);
    if ($Conn=pg_connect($ConnectString)){
        pg_close($Conn);
        return true;
    }else{
        return false;
    }
}
$OK=checkDB();


if (!checkS3()){
    //echo 'cannot connect once...trying again';
    reconnectS3();

    if (!checkS3()){
        //echo 'connection failed again';
        $OK=false;
    }
}

if (!$OK)
    header("HTTP/1.0 404 Not Found");
else
    echo 1;
?>