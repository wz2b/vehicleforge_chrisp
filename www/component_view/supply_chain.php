<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

// Include css/less for marketplace //TODO: include this in main CSS
$HTML->addStylesheet('/themes/gforge/css/marketplace.less');

// Create the site header
site_header(array('title'=>'Component View – Supply Chain'));
?>

<div class="span3">

	<?php 
	$HTML->boxTop();
	$HTML->heading("Organizing Principle", 3); 
	?>
	<style type="text/css" style="display:none;">.selected {font-weight:bold;}</style>
	<ul style="list-style-type:disc; padding-left:10px;">
		<li><a href="./classic.php">Classic</a></li>
		<li><a href="./volume.php">Volume/3D</a></li>
		<li><a href="./bill_of_materials.php">Bill of Materials</a></li>
		<li><a href="./services.php">Service Dependency</a></li>
		<li><a href="./personnel.php">Personnel</a></li>
		<li><a class="selected" href="./supply_chain.php">Supply Chain</a></li>										
	</ul>
	
	<?php $HTML->boxBottom(); ?>

</div>

<div class="span9">

<?php 
$HTML->boxTop();
$HTML->heading("Powertrain System", 3);
?>
<p>Under Development.</p>

<?php $HTML->boxBottom(); ?>

</div>

<?php
// Create site footer
site_footer(array());
?>