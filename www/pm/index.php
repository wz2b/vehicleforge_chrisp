<?php
/**
 * Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'pm/include/ProjectGroupHTML.class.php';

use_stylesheet('/themes/css/tablesorter/style.css');
use_javascript('jquery.tablesorter.min.js');

$group_id = getIntFromRequest('group_id');

session_require_perm ('project_read', $group_id) ;

if (!$group_id) {
	exit_no_group();
}

$g = group_get_object($group_id);
if (!$g || !is_object($g)) {
	exit_no_group();
} elseif ($g->isError()) {
	exit_error($g->getErrorMessage(),'pm');
}

$pgf = new ProjectGroupFactory($g);
if (!$pgf || !is_object($pgf)) {
	exit_error(_('Could Not Get Factory'),'pm');
} elseif ($pgf->isError()) {
	exit_error($pgf->getErrorMessage(),'pm');
}

$pg_arr = $pgf->getProjectGroups();
if ($pg_arr && $pgf->isError()) {
	exit_error($pgf->getErrorMessage(),'pm');
}

pm_header(array());
$Layout->col(12,true);
echo $HTML->tertiary_menu();

plugin_hook("blocks", "tasks index");

if (count($pg_arr) < 1 || $pg_arr == false) {
	echo '<p class="warning_msg">'._('No Subprojects Found').'</p>';
	echo '<p>'._('No subprojects have been set up, or you cannot view them.').'</p>';
	echo '<p class="important">'._('The Admin for this project will have to set up subprojects using the admin page.').'</p>';
} else {
	echo '<p>'._('Choose a Subproject and you can browse/edit/add tasks to it.').'</p>';

    $Table=new BsTable(array('class'=>'table table-striped tablesorter'));
    $Table->head(array('ID','Subproject Name', 'Description', 'Open', 'Total'));

	for ($j = 0; $j < count($pg_arr); $j++) {
		if (!is_object($pg_arr[$j])) {
			//just skip it
		} elseif ($pg_arr[$j]->isError()) {
			echo $pg_arr[$j]->getErrorMessage();
		} else {
            $Table->col($pg_arr[$j]->getID())
                ->col('<a href="/pm/task.php?group_project_id='. $pg_arr[$j]->getID().'&amp;group_id='.$group_id.'&amp;func=browse">'.$pg_arr[$j]->getName().'</a>')
                ->col($pg_arr[$j]->getDescription())
                ->col($pg_arr[$j]->getOpenCount())
                ->col($pg_arr[$j]->getTotalCount());
		}
	}
	echo $HTML->listTableBottom();

    echo $Table->render();
}

$Layout->endcol();
pm_footer(array());

?>
