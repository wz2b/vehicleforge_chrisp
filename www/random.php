<?php
require_once 'env.inc.php';
require_once APP_PATH.'common/include/pre.php';



if (isset($_REQUEST['length'])){
    $FileData=new FileData;
    $Uploader=new FileUploader(true);
    $Curl=new curl;
    $FileMap=array();
    $ParamData=array();
    $ParamType=array();

    foreach($_REQUEST as $key=>$val){
       if ($FileData->load('file-'.$key)){
           $Destination='/tmp/test_files/'.$FileData->get('name');

           if ($Uploader->upload($FileData, $Destination)){
               //$FileMap[$key]="file[".$Curl->addFile($Destination)."]";
               $ParamData[$key]='"file['.$Curl->addFile($Destination).']"';
               $ParamType[$key]="File";
           }
       }else{
           $ParamData[$key]=$val;
           $ParamType[$key]="Real";
       }
    }

    $Curl->opts(array(
        CURLOPT_VERBOSE     => 1,
        //CURLOPT_URL         => 'http://ec2-50-17-110-63.compute-1.amazonaws.com:8080/DOMEApiServicesJZK/runModel',
        CURLOPT_URL         => 'http://localhost/random2.php',
        CURLOPT_POST        => true,
        CURLINFO_HEADER_OUT => true
    ));

    $Curl->data(array(
        'data'          => '{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"'.$ParamType['width'].'","unit":"foot ( U.S.)","value":'.$ParamData['width'].',"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"'.$ParamType['length'].'","unit":"foot ( U.S.)","value":'.$ParamData['length'].',"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}'
    ));

    var_dump($Curl->exec(true));

    //$Curl->info();

    //var_dump($FileMap);
}
?>

<form action="random.php" method="post" enctype="multipart/form-data">
    Param 1 <input type="text" name="width" /><br />
    File for Param 1 <input type="file" name="file-width" /><br />
    Param 2 <input type="text" name="length" /><br />
    File for Param 2 <input type="file" name="file-length" /><br />
   <input type="submit" />
</form>