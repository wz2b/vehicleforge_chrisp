package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.*;

import java.util.concurrent.TimeUnit;
//import java.awt.event.KeyEvent;
//import java.awt.Robot;

/**
 * @author Amine Chigani
 * 
 * Unit tests for navigating through the Marketplace section of the site
 * clicking each link and checking whether the correct page is loaded. 
 */
public class MarketplaceNavigationTest extends BaseTest {
    
	@Test
    public void testMarketplacePageNavigation() throws Exception {

        testForgeLogin();
 
        // Click on the Marketplace tab after login.
        driver.findElement(By.linkText("Marketplace")).click();
        if (!driver.getCurrentUrl().endsWith("marketplace/services.php")) {
            fail("Doesn't go to the Marketplace page when the Marketplace tab is clicked.");
        }
        
        //Test the sub-menu of Marketplace page
        driver.findElement(By.linkText("Components")).click();
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/components.php"));
        
        driver.findElement(By.linkText("Services")).click();
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/services.php"));
        
	}
    
    @Test
    public void testMarketplaceComponentsSearchTab() throws Exception{
        testForgeLogin();
        
        
        //Test Search Members of Services tab within Marketplace page
        driver.findElement(By.linkText("Marketplace")).click();
        driver.findElement(By.linkText("Components")).click();
        WebElement element = driver.findElement(By.cssSelector("#search > input[name=\"q\"]"));
        element.click();
        element.sendKeys("ttt" + "\n");
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/components.php?q=ttt"));
       
           
    }
    
    
     @Test
     public void testMarketplaceServicesSearchTab() throws Exception{
        testForgeLogin();
        
        
        //Test Search Members of Services tab within Marketplace page
        driver.findElement(By.linkText("Marketplace")).click();
        driver.findElement(By.linkText("Services")).click();
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/services.php"));
        WebElement element = driver.findElement(By.cssSelector("#search > input[name=\"q\"]"));
        element.click();
        element.sendKeys("demo" + "\n");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        assertTrue(driver.getCurrentUrl().endsWith("marketplace/services.php?q=demo"));
     }
    
    
    
}
