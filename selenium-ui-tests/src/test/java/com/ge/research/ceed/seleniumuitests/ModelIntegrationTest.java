package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.ui.Select;

public class ModelIntegrationTest extends BaseTest {


	// @Test
	  public void testModelIntegration() throws Exception {
		testForgeAdminLogin();
		driver.get(baseUrl + "/index2.php");
		driver.findElement(By.linkText("Projects")).click();
		driver.findElement(By.linkText("dummy")).click();
		driver.findElement(By.linkText("Services")).click();
		driver.findElement(By.linkText("Integrate Services")).click();
		assertTrue(driver.getCurrentUrl().indexOf("services/integrate.php?group_id=") != -1);		
		
		WebElement element = driver.findElement(By.xpath(".//*[@id='service-list']/div[1]"));
		
		//WebElement target = driver.findElement(By.id("integration-panel"));
		WebElement target = driver.findElement(By.id("integration-panel"));
		
		//System.out.println(target.getLocation());
		(new Actions(driver)).dragAndDrop(element, target).build().perform();
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[1]/a")).click();
		
		/*Actions builder = new Actions(driver);

		Action dragAndDrop = builder.clickAndHold(element)
		   .moveToElement(target)
		   .release(target)
		   .build();

		dragAndDrop.perform();*/
		
		//assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*SpecimenWidth[\\s\\S]*$"));
		//driver.findElement(By.xpath(".//*[@id='integration-panel']/div/div/div[2]/ul/li")).click();

		/*WebElement element2 = driver.findElement(By.xpath(".//*[@id='service_nav']/li[4]/a"));
		WebElement target2 = driver.findElement(By.id("integration-panel"));
		(new Actions(driver)).dragAndDrop(element2, target2).build().perform();
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[1]/a")).click();
		
		
		driver.findElement(By.xpath(".//*[@id='integration-panel']/div[2]/div/div[1]/ul/li")).click();
		driver.findElement(By.xpath(".//*[@id='save-relation']")).click();

		
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[2]/a")).click();
		assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*BetaFactor[\\s\\S]*$"));*/
	  }
	

}
