package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

public class AddServiceAndRunTest extends BaseTest {


	 @Test
	  public void testAddDOMEServer() throws Exception {
		testForgeAdminLogin();
	    driver.get(baseUrl + "/my/");
	    driver.findElement(By.linkText("Edit")).click();
	    String DOMEServerName = driver.findElement(By.cssSelector("input.url")).getAttribute("value");
	    driver.findElement(By.linkText("Forge Admin")).click();
	    driver.findElement(By.linkText("Log Out")).click();
	    
	    testForgeLogin();
	    
	    driver.findElement(By.linkText("Register Server")).click();
	        
	    driver.findElement(By.name("server_name")).click();
	    driver.findElement(By.name("server_name")).clear();
	    System.out.println(TestUtils.ran);
	    driver.findElement(By.name("server_name")).sendKeys("MyServer - " + TestUtils.ran);
	    driver.findElement(By.name("server_url")).clear();
	    driver.findElement(By.name("server_url")).sendKeys(DOMEServerName);
	    driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
	    assertEquals("Your server has been registered", driver.findElement(By.cssSelector("p")).getText());
	  }
	
	@Test
	public void testAddComponentToFractureMechanics() throws Exception {
		testForgeLogin();
		driver.get(baseUrl + "/index2.php");
		driver.findElement(By.linkText("Projects")).click();
		driver.findElement(By.linkText("Test Demo")).click();
		driver.findElement(By.linkText("Components")).click();
		driver.findElement(By.id("addComponent")).click();
		driver.findElement(By.id("cname")).click();
		driver.findElement(By.id("cname")).clear();
		driver.findElement(By.id("cname")).sendKeys("component - " + TestUtils.ran);
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		driver.findElement(By.id("edit_details")).click();
		driver.findElement(By.id("cem_desc")).click();
		driver.findElement(By.id("cem_desc")).clear();
		driver.findElement(By.id("cem_desc")).sendKeys("This is the component's description. Number " + TestUtils.ran);
		driver.findElement(By.id("addTagField")).clear();
		driver.findElement(By.id("addTagField")).sendKeys("tag1");
		driver.findElement(By.cssSelector("input.btn.btn-small")).click();
		driver.findElement(By.id("addTagField")).clear();
		driver.findElement(By.id("addTagField")).sendKeys("tag2");
		driver.findElement(By.cssSelector("input.btn.btn-small")).click();
		driver.findElement(By.id("edit_details")).click();

		//need assert
	}

	@Test
	public void testAddServiceToFractureMechanicsComponent() throws Exception {
		testForgeLogin();
		driver.get(baseUrl + "/index2.php");
		driver.findElement(By.linkText("Projects")).click();
		driver.findElement(By.linkText("Test Demo")).click();
		driver.findElement(By.linkText("Components")).click();
		driver.findElement(By.linkText("component - " + TestUtils.ran)).click();

		driver.findElement(By.xpath("(//a[contains(text(),'Services')])[2]")).click();
		driver.findElement(By.linkText("Add Service")).click();
		Select serverSelect = new Select(driver.findElement(By.id("server")));
		System.out.println(serverSelect.toString());
		serverSelect.selectByVisibleText("MyServer - " + TestUtils.ran);
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if ("Fracture-Mechanics".equals(driver.findElement(By.linkText("Fracture-Mechanics")).getText())) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		driver.findElement(By.linkText("Fracture-Mechanics")).click();
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if ("Alpha".equals(driver.findElement(By.linkText("Alpha")).getText())) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		driver.findElement(By.linkText("Alpha")).click();
		for (int second = 0;; second++) {
			if (second >= 60) fail("timeout");
			try { if ("Default Interface".equals(driver.findElement(By.linkText("Default Interface")).getText())) break; } catch (Exception e) {}
			Thread.sleep(1000);
		}

		driver.findElement(By.linkText("Default Interface")).click();
		Thread.sleep(1000);


		driver.findElement(By.id("alias")).click();
		driver.findElement(By.id("alias")).clear();
		driver.findElement(By.id("alias")).sendKeys("Alpha - " + TestUtils.ran);
		Thread.sleep(1000);
		driver.findElement(By.id("register")).click();
		//service is registered  (TEST WORKS TO THIS POINT)

		Thread.sleep(1000);
		driver.findElement(By.linkText("Services")).click();
		driver.findElement(By.linkText("Alpha - " + TestUtils.ran)).click();
		//		  String tmp = driver.getPageSource();
		//		    System.out.println(tmp);

		driver.findElement(By.linkText("Settings")).click();


		Thread.sleep(1000);
		driver.findElement(By.id("desc")).click();
		driver.findElement(By.id("desc")).clear();
		driver.findElement(By.id("desc")).sendKeys("This is the service " + TestUtils.ran+" description.");
		driver.findElement(By.id("settings_save")).click();
		driver.findElement(By.linkText("Description")).click();
		driver.findElement(By.linkText("Run Service")).click();
		driver.findElement(By.id("input_d9f30f37-d800-1004-8f53-704dbfababa8")).click();
		driver.findElement(By.id("input_d9f30f37-d800-1004-8f53-704dbfababa8")).clear();
		driver.findElement(By.id("input_d9f30f37-d800-1004-8f53-704dbfababa8")).sendKeys("3");
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		//	    for (int second = 0;; second++) {
		//	    	if (second >= 60) fail("timeout");
		//	    	try { if ("Finished".equals(driver.findElement(By.id("status")).getText())) break; } catch (Exception e) {}
		//	    	Thread.sleep(1000);
		//	    }
		//
		//	    driver.findElement(By.id("inputsForm")).click();
		//	    driver.findElement(By.cssSelector("input.btn")).click();
	}

}
