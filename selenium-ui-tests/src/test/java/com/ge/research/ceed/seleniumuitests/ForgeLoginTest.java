package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ForgeLoginTest extends BaseTest{
   
   @Test
   public void testForgeAdminLogin() throws Exception{
      
      if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
        else{
           driver.manage().deleteAllCookies();
        }
        
        
      // Click on Log In in the page.
       WebElement element = driver.findElement(By.id("site_login"));
       element.click();   
       
       element = driver.findElement(By.name("form_loginname"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_USER);
       
       element = driver.findElement(By.name("form_pw"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_PASS);
       
       element = driver.findElement(By.className("btn"));
       element.submit();
       
       
       assertTrue(driver.getPageSource().indexOf("Your account does not exist.") == -1);
       assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") == -1);
       assertTrue(driver.getCurrentUrl().endsWith("my/"));
        
   
   }


}
