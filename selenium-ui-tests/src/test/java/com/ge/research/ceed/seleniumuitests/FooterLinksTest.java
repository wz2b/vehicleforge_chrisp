package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.fail;
import org.junit.Test;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Amine Chigani
 * 
 * Unit tests for navigating through the site footer
 * clicking each link and checking whether the correct page is loaded. 
 */
public class FooterLinksTest extends BaseTest {
    
	@Test
    public void testFooterLinks() throws Exception {

        testForgeLogin();
 
        //Click on the Community tab to start from a page other than Home. 
        driver.findElement(By.linkText("Community")).click();
        if (!driver.getCurrentUrl().endsWith("community/")) {
            fail("Doesn't go to the Community page when the Community tab is clicked.");
        }
        
        // Click on footer links and check whether the correct page is loaded.
        driver.findElement(By.linkText("About Us")).click();
        if (!driver.getCurrentUrl().endsWith("index2.php")) {
            fail("Doesn't go to the About Us section in the Home page.");
        }
        //Test "Latest News" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Latest News")).click();
        if (!driver.getCurrentUrl().endsWith("community/latest.php")) {
            fail("Doesn't go to the Latest News section in the Home page.");
        }
         //Test "Members" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Members")).click();
        if (!driver.getCurrentUrl().endsWith("community/members.php")) {
            fail("Doesn't go to the Members section in the Community page.");
        }
         //Test "Discussion Board" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Discussion Board")).click();
        if (!driver.getCurrentUrl().endsWith("community/boards.php")) {
            fail("Doesn't go to the Discussion Board section in the Community page.");
        }
         //Test "Resources" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Resources")).click();
        if (!driver.getCurrentUrl().endsWith("community/resources.php")) {
        	fail("Doesn't go to the Resources section in the Community page.");
        }
         //Test "Components" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Components")).click();
        if (!driver.getCurrentUrl().endsWith("marketplace/components.php")) {
            fail("Doesn't go to the Marketplace page.");
        }
         //Test "Services" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Services")).click();
        if (!driver.getCurrentUrl().endsWith("marketplace/services.php")) {
            fail("Doesn't go to the Services section in the Marketplace page.");
        }
         //Test "Projects" link text and check whether the correct page is loaded.
        driver.findElement(By.linkText("Projects")).click();
        if (!driver.getCurrentUrl().endsWith("softwaremap/full_list.php")) {
            fail("Doesn't go to the Projects page.");
        }
                
         testFAQs();
        testContactUs();
        testFacebook();
        testTwitter();  
	}
    
    //Test "FAQs" link text and check whether the correct page is loaded.
	public void testFAQs() throws Exception{
		//get initial window handle
		driver.get(baseUrl);
		String oldTab = driver.getWindowHandle();       
        driver.findElement(By.linkText("FAQs")).click();
        List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
        //remove the old tab
        browserTabs.remove(oldTab);
        //switch to new tab
        driver.switchTo().window(browserTabs .get(0));
        //check is it correct page opened or not (e.g. check page's title)
        if (!driver.getCurrentUrl().endsWith("dmdii.uilabs.org/membership/faq")) {
            fail("Doesn't go to the FAQs page.");
        }              
        //then close tab and get back
        driver.close();
        driver.switchTo().window(oldTab);
        if (!driver.getCurrentUrl().endsWith("index2.php")) {
            fail("Doesn't go to the Home page.");
        }     
     }
	
	
	//Test "Contact Us" link text and check whether the correct page is loaded.
	public void testContactUs() throws Exception{
		//get initial window handle
		driver.get(baseUrl);
		String oldTab = driver.getWindowHandle();       
		driver.findElement(By.linkText("Contact Us")).click();
        List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
        //remove the old tab
        browserTabs.remove(oldTab);
        //switch to new tab
        driver.switchTo().window(browserTabs .get(0));
        //check is it correct page opened or not (e.g. check page's title)
        if (!driver.getCurrentUrl().endsWith("dmdii.uilabs.org/connect")) {
            fail("Doesn't go to the Contact Us page.");
        }            
        //then close tab and get back
        driver.close();
        driver.switchTo().window(oldTab);
        if (!driver.getCurrentUrl().endsWith("index2.php")) {
            fail("Doesn't go to the Home page.");
        }		
	}
	
	//Test "Facebook" link text and check whether the correct page is loaded.
	public void testFacebook() throws Exception{
		//get initial window handle
		driver.get(baseUrl);
		String oldTab = driver.getWindowHandle();       
		driver.findElement(By.linkText("Facebook")).click();
        List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
        //remove the old tab
        browserTabs.remove(oldTab);
        //switch to new tab
        driver.switchTo().window(browserTabs .get(0));
        //check is it correct page opened or not (e.g. check page's title)
        if (driver.getCurrentUrl().endsWith("facebook.com")) {
            fail("Doesn't go to the Facebook page.");
        }          
        //then close tab and get back
        driver.close();
        driver.switchTo().window(oldTab);
        if (!driver.getCurrentUrl().endsWith("index2.php")) {
            fail("Doesn't go to the Home page.");
        }		
	}
    

    //Test "Twitter" link text and check whether the correct page is loaded.
	public void testTwitter() throws Exception{
		//get initial window handle
		driver.get(baseUrl);
		String oldTab = driver.getWindowHandle();       
		driver.findElement(By.linkText("Twitter")).click();
        List<String> browserTabs = new ArrayList<String> (driver.getWindowHandles());
        //remove the old tab
        browserTabs.remove(oldTab);
        //switch to new tab
        driver.switchTo().window(browserTabs .get(0));
        //check is it correct page opened or not (e.g. check page's title)
        if (driver.getCurrentUrl().endsWith("twitter.com")) {
            fail("Doesn't go to the Twitter page.");
        }        
        //then close tab and get back
        driver.close();
        driver.switchTo().window(oldTab);
        if (!driver.getCurrentUrl().endsWith("index2.php")) {
            fail("Doesn't go to the Home page.");
        }
    }
}
